import Vue from 'vue'
import Router from 'vue-router'
import EditorRouter from '@/views/Editor/MainRouter'
import EditorMain from '@/views/Editor/Main'
import EditorQuestionAdding from '@/views/Editor/EditorQuestionAdding'
import Faqs from '@/views/Editor/FAQs'
import comment from '@/components/Editor/PageComponents/Editor/FAQ/commentFaq'
import MainFaq from '@/components/Editor/PageComponents/Editor/FAQ/MainFaq'
import Topic from '@/views/Editor/Topic'
import Calendar from '@/views/Editor/Calendar'


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/editor',
      component: EditorRouter,
      children: [{
          path: '',
          name: 'EditorMain',
          component: EditorMain
        },
        {
          path: 'add-question',
          name: 'EditorQuestionAdding',
          component: EditorQuestionAdding
           
        },
        {
          path:'topic',
          name:'Topic',
          component:Topic
        },
        {
          path:'calendar',
          name:'Calendar',
          component:Calendar
        },

        {
          path: 'faqs',
        
          component: Faqs,
          children: [{
              path: '',
              name: 'MainFaq',
              component: MainFaq
            },
            {
              path: 'comment',
              name: 'comment',
              component: comment
            }

          ]
        }
      ]
    }
  ]
})
