/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import NumberBox, { IOptions } from "devextreme/ui/number_box";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "disabled" | "elementAttr" | "focusStateEnabled" | "format" | "height" | "hint" | "hoverStateEnabled" | "inputAttr" | "invalidValueMessage" | "isValid" | "max" | "min" | "mode" | "name" | "onChange" | "onContentReady" | "onCopy" | "onCut" | "onDisposing" | "onEnterKey" | "onFocusIn" | "onFocusOut" | "onInitialized" | "onInput" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onOptionChanged" | "onPaste" | "onValueChanged" | "placeholder" | "readOnly" | "rtlEnabled" | "showClearButton" | "showSpinButtons" | "step" | "stylingMode" | "tabIndex" | "text" | "useLargeSpinButtons" | "validationError" | "validationMessageMode" | "value" | "valueChangeEvent" | "visible" | "width">;
interface DxNumberBox extends VueConstructor, AccessibleOptions {
    readonly instance?: NumberBox;
}
declare const DxNumberBox: DxNumberBox;
declare const DxFormat: any;
export default DxNumberBox;
export { DxNumberBox, DxFormat };
