/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var pivot_grid_field_chooser_1 = require("devextreme/ui/pivot_grid_field_chooser");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxPivotGridFieldChooser = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        allowSearch: Boolean,
        applyChangesMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "instantly",
                "onDemand"
            ].indexOf(v) !== -1; }
        },
        dataSource: Object,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        headerFilter: Object,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        layout: {
            type: Number,
            validator: function (v) { return typeof (v) !== "number" || [
                0,
                1,
                2
            ].indexOf(v) !== -1; }
        },
        onContentReady: Function,
        onContextMenuPreparing: Function,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        rtlEnabled: Boolean,
        searchTimeout: Number,
        state: Object,
        tabIndex: Number,
        texts: Object,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = pivot_grid_field_chooser_1.default;
        this.$_expectedChildren = {
            headerFilter: { isCollectionItem: false, optionName: "headerFilter" },
            pivotGridFieldChooserTexts: { isCollectionItem: false, optionName: "texts" },
            texts: { isCollectionItem: false, optionName: "texts" }
        };
    }
});
exports.DxPivotGridFieldChooser = DxPivotGridFieldChooser;
var DxHeaderFilter = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowSearch: Boolean,
        height: Number,
        searchTimeout: Number,
        texts: Object,
        width: Number
    }
});
exports.DxHeaderFilter = DxHeaderFilter;
DxHeaderFilter.$_optionName = "headerFilter";
DxHeaderFilter.$_expectedChildren = {
    headerFilterTexts: { isCollectionItem: false, optionName: "texts" },
    texts: { isCollectionItem: false, optionName: "texts" }
};
var DxHeaderFilterTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        cancel: String,
        emptyValue: String,
        ok: String
    }
});
exports.DxHeaderFilterTexts = DxHeaderFilterTexts;
DxHeaderFilterTexts.$_optionName = "texts";
var DxPivotGridFieldChooserTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allFields: String,
        columnFields: String,
        dataFields: String,
        filterFields: String,
        rowFields: String
    }
});
exports.DxPivotGridFieldChooserTexts = DxPivotGridFieldChooserTexts;
DxPivotGridFieldChooserTexts.$_optionName = "texts";
var DxTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allFields: String,
        cancel: String,
        columnFields: String,
        dataFields: String,
        emptyValue: String,
        filterFields: String,
        ok: String,
        rowFields: String
    }
});
exports.DxTexts = DxTexts;
DxTexts.$_optionName = "texts";
exports.default = DxPivotGridFieldChooser;
