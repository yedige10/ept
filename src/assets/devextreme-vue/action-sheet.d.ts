/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import ActionSheet, { IOptions } from "devextreme/ui/action_sheet";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "cancelText" | "dataSource" | "disabled" | "elementAttr" | "height" | "hint" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemTemplate" | "onCancelClick" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onOptionChanged" | "rtlEnabled" | "showCancelButton" | "showTitle" | "target" | "title" | "usePopover" | "visible" | "width">;
interface DxActionSheet extends VueConstructor, AccessibleOptions {
    readonly instance?: ActionSheet;
}
declare const DxActionSheet: DxActionSheet;
declare const DxItem: any;
export default DxActionSheet;
export { DxActionSheet, DxItem };
