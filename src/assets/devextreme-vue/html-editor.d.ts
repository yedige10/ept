/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import HtmlEditor, { IOptions } from "devextreme/ui/html_editor";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "isValid" | "name" | "onContentReady" | "onDisposing" | "onFocusIn" | "onFocusOut" | "onInitialized" | "onOptionChanged" | "onValueChanged" | "placeholder" | "readOnly" | "rtlEnabled" | "tabIndex" | "toolbar" | "validationError" | "validationMessageMode" | "value" | "valueType" | "variables" | "visible" | "width">;
interface DxHtmlEditor extends VueConstructor, AccessibleOptions {
    readonly instance?: HtmlEditor;
}
declare const DxHtmlEditor: DxHtmlEditor;
declare const DxItem: any;
declare const DxToolbar: any;
declare const DxVariables: any;
export default DxHtmlEditor;
export { DxHtmlEditor, DxItem, DxToolbar, DxVariables };
