/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var vector_map_1 = require("devextreme/viz/vector_map");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxVectorMap = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        background: Object,
        bounds: Array,
        center: Array,
        controlBar: Object,
        disabled: Boolean,
        elementAttr: Object,
        export: Object,
        layers: [Array, Object],
        legends: Array,
        loadingIndicator: Object,
        maxZoomFactor: Number,
        onCenterChanged: Function,
        onClick: [Function, String],
        onDisposing: Function,
        onDrawn: Function,
        onExported: Function,
        onExporting: Function,
        onFileSaving: Function,
        onIncidentOccurred: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onSelectionChanged: Function,
        onTooltipHidden: Function,
        onTooltipShown: Function,
        onZoomFactorChanged: Function,
        panningEnabled: Boolean,
        pathModified: Boolean,
        projection: Object,
        redrawOnResize: Boolean,
        rtlEnabled: Boolean,
        size: Object,
        theme: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "android5.light",
                "generic.dark",
                "generic.light",
                "generic.contrast",
                "ios7.default",
                "win10.black",
                "win10.white",
                "win8.black",
                "win8.white",
                "generic.carmine",
                "generic.darkmoon",
                "generic.darkviolet",
                "generic.greenmist",
                "generic.softblue",
                "material.blue.light",
                "material.lime.light",
                "material.orange.light",
                "material.purple.light",
                "material.teal.light"
            ].indexOf(v) !== -1; }
        },
        title: [Object, String],
        tooltip: Object,
        touchEnabled: Boolean,
        wheelEnabled: Boolean,
        zoomFactor: Number,
        zoomingEnabled: Boolean
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = vector_map_1.default;
        this.$_expectedChildren = {
            background: { isCollectionItem: false, optionName: "background" },
            controlBar: { isCollectionItem: false, optionName: "controlBar" },
            export: { isCollectionItem: false, optionName: "export" },
            layer: { isCollectionItem: true, optionName: "layers" },
            legend: { isCollectionItem: true, optionName: "legends" },
            loadingIndicator: { isCollectionItem: false, optionName: "loadingIndicator" },
            size: { isCollectionItem: false, optionName: "size" },
            title: { isCollectionItem: false, optionName: "title" },
            tooltip: { isCollectionItem: false, optionName: "tooltip" }
        };
    }
});
exports.DxVectorMap = DxVectorMap;
var DxBackground = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        borderColor: String,
        color: String
    }
});
exports.DxBackground = DxBackground;
DxBackground.$_optionName = "background";
var DxBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxBorder = DxBorder;
DxBorder.$_optionName = "border";
var DxControlBar = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        borderColor: String,
        color: String,
        enabled: Boolean,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: Number,
        opacity: Number,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxControlBar = DxControlBar;
DxControlBar.$_optionName = "controlBar";
var DxExport = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        enabled: Boolean,
        fileName: String,
        formats: Array,
        margin: Number,
        printingEnabled: Boolean,
        proxyUrl: String
    }
});
exports.DxExport = DxExport;
DxExport.$_optionName = "export";
var DxFont = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        family: String,
        opacity: Number,
        size: [Number, String],
        weight: Number
    }
});
exports.DxFont = DxFont;
DxFont.$_optionName = "font";
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        dataField: String,
        enabled: Boolean,
        font: Object
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
DxLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxLayer = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        borderColor: String,
        borderWidth: Number,
        color: String,
        colorGroupingField: String,
        colorGroups: Array,
        customize: Function,
        dataField: String,
        dataSource: [Object, String],
        elementType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bubble",
                "dot",
                "image",
                "pie"
            ].indexOf(v) !== -1; }
        },
        hoveredBorderColor: String,
        hoveredBorderWidth: Number,
        hoveredColor: String,
        hoverEnabled: Boolean,
        label: Object,
        maxSize: Number,
        minSize: Number,
        name: String,
        opacity: Number,
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        paletteSize: Number,
        selectedBorderColor: String,
        selectedBorderWidth: Number,
        selectedColor: String,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "none",
                "single"
            ].indexOf(v) !== -1; }
        },
        size: Number,
        sizeGroupingField: String,
        sizeGroups: Array,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "area",
                "line",
                "marker"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxLayer = DxLayer;
DxLayer.$_optionName = "layers";
DxLayer.$_isCollectionItem = true;
DxLayer.$_expectedChildren = {
    label: { isCollectionItem: false, optionName: "label" }
};
var DxLegend = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        border: Object,
        columnCount: Number,
        columnItemSpacing: Number,
        customizeHint: Function,
        customizeText: Function,
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        itemsAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        itemTextPosition: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        markerColor: String,
        markerShape: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "circle",
                "square"
            ].indexOf(v) !== -1; }
        },
        markerSize: Number,
        orientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "horizontal",
                "vertical"
            ].indexOf(v) !== -1; }
        },
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        rowCount: Number,
        rowItemSpacing: Number,
        source: Object,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxLegend = DxLegend;
DxLegend.$_optionName = "legends";
DxLegend.$_isCollectionItem = true;
DxLegend.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    legendBorder: { isCollectionItem: false, optionName: "border" },
    margin: { isCollectionItem: false, optionName: "margin" },
    source: { isCollectionItem: false, optionName: "source" }
};
var DxLegendBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxLegendBorder = DxLegendBorder;
DxLegendBorder.$_optionName = "border";
var DxLoadingIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        font: Object,
        show: Boolean,
        text: String
    }
});
exports.DxLoadingIndicator = DxLoadingIndicator;
DxLoadingIndicator.$_optionName = "loadingIndicator";
DxLoadingIndicator.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxMargin = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Number,
        left: Number,
        right: Number,
        top: Number
    }
});
exports.DxMargin = DxMargin;
DxMargin.$_optionName = "margin";
var DxShadow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        blur: Number,
        color: String,
        offsetX: Number,
        offsetY: Number,
        opacity: Number
    }
});
exports.DxShadow = DxShadow;
DxShadow.$_optionName = "shadow";
var DxSize = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        width: Number
    }
});
exports.DxSize = DxSize;
DxSize.$_optionName = "size";
var DxSource = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        grouping: String,
        layer: String
    }
});
exports.DxSource = DxSource;
DxSource.$_optionName = "source";
var DxSubtitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String
    }
});
exports.DxSubtitle = DxSubtitle;
DxSubtitle.$_optionName = "subtitle";
DxSubtitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        placeholderSize: Number,
        subtitle: [Object, String],
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxTitle = DxTitle;
DxTitle.$_optionName = "title";
DxTitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    margin: { isCollectionItem: false, optionName: "margin" },
    subtitle: { isCollectionItem: false, optionName: "subtitle" }
};
var DxTooltip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        arrowLength: Number,
        border: Object,
        color: String,
        container: {},
        customizeTooltip: Function,
        enabled: Boolean,
        font: Object,
        format: [Object, Function, String],
        opacity: Number,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        shadow: Object,
        zIndex: Number
    }
});
exports.DxTooltip = DxTooltip;
DxTooltip.$_optionName = "tooltip";
DxTooltip.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    shadow: { isCollectionItem: false, optionName: "shadow" },
    tooltipBorder: { isCollectionItem: false, optionName: "border" }
};
var DxTooltipBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxTooltipBorder = DxTooltipBorder;
DxTooltipBorder.$_optionName = "border";
exports.default = DxVectorMap;
