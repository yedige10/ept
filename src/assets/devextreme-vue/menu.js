/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var menu_1 = require("devextreme/ui/menu");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxMenu = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        adaptivityEnabled: Boolean,
        animation: Object,
        cssClass: String,
        dataSource: [Array, Object, String],
        disabled: Boolean,
        disabledExpr: [Function, String],
        displayExpr: [Function, String],
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hideSubmenuOnMouseLeave: Boolean,
        hint: String,
        hoverStateEnabled: Boolean,
        items: Array,
        itemsExpr: [Function, String],
        itemTemplate: {},
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemRendered: Function,
        onOptionChanged: Function,
        onSelectionChanged: Function,
        onSubmenuHidden: Function,
        onSubmenuHiding: Function,
        onSubmenuShowing: Function,
        onSubmenuShown: Function,
        orientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "horizontal",
                "vertical"
            ].indexOf(v) !== -1; }
        },
        rtlEnabled: Boolean,
        selectByClick: Boolean,
        selectedExpr: [Function, String],
        selectedItem: Object,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "single"
            ].indexOf(v) !== -1; }
        },
        showFirstSubmenuMode: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "onClick",
                "onHover"
            ].indexOf(v) !== -1; }
        },
        showSubmenuMode: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "onClick",
                "onHover"
            ].indexOf(v) !== -1; }
        },
        submenuDirection: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "leftOrTop",
                "rightOrBottom"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = menu_1.default;
        this.$_expectedChildren = {
            animation: { isCollectionItem: false, optionName: "animation" },
            item: { isCollectionItem: true, optionName: "items" },
            showFirstSubmenuMode: { isCollectionItem: false, optionName: "showFirstSubmenuMode" },
            showSubmenuMode: { isCollectionItem: false, optionName: "showSubmenuMode" }
        };
    }
});
exports.DxMenu = DxMenu;
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        hide: Object,
        show: Object
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
DxAnimation.$_expectedChildren = {
    hide: { isCollectionItem: false, optionName: "hide" },
    show: { isCollectionItem: false, optionName: "show" }
};
var DxDelay = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        hide: Number,
        show: Number
    }
});
exports.DxDelay = DxDelay;
DxDelay.$_optionName = "delay";
var DxHide = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxHide = DxHide;
DxHide.$_optionName = "hide";
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        beginGroup: Boolean,
        closeMenuOnClick: Boolean,
        disabled: Boolean,
        html: String,
        icon: String,
        items: Array,
        selectable: Boolean,
        selected: Boolean,
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
var DxShow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxShow = DxShow;
DxShow.$_optionName = "show";
var DxShowFirstSubmenuMode = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        delay: [Number, Object],
        name: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "onClick",
                "onHover"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxShowFirstSubmenuMode = DxShowFirstSubmenuMode;
DxShowFirstSubmenuMode.$_optionName = "showFirstSubmenuMode";
DxShowFirstSubmenuMode.$_expectedChildren = {
    delay: { isCollectionItem: false, optionName: "delay" }
};
var DxShowSubmenuMode = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        delay: [Number, Object],
        name: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "onClick",
                "onHover"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxShowSubmenuMode = DxShowSubmenuMode;
DxShowSubmenuMode.$_optionName = "showSubmenuMode";
DxShowSubmenuMode.$_expectedChildren = {
    delay: { isCollectionItem: false, optionName: "delay" }
};
exports.default = DxMenu;
