/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var filter_builder_1 = require("devextreme/ui/filter_builder");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxFilterBuilder = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        allowHierarchicalFields: Boolean,
        customOperations: Array,
        disabled: Boolean,
        elementAttr: Object,
        fields: Array,
        filterOperationDescriptions: Object,
        focusStateEnabled: Boolean,
        groupOperationDescriptions: Object,
        groupOperations: Array,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        maxGroupLevel: Number,
        onContentReady: Function,
        onDisposing: Function,
        onEditorPrepared: Function,
        onEditorPreparing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onValueChanged: Function,
        rtlEnabled: Boolean,
        tabIndex: Number,
        value: [Array, Function, String],
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = filter_builder_1.default;
        this.$_expectedChildren = {
            customOperation: { isCollectionItem: true, optionName: "customOperations" },
            field: { isCollectionItem: true, optionName: "fields" },
            filterOperationDescriptions: { isCollectionItem: false, optionName: "filterOperationDescriptions" },
            groupOperationDescriptions: { isCollectionItem: false, optionName: "groupOperationDescriptions" }
        };
    }
});
exports.DxFilterBuilder = DxFilterBuilder;
var DxCustomOperation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        calculateFilterExpression: Function,
        caption: String,
        customizeText: Function,
        dataTypes: Array,
        editorTemplate: {},
        hasValue: Boolean,
        icon: String,
        name: String
    }
});
exports.DxCustomOperation = DxCustomOperation;
DxCustomOperation.$_optionName = "customOperations";
DxCustomOperation.$_isCollectionItem = true;
var DxField = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        calculateFilterExpression: Function,
        caption: String,
        customizeText: Function,
        dataField: String,
        dataType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "string",
                "number",
                "date",
                "boolean",
                "object",
                "datetime"
            ].indexOf(v) !== -1; }
        },
        defaultFilterOperation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "=",
                "<>",
                "<",
                "<=",
                ">",
                ">=",
                "contains",
                "endswith",
                "isblank",
                "isnotblank",
                "notcontains",
                "startswith",
                "between"
            ].indexOf(v) !== -1; }
        },
        editorOptions: Object,
        editorTemplate: {},
        falseText: String,
        filterOperations: Array,
        format: [Object, Function, String],
        lookup: Object,
        trueText: String
    }
});
exports.DxField = DxField;
DxField.$_optionName = "fields";
DxField.$_isCollectionItem = true;
DxField.$_expectedChildren = {
    format: { isCollectionItem: false, optionName: "format" },
    lookup: { isCollectionItem: false, optionName: "lookup" }
};
var DxFilterOperationDescriptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        between: String,
        contains: String,
        endsWith: String,
        equal: String,
        greaterThan: String,
        greaterThanOrEqual: String,
        isBlank: String,
        isNotBlank: String,
        lessThan: String,
        lessThanOrEqual: String,
        notContains: String,
        notEqual: String,
        startsWith: String
    }
});
exports.DxFilterOperationDescriptions = DxFilterOperationDescriptions;
DxFilterOperationDescriptions.$_optionName = "filterOperationDescriptions";
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxGroupOperationDescriptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        and: String,
        notAnd: String,
        notOr: String,
        or: String
    }
});
exports.DxGroupOperationDescriptions = DxGroupOperationDescriptions;
DxGroupOperationDescriptions.$_optionName = "groupOperationDescriptions";
var DxLookup = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowClearing: Boolean,
        dataSource: [Array, Object],
        displayExpr: [Function, String],
        valueExpr: [Function, String]
    }
});
exports.DxLookup = DxLookup;
DxLookup.$_optionName = "lookup";
exports.default = DxFilterBuilder;
