/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import ValidationGroup, { IOptions } from "devextreme/ui/validation_group";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "elementAttr" | "height" | "onDisposing" | "onInitialized" | "onOptionChanged" | "width">;
interface DxValidationGroup extends VueConstructor, AccessibleOptions {
    readonly instance?: ValidationGroup;
}
declare const DxValidationGroup: DxValidationGroup;
export default DxValidationGroup;
export { DxValidationGroup };
