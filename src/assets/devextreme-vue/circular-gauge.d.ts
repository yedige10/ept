/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import CircularGauge, { IOptions } from "devextreme/viz/circular_gauge";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "animation" | "containerBackgroundColor" | "disabled" | "elementAttr" | "export" | "geometry" | "loadingIndicator" | "margin" | "onDisposing" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onOptionChanged" | "onTooltipHidden" | "onTooltipShown" | "pathModified" | "rangeContainer" | "redrawOnResize" | "rtlEnabled" | "scale" | "size" | "subvalueIndicator" | "subvalues" | "theme" | "title" | "tooltip" | "value" | "valueIndicator">;
interface DxCircularGauge extends VueConstructor, AccessibleOptions {
    readonly instance?: CircularGauge;
}
declare const DxCircularGauge: DxCircularGauge;
declare const DxAnimation: any;
declare const DxBorder: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxGeometry: any;
declare const DxLabel: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxMinorTick: any;
declare const DxRange: any;
declare const DxRangeContainer: any;
declare const DxScale: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxSubtitle: any;
declare const DxSubvalueIndicator: any;
declare const DxText: any;
declare const DxTick: any;
declare const DxTitle: any;
declare const DxTooltip: any;
declare const DxValueIndicator: any;
export default DxCircularGauge;
export { DxCircularGauge, DxAnimation, DxBorder, DxExport, DxFont, DxFormat, DxGeometry, DxLabel, DxLoadingIndicator, DxMargin, DxMinorTick, DxRange, DxRangeContainer, DxScale, DxShadow, DxSize, DxSubtitle, DxSubvalueIndicator, DxText, DxTick, DxTitle, DxTooltip, DxValueIndicator };
