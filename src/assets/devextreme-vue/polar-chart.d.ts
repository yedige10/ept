/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import PolarChart, { IOptions } from "devextreme/viz/polar_chart";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "adaptiveLayout" | "animation" | "argumentAxis" | "barGroupPadding" | "barGroupWidth" | "commonAxisSettings" | "commonSeriesSettings" | "containerBackgroundColor" | "customizeLabel" | "customizePoint" | "dataPrepareSettings" | "dataSource" | "disabled" | "elementAttr" | "export" | "legend" | "loadingIndicator" | "margin" | "negativesAsZeroes" | "onArgumentAxisClick" | "onDisposing" | "onDone" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onLegendClick" | "onOptionChanged" | "onPointClick" | "onPointHoverChanged" | "onPointSelectionChanged" | "onSeriesClick" | "onSeriesHoverChanged" | "onSeriesSelectionChanged" | "onTooltipHidden" | "onTooltipShown" | "palette" | "paletteExtensionMode" | "pathModified" | "pointSelectionMode" | "redrawOnResize" | "resolveLabelOverlapping" | "rtlEnabled" | "series" | "seriesSelectionMode" | "seriesTemplate" | "size" | "theme" | "title" | "tooltip" | "useSpiderWeb" | "valueAxis">;
interface DxPolarChart extends VueConstructor, AccessibleOptions {
    readonly instance?: PolarChart;
}
declare const DxPolarChart: DxPolarChart;
declare const DxAdaptiveLayout: any;
declare const DxAnimation: any;
declare const DxArgumentAxis: any;
declare const DxArgumentFormat: any;
declare const DxAxisLabel: any;
declare const DxBorder: any;
declare const DxCommonAxisSettings: any;
declare const DxCommonAxisSettingsLabel: any;
declare const DxCommonSeriesSettings: any;
declare const DxCommonSeriesSettingsHoverStyle: any;
declare const DxCommonSeriesSettingsLabel: any;
declare const DxCommonSeriesSettingsSelectionStyle: any;
declare const DxConnector: any;
declare const DxConstantLine: any;
declare const DxConstantLineLabel: any;
declare const DxConstantLineStyle: any;
declare const DxConstantLineStyleLabel: any;
declare const DxDataPrepareSettings: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxGrid: any;
declare const DxHatching: any;
declare const DxHoverStyle: any;
declare const DxImage: any;
declare const DxLabel: any;
declare const DxLegend: any;
declare const DxLegendBorder: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxMinorGrid: any;
declare const DxMinorTick: any;
declare const DxMinorTickInterval: any;
declare const DxPoint: any;
declare const DxPointBorder: any;
declare const DxPointHoverStyle: any;
declare const DxPointSelectionStyle: any;
declare const DxSelectionStyle: any;
declare const DxSeries: any;
declare const DxSeriesBorder: any;
declare const DxSeriesTemplate: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxStrip: any;
declare const DxStripLabel: any;
declare const DxStripStyle: any;
declare const DxStripStyleLabel: any;
declare const DxSubtitle: any;
declare const DxTick: any;
declare const DxTickInterval: any;
declare const DxTitle: any;
declare const DxTooltip: any;
declare const DxTooltipBorder: any;
declare const DxValueAxis: any;
declare const DxValueErrorBar: any;
export default DxPolarChart;
export { DxPolarChart, DxAdaptiveLayout, DxAnimation, DxArgumentAxis, DxArgumentFormat, DxAxisLabel, DxBorder, DxCommonAxisSettings, DxCommonAxisSettingsLabel, DxCommonSeriesSettings, DxCommonSeriesSettingsHoverStyle, DxCommonSeriesSettingsLabel, DxCommonSeriesSettingsSelectionStyle, DxConnector, DxConstantLine, DxConstantLineLabel, DxConstantLineStyle, DxConstantLineStyleLabel, DxDataPrepareSettings, DxExport, DxFont, DxFormat, DxGrid, DxHatching, DxHoverStyle, DxImage, DxLabel, DxLegend, DxLegendBorder, DxLoadingIndicator, DxMargin, DxMinorGrid, DxMinorTick, DxMinorTickInterval, DxPoint, DxPointBorder, DxPointHoverStyle, DxPointSelectionStyle, DxSelectionStyle, DxSeries, DxSeriesBorder, DxSeriesTemplate, DxShadow, DxSize, DxStrip, DxStripLabel, DxStripStyle, DxStripStyleLabel, DxSubtitle, DxTick, DxTickInterval, DxTitle, DxTooltip, DxTooltipBorder, DxValueAxis, DxValueErrorBar };
