/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import TextBox, { IOptions } from "devextreme/ui/text_box";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "inputAttr" | "isValid" | "mask" | "maskChar" | "maskInvalidMessage" | "maskRules" | "maxLength" | "mode" | "name" | "onChange" | "onContentReady" | "onCopy" | "onCut" | "onDisposing" | "onEnterKey" | "onFocusIn" | "onFocusOut" | "onInitialized" | "onInput" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onOptionChanged" | "onPaste" | "onValueChanged" | "placeholder" | "readOnly" | "rtlEnabled" | "showClearButton" | "showMaskMode" | "spellcheck" | "stylingMode" | "tabIndex" | "text" | "useMaskedValue" | "validationError" | "validationMessageMode" | "value" | "valueChangeEvent" | "visible" | "width">;
interface DxTextBox extends VueConstructor, AccessibleOptions {
    readonly instance?: TextBox;
}
declare const DxTextBox: DxTextBox;
export default DxTextBox;
export { DxTextBox };
