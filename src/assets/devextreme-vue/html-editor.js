/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var html_editor_1 = require("devextreme/ui/html_editor");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxHtmlEditor = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        isValid: Boolean,
        name: String,
        onContentReady: Function,
        onDisposing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onValueChanged: Function,
        placeholder: String,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        tabIndex: Number,
        toolbar: Object,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: {},
        valueType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "html",
                "markdown"
            ].indexOf(v) !== -1; }
        },
        variables: Object,
        visible: Boolean,
        width: [Function, Number, String]
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = html_editor_1.default;
        this.$_expectedChildren = {
            toolbar: { isCollectionItem: false, optionName: "toolbar" },
            variables: { isCollectionItem: false, optionName: "variables" }
        };
    }
});
exports.DxHtmlEditor = DxHtmlEditor;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        cssClass: String,
        disabled: Boolean,
        formatName: String,
        formatValues: Array,
        html: String,
        locateInMenu: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto",
                "never"
            ].indexOf(v) !== -1; }
        },
        location: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "after",
                "before",
                "center"
            ].indexOf(v) !== -1; }
        },
        menuItemTemplate: {},
        options: Object,
        showText: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "inMenu"
            ].indexOf(v) !== -1; }
        },
        template: {},
        text: String,
        visible: Boolean,
        widget: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dxAutocomplete",
                "dxButton",
                "dxCheckBox",
                "dxDateBox",
                "dxMenu",
                "dxSelectBox",
                "dxTabs",
                "dxTextBox",
                "dxButtonGroup"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
var DxToolbar = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        container: {},
        items: Array
    }
});
exports.DxToolbar = DxToolbar;
DxToolbar.$_optionName = "toolbar";
DxToolbar.$_expectedChildren = {
    item: { isCollectionItem: true, optionName: "items" }
};
var DxVariables = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        dataSource: [Array, Object, String],
        escapeChar: [Array, String]
    }
});
exports.DxVariables = DxVariables;
DxVariables.$_optionName = "variables";
exports.default = DxHtmlEditor;
