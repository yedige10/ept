/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var box_1 = require("devextreme/ui/box");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxBox = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        align: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "end",
                "space-around",
                "space-between",
                "start"
            ].indexOf(v) !== -1; }
        },
        crossAlign: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "end",
                "start",
                "stretch"
            ].indexOf(v) !== -1; }
        },
        dataSource: [Array, Object, String],
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "col",
                "row"
            ].indexOf(v) !== -1; }
        },
        disabled: Boolean,
        elementAttr: Object,
        height: [Function, Number, String],
        hoverStateEnabled: Boolean,
        itemHoldTimeout: Number,
        items: Array,
        itemTemplate: {},
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onOptionChanged: Function,
        rtlEnabled: Boolean,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = box_1.default;
        this.$_expectedChildren = {
            item: { isCollectionItem: true, optionName: "items" }
        };
    }
});
exports.DxBox = DxBox;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        baseSize: {
            type: [Number, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "auto"
            ].indexOf(v) !== -1; }
        },
        box: Object,
        disabled: Boolean,
        html: String,
        ratio: Number,
        shrink: Number,
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
exports.default = DxBox;
