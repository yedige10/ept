/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Sankey, { IOptions } from "devextreme/viz/sankey";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "adaptiveLayout" | "alignment" | "dataSource" | "disabled" | "elementAttr" | "export" | "hoverEnabled" | "label" | "link" | "loadingIndicator" | "margin" | "node" | "onDisposing" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onLinkClick" | "onLinkHoverChanged" | "onNodeClick" | "onNodeHoverChanged" | "onOptionChanged" | "palette" | "paletteExtensionMode" | "pathModified" | "redrawOnResize" | "rtlEnabled" | "size" | "sortData" | "sourceField" | "targetField" | "theme" | "title" | "tooltip" | "weightField">;
interface DxSankey extends VueConstructor, AccessibleOptions {
    readonly instance?: Sankey;
}
declare const DxSankey: DxSankey;
declare const DxAdaptiveLayout: any;
declare const DxBorder: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxHatching: any;
declare const DxHoverStyle: any;
declare const DxLabel: any;
declare const DxLink: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxNode: any;
declare const DxSankeyborder: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxSubtitle: any;
declare const DxTitle: any;
declare const DxTooltip: any;
declare const DxTooltipBorder: any;
export default DxSankey;
export { DxSankey, DxAdaptiveLayout, DxBorder, DxExport, DxFont, DxFormat, DxHatching, DxHoverStyle, DxLabel, DxLink, DxLoadingIndicator, DxMargin, DxNode, DxSankeyborder, DxShadow, DxSize, DxSubtitle, DxTitle, DxTooltip, DxTooltipBorder };
