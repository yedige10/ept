/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import TileView, { IOptions } from "devextreme/ui/tile_view";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "baseItemHeight" | "baseItemWidth" | "dataSource" | "direction" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "itemHoldTimeout" | "itemMargin" | "items" | "itemTemplate" | "noDataText" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onOptionChanged" | "rtlEnabled" | "showScrollbar" | "tabIndex" | "visible" | "width">;
interface DxTileView extends VueConstructor, AccessibleOptions {
    readonly instance?: TileView;
}
declare const DxTileView: DxTileView;
declare const DxItem: any;
export default DxTileView;
export { DxTileView, DxItem };
