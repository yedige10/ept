/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Scheduler, { IOptions } from "devextreme/ui/scheduler";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "allDayExpr" | "appointmentCollectorTemplate" | "appointmentTemplate" | "appointmentTooltipTemplate" | "cellDuration" | "crossScrollingEnabled" | "currentDate" | "currentView" | "customizeDateNavigatorText" | "dataCellTemplate" | "dataSource" | "dateCellTemplate" | "dateSerializationFormat" | "descriptionExpr" | "disabled" | "dropDownAppointmentTemplate" | "editing" | "elementAttr" | "endDateExpr" | "endDateTimeZoneExpr" | "endDayHour" | "firstDayOfWeek" | "focusStateEnabled" | "groupByDate" | "groups" | "height" | "hint" | "indicatorUpdateInterval" | "max" | "maxAppointmentsPerCell" | "min" | "noDataText" | "onAppointmentAdded" | "onAppointmentAdding" | "onAppointmentClick" | "onAppointmentContextMenu" | "onAppointmentDblClick" | "onAppointmentDeleted" | "onAppointmentDeleting" | "onAppointmentFormOpening" | "onAppointmentRendered" | "onAppointmentUpdated" | "onAppointmentUpdating" | "onCellClick" | "onCellContextMenu" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "recurrenceEditMode" | "recurrenceExceptionExpr" | "recurrenceRuleExpr" | "remoteFiltering" | "resourceCellTemplate" | "resources" | "rtlEnabled" | "selectedCellData" | "shadeUntilCurrentTime" | "showAllDayPanel" | "showCurrentTimeIndicator" | "startDateExpr" | "startDateTimeZoneExpr" | "startDayHour" | "tabIndex" | "textExpr" | "timeCellTemplate" | "timeZone" | "useDropDownViewSwitcher" | "views" | "visible" | "width">;
interface DxScheduler extends VueConstructor, AccessibleOptions {
    readonly instance?: Scheduler;
}
declare const DxScheduler: DxScheduler;
declare const DxEditing: any;
declare const DxResource: any;
declare const DxView: any;
export default DxScheduler;
export { DxScheduler, DxEditing, DxResource, DxView };
