/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import ResponsiveBox, { IOptions } from "devextreme/ui/responsive_box";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "cols" | "dataSource" | "disabled" | "elementAttr" | "height" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemTemplate" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onOptionChanged" | "rows" | "rtlEnabled" | "screenByWidth" | "singleColumnScreen" | "visible" | "width">;
interface DxResponsiveBox extends VueConstructor, AccessibleOptions {
    readonly instance?: ResponsiveBox;
}
declare const DxResponsiveBox: DxResponsiveBox;
declare const DxCol: any;
declare const DxItem: any;
declare const DxLocation: any;
declare const DxRow: any;
export default DxResponsiveBox;
export { DxResponsiveBox, DxCol, DxItem, DxLocation, DxRow };
