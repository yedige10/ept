/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Autocomplete, { IOptions } from "devextreme/ui/autocomplete";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "dataSource" | "deferRendering" | "disabled" | "displayValue" | "dropDownButtonTemplate" | "elementAttr" | "focusStateEnabled" | "grouped" | "groupTemplate" | "height" | "hint" | "hoverStateEnabled" | "inputAttr" | "isValid" | "items" | "itemTemplate" | "maxItemCount" | "maxLength" | "minSearchLength" | "name" | "onChange" | "onClosed" | "onContentReady" | "onCopy" | "onCut" | "onDisposing" | "onEnterKey" | "onFocusIn" | "onFocusOut" | "onInitialized" | "onInput" | "onItemClick" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onOpened" | "onOptionChanged" | "onPaste" | "onSelectionChanged" | "onValueChanged" | "opened" | "openOnFieldClick" | "placeholder" | "readOnly" | "rtlEnabled" | "searchExpr" | "searchMode" | "searchTimeout" | "selectedItem" | "showClearButton" | "showDropDownButton" | "spellcheck" | "stylingMode" | "tabIndex" | "text" | "validationError" | "validationMessageMode" | "value" | "valueChangeEvent" | "valueExpr" | "visible" | "width">;
interface DxAutocomplete extends VueConstructor, AccessibleOptions {
    readonly instance?: Autocomplete;
}
declare const DxAutocomplete: DxAutocomplete;
declare const DxItem: any;
export default DxAutocomplete;
export { DxAutocomplete, DxItem };
