/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Drawer, { IOptions } from "devextreme/ui/drawer";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "activeStateEnabled" | "animationDuration" | "animationEnabled" | "closeOnOutsideClick" | "disabled" | "elementAttr" | "height" | "hint" | "hoverStateEnabled" | "maxSize" | "minSize" | "onDisposing" | "onInitialized" | "onOptionChanged" | "opened" | "openedStateMode" | "position" | "revealMode" | "rtlEnabled" | "shading" | "target" | "template" | "visible" | "width">;
interface DxDrawer extends VueConstructor, AccessibleOptions {
    readonly instance?: Drawer;
}
declare const DxDrawer: DxDrawer;
export default DxDrawer;
export { DxDrawer };
