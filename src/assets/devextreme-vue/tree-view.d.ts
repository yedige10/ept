/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import TreeView, { IOptions } from "devextreme/ui/tree_view";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "animationEnabled" | "createChildren" | "dataSource" | "dataStructure" | "disabled" | "disabledExpr" | "displayExpr" | "elementAttr" | "expandAllEnabled" | "expandedExpr" | "expandEvent" | "expandNodesRecursive" | "focusStateEnabled" | "hasItemsExpr" | "height" | "hint" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemsExpr" | "itemTemplate" | "keyExpr" | "noDataText" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemCollapsed" | "onItemContextMenu" | "onItemExpanded" | "onItemHold" | "onItemRendered" | "onItemSelectionChanged" | "onOptionChanged" | "onSelectAllValueChanged" | "onSelectionChanged" | "parentIdExpr" | "rootValue" | "rtlEnabled" | "scrollDirection" | "searchEditorOptions" | "searchEnabled" | "searchExpr" | "searchMode" | "searchTimeout" | "searchValue" | "selectAllText" | "selectByClick" | "selectedExpr" | "selectionMode" | "selectNodesRecursive" | "showCheckBoxesMode" | "tabIndex" | "virtualModeEnabled" | "visible" | "width">;
interface DxTreeView extends VueConstructor, AccessibleOptions {
    readonly instance?: TreeView;
}
declare const DxTreeView: DxTreeView;
declare const DxItem: any;
declare const DxSearchEditorOptions: any;
export default DxTreeView;
export { DxTreeView, DxItem, DxSearchEditorOptions };
