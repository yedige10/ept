/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import ProgressBar, { IOptions } from "devextreme/ui/progress_bar";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "disabled" | "elementAttr" | "height" | "hint" | "hoverStateEnabled" | "isValid" | "max" | "min" | "onComplete" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onValueChanged" | "readOnly" | "rtlEnabled" | "showStatus" | "statusFormat" | "validationError" | "validationMessageMode" | "value" | "visible" | "width">;
interface DxProgressBar extends VueConstructor, AccessibleOptions {
    readonly instance?: ProgressBar;
}
declare const DxProgressBar: DxProgressBar;
export default DxProgressBar;
export { DxProgressBar };
