/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var helpers_1 = require("./helpers");
var Configuration = /** @class */ (function () {
    function Configuration(updateFunc, name, initialValues, expectedChildren, isCollectionItem, collectionItemIndex) {
        this._updateFunc = updateFunc;
        this._name = name;
        this._initialValues = initialValues ? initialValues : {};
        this._nestedConfigurations = [];
        this._isCollectionItem = !!isCollectionItem;
        this._collectionItemIndex = collectionItemIndex;
        this._expectedChildren = expectedChildren || {};
        this.updateValue = this.updateValue.bind(this);
    }
    Object.defineProperty(Configuration.prototype, "name", {
        get: function () {
            return this._name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Configuration.prototype, "fullPath", {
        get: function () {
            return this._isCollectionItem ? this._name + "[" + this._collectionItemIndex + "]" : this._name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Configuration.prototype, "options", {
        get: function () {
            return this._options;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Configuration.prototype, "initialValues", {
        get: function () {
            return this._initialValues;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Configuration.prototype, "expectedChildren", {
        get: function () {
            return this._expectedChildren;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Configuration.prototype, "nested", {
        get: function () {
            return this._nestedConfigurations;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Configuration.prototype, "collectionItemIndex", {
        get: function () {
            return this._collectionItemIndex;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Configuration.prototype, "isCollectionItem", {
        get: function () {
            return this._isCollectionItem;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Configuration.prototype, "updateFunc", {
        get: function () {
            return this._updateFunc;
        },
        enumerable: true,
        configurable: true
    });
    Configuration.prototype.init = function (options) {
        this._options = options ? options : [];
    };
    Object.defineProperty(Configuration.prototype, "optionChangedFunc", {
        set: function (handler) {
            this._optionChangedFunc = handler;
        },
        enumerable: true,
        configurable: true
    });
    Configuration.prototype.onOptionChanged = function (args) {
        if (this._optionChangedFunc) {
            this._optionChangedFunc(args);
        }
        this._nestedConfigurations.forEach(function (nestedConfig) {
            nestedConfig.onOptionChanged(args);
        });
    };
    Configuration.prototype.cleanNested = function () {
        this._nestedConfigurations = [];
    };
    Configuration.prototype.createNested = function (name, initialValues, isCollectionItem, expectedChildren) {
        var expected = this._expectedChildren[name];
        var actualName = name;
        var actualIsCollectionItem = isCollectionItem;
        if (expected) {
            actualIsCollectionItem = expected.isCollectionItem;
            if (expected.optionName) {
                actualName = expected.optionName;
            }
        }
        var collectionItemIndex = -1;
        if (actualIsCollectionItem && actualName) {
            collectionItemIndex = this._nestedConfigurations.filter(function (c) { return c._name && c._name === actualName; }).length;
        }
        var configuration = new Configuration(this.updateValue, actualName, initialValues, expectedChildren, actualIsCollectionItem, collectionItemIndex);
        this._nestedConfigurations.push(configuration);
        return configuration;
    };
    Configuration.prototype.updateValue = function (nestedName, value) {
        var fullName = [this.fullPath, nestedName].filter(function (n) { return n; }).join(".");
        this._updateFunc(fullName, value);
    };
    Configuration.prototype.getInitialValues = function () {
        var values = __assign({}, this._initialValues);
        this._nestedConfigurations.forEach(function (o) {
            if (!o._name) {
                return;
            }
            var nestedValue = o.getInitialValues();
            if (!nestedValue) {
                return;
            }
            if (!o._isCollectionItem) {
                values[o._name] = nestedValue;
            }
            else {
                var arr = values[o._name];
                if (!arr || !Array.isArray(arr)) {
                    arr = [];
                    values[o._name] = arr;
                }
                arr.push(nestedValue);
            }
        });
        return Object.keys(values).length > 0 ? values : undefined;
    };
    Configuration.prototype.getOptionsToWatch = function () {
        var blackList = {};
        this._nestedConfigurations.forEach(function (c) { return c._name && (blackList[c._name] = true); });
        return this._options.filter(function (o) { return !blackList[o]; });
    };
    return Configuration;
}());
function bindOptionWatchers(config, vueInstance) {
    var targets = config.getOptionsToWatch();
    if (targets) {
        targets.forEach(function (optionName) {
            vueInstance.$watch(optionName, function (value) { return config.updateValue(optionName, value); });
        });
    }
}
exports.bindOptionWatchers = bindOptionWatchers;
function subscribeOnUpdates(config, vueInstance) {
    config.optionChangedFunc = function (args) {
        var optionName = args.name;
        var optionValue = args.value;
        var fullOptionPath = config.fullPath + ".";
        if (config.name && config.name === args.name && args.fullName.indexOf(fullOptionPath) === 0) {
            optionName = args.fullName.slice(fullOptionPath.length);
        }
        else if (args.fullName !== args.name) {
            optionValue = args.component.option(optionName);
        }
        if (!helpers_1.isEqual(args.value, args.previousValue)) {
            vueInstance.$emit("update:" + optionName, optionValue);
        }
    };
}
exports.subscribeOnUpdates = subscribeOnUpdates;
exports.default = Configuration;
