/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var events = require("devextreme/events");
var children_processing_1 = require("./children-processing");
var configuration_1 = require("./configuration");
var helpers_1 = require("./helpers");
var Vue = VueType.default || VueType;
var DX_TEMPLATE_WRAPPER_CLASS = "dx-template-wrapper";
var DX_REMOVE_EVENT = "dxremove";
var BaseComponent = Vue.extend({
    inheritAttrs: false,
    data: function () {
        return {
            eventBus: new Vue()
        };
    },
    provide: function () {
        return {
            eventBus: this.eventBus
        };
    },
    render: function (createElement) {
        var children = [];
        children_processing_1.pullAllChildren(this.$slots.default, children, this.$_config);
        this.$_processChildren(children);
        return createElement("div", {
            attrs: { id: this.$attrs.id }
        }, children);
    },
    updated: function () {
        this.eventBus.$emit("updated");
    },
    beforeDestroy: function () {
        var instance = this.$_instance;
        if (instance) {
            events.triggerHandler(this.$el, DX_REMOVE_EVENT);
            instance.dispose();
        }
    },
    created: function () {
        var _this = this;
        this.$_config = new configuration_1.default(function (n, v) { return _this.$_instance.option(n, v); }, null, this.$options.propsData && __assign({}, this.$options.propsData), this.$_expectedChildren);
        this.$_config.init(this.$props && Object.keys(this.$props));
    },
    methods: {
        $_createWidget: function (element) {
            var config = this.$_config;
            var options = __assign({}, this.$options.propsData, config.getInitialValues(), this.$_getIntegrationOptions());
            var instance = new this.$_WidgetClass(element, options);
            this.$_instance = instance;
            instance.on("optionChanged", function (args) { return config.onOptionChanged(args); });
            configuration_1.subscribeOnUpdates(config, this);
            configuration_1.bindOptionWatchers(config, this);
            this.$_createEmitters(instance);
        },
        $_getIntegrationOptions: function () {
            var _this = this;
            var TEMPLATE_PROP = "template";
            function shouldAddTemplate(child) {
                return child.$vnode
                    && child.$vnode.componentOptions.$_config
                    && child.$vnode.componentOptions.$_config.name
                    && TEMPLATE_PROP in child.$props
                    && child.$scopedSlots.default;
            }
            var result = __assign({ integrationOptions: {
                    watchMethod: this.$_getWatchMethod(),
                } }, this.$_getExtraIntegrationOptions());
            var templates = __assign({}, this.$scopedSlots);
            this.$children.forEach(function (child) {
                if (shouldAddTemplate(child)) {
                    var templateName = child.$vnode.componentOptions.$_config.fullPath + "." + TEMPLATE_PROP;
                    templates[templateName] = child.$scopedSlots.default;
                    result[templateName] = templateName;
                }
            });
            if (Object.keys(templates).length) {
                result.integrationOptions.templates = {};
                Object.keys(templates).forEach(function (name) {
                    result.integrationOptions.templates[name] = _this.$_fillTemplate(templates[name], name);
                });
            }
            return result;
        },
        $_getWatchMethod: function () {
            var _this = this;
            return function (valueGetter, valueChangeCallback, options) {
                options = options || {};
                if (!options.skipImmediate) {
                    valueChangeCallback(valueGetter());
                }
                return _this.$watch(function () {
                    return valueGetter();
                }, function (newValue, oldValue) {
                    if (helpers_1.toComparable(oldValue) !== helpers_1.toComparable(newValue) || options.deep) {
                        valueChangeCallback(newValue);
                    }
                }, {
                    deep: options.deep
                });
            };
        },
        $_getExtraIntegrationOptions: function () {
            return {};
        },
        $_processChildren: function (_children) {
            return;
        },
        $_fillTemplate: function (template, name) {
            var _this = this;
            return {
                render: function (data) {
                    var vm = new Vue({
                        name: name,
                        inject: ["eventBus"],
                        parent: _this,
                        created: function () {
                            var _this = this;
                            this.eventBus.$on("updated", function () {
                                _this.$forceUpdate();
                            });
                        },
                        render: function () { return template(data.model); }
                    }).$mount();
                    var element = vm.$el;
                    element.classList.add(DX_TEMPLATE_WRAPPER_CLASS);
                    var container = data.container.get ? data.container.get(0) : data.container;
                    container.appendChild(element);
                    events.one(element, DX_REMOVE_EVENT, vm.$destroy.bind(vm));
                    return element;
                }
            };
        },
        $_createEmitters: function (instance) {
            var _this = this;
            Object.keys(this.$listeners).forEach(function (listenerName) {
                var eventName = helpers_1.camelize(listenerName);
                instance.on(eventName, function (e) {
                    _this.$emit(listenerName, e);
                });
            });
        }
    }
});
exports.BaseComponent = BaseComponent;
var DxComponent = BaseComponent.extend({
    methods: {
        $_getExtraIntegrationOptions: function () {
            return {
                onInitializing: function () {
                    this.beginUpdate();
                }
            };
        },
        $_processChildren: function (children) {
            children.forEach(function (childNode) {
                if (!childNode.componentOptions) {
                    return;
                }
                childNode.componentOptions.$_hasOwner = true;
            });
        },
    },
    mounted: function () {
        var _this = this;
        this.$_createWidget(this.$el);
        this.$_instance.endUpdate();
        this.$children.forEach(function (child) {
            if (child.$_isExtension) {
                child.attachTo(_this.$el);
            }
        });
    }
});
exports.DxComponent = DxComponent;
