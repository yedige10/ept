/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function uppercaseFirst(value) {
    return value[0].toUpperCase() + value.substr(1);
}
exports.uppercaseFirst = uppercaseFirst;
function lowercaseFirst(value) {
    return value[0].toLowerCase() + value.substr(1);
}
exports.lowercaseFirst = lowercaseFirst;
function camelize(value) {
    return lowercaseFirst(value.split("-").map(function (v) { return uppercaseFirst(v); }).join(""));
}
exports.camelize = camelize;
function toComparable(value) {
    return value instanceof Date ? value.getTime() : value;
}
exports.toComparable = toComparable;
function isEqual(value1, value2) {
    if (toComparable(value1) === toComparable(value2)) {
        return true;
    }
    if (Array.isArray(value1) && Array.isArray(value2)) {
        return value1.length === 0 && value2.length === 0;
    }
    return false;
}
exports.isEqual = isEqual;
