/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

export declare function uppercaseFirst(value: string): string;
export declare function lowercaseFirst(value: string): string;
export declare function camelize(value: string): string;
export declare function toComparable(value: any): any;
export declare function isEqual(value1: any, value2: any): boolean;
