/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var configuration_1 = require("./configuration");
var DxConfiguration = Vue.extend({
    beforeMount: function () {
        var config = this.$vnode.componentOptions.$_config;
        config.init(Object.keys(this.$props));
        configuration_1.bindOptionWatchers(config, this);
        configuration_1.subscribeOnUpdates(config, this);
    },
    render: function (createElement) {
        return createElement();
    }
});
exports.DxConfiguration = DxConfiguration;
