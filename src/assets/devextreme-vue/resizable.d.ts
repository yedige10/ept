/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Resizable, { IOptions } from "devextreme/ui/resizable";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "elementAttr" | "handles" | "height" | "maxHeight" | "maxWidth" | "minHeight" | "minWidth" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onResize" | "onResizeEnd" | "onResizeStart" | "rtlEnabled" | "width">;
interface DxResizable extends VueConstructor, AccessibleOptions {
    readonly instance?: Resizable;
}
declare const DxResizable: DxResizable;
export default DxResizable;
export { DxResizable };
