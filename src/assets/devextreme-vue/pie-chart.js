/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var pie_chart_1 = require("devextreme/viz/pie_chart");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxPieChart = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        adaptiveLayout: Object,
        animation: [Boolean, Object],
        commonSeriesSettings: Object,
        customizeLabel: Function,
        customizePoint: Function,
        dataSource: [Array, Object, String],
        diameter: Number,
        disabled: Boolean,
        elementAttr: Object,
        export: Object,
        innerRadius: Number,
        legend: Object,
        loadingIndicator: Object,
        margin: Object,
        minDiameter: Number,
        onDisposing: Function,
        onDone: Function,
        onDrawn: Function,
        onExported: Function,
        onExporting: Function,
        onFileSaving: Function,
        onIncidentOccurred: Function,
        onInitialized: Function,
        onLegendClick: [Function, String],
        onOptionChanged: Function,
        onPointClick: [Function, String],
        onPointHoverChanged: Function,
        onPointSelectionChanged: Function,
        onTooltipHidden: Function,
        onTooltipShown: Function,
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        paletteExtensionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "alternate",
                "blend",
                "extrapolate"
            ].indexOf(v) !== -1; }
        },
        pathModified: Boolean,
        pointSelectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "single"
            ].indexOf(v) !== -1; }
        },
        redrawOnResize: Boolean,
        resolveLabelOverlapping: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "hide",
                "none",
                "shift"
            ].indexOf(v) !== -1; }
        },
        rtlEnabled: Boolean,
        segmentsDirection: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "anticlockwise",
                "clockwise"
            ].indexOf(v) !== -1; }
        },
        series: [Array, Object],
        seriesTemplate: Object,
        size: Object,
        sizeGroup: String,
        startAngle: Number,
        theme: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "android5.light",
                "generic.dark",
                "generic.light",
                "generic.contrast",
                "ios7.default",
                "win10.black",
                "win10.white",
                "win8.black",
                "win8.white",
                "generic.carmine",
                "generic.darkmoon",
                "generic.darkviolet",
                "generic.greenmist",
                "generic.softblue",
                "material.blue.light",
                "material.lime.light",
                "material.orange.light",
                "material.purple.light",
                "material.teal.light"
            ].indexOf(v) !== -1; }
        },
        title: [Object, String],
        tooltip: Object,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "donut",
                "doughnut",
                "pie"
            ].indexOf(v) !== -1; }
        }
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = pie_chart_1.default;
        this.$_expectedChildren = {
            adaptiveLayout: { isCollectionItem: false, optionName: "adaptiveLayout" },
            animation: { isCollectionItem: false, optionName: "animation" },
            commonSeriesSettings: { isCollectionItem: false, optionName: "commonSeriesSettings" },
            export: { isCollectionItem: false, optionName: "export" },
            legend: { isCollectionItem: false, optionName: "legend" },
            loadingIndicator: { isCollectionItem: false, optionName: "loadingIndicator" },
            margin: { isCollectionItem: false, optionName: "margin" },
            series: { isCollectionItem: true, optionName: "series" },
            seriesTemplate: { isCollectionItem: false, optionName: "seriesTemplate" },
            size: { isCollectionItem: false, optionName: "size" },
            title: { isCollectionItem: false, optionName: "title" },
            tooltip: { isCollectionItem: false, optionName: "tooltip" }
        };
    }
});
exports.DxPieChart = DxPieChart;
var DxAdaptiveLayout = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        keepLabels: Boolean,
        width: Number
    }
});
exports.DxAdaptiveLayout = DxAdaptiveLayout;
DxAdaptiveLayout.$_optionName = "adaptiveLayout";
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        duration: Number,
        easing: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "easeOutCubic",
                "linear"
            ].indexOf(v) !== -1; }
        },
        enabled: Boolean,
        maxPointCountSupported: Number
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
var DxArgumentFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxArgumentFormat = DxArgumentFormat;
DxArgumentFormat.$_optionName = "argumentFormat";
var DxBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxBorder = DxBorder;
DxBorder.$_optionName = "border";
var DxCommonSeriesSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        argumentField: String,
        argumentType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "datetime",
                "numeric",
                "string"
            ].indexOf(v) !== -1; }
        },
        border: Object,
        color: String,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        label: Object,
        maxLabelCount: Number,
        minSegmentSize: Number,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        smallValuesGrouping: Object,
        tagField: String,
        valueField: String
    }
});
exports.DxCommonSeriesSettings = DxCommonSeriesSettings;
DxCommonSeriesSettings.$_optionName = "commonSeriesSettings";
var DxConnector = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxConnector = DxConnector;
DxConnector.$_optionName = "connector";
var DxExport = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        enabled: Boolean,
        fileName: String,
        formats: Array,
        margin: Number,
        printingEnabled: Boolean,
        proxyUrl: String
    }
});
exports.DxExport = DxExport;
DxExport.$_optionName = "export";
var DxFont = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        family: String,
        opacity: Number,
        size: [Number, String],
        weight: Number
    }
});
exports.DxFont = DxFont;
DxFont.$_optionName = "font";
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxHatching = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "none",
                "right"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        step: Number,
        width: Number
    }
});
exports.DxHatching = DxHatching;
DxHatching.$_optionName = "hatching";
var DxHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        hatching: Object
    }
});
exports.DxHoverStyle = DxHoverStyle;
DxHoverStyle.$_optionName = "hoverStyle";
DxHoverStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hatching: { isCollectionItem: false, optionName: "hatching" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        argumentFormat: [Object, Function, String],
        backgroundColor: String,
        border: Object,
        connector: Object,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "columns",
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        radialOffset: Number,
        rotationAngle: Number,
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
DxLabel.$_expectedChildren = {
    argumentFormat: { isCollectionItem: false, optionName: "argumentFormat" },
    border: { isCollectionItem: false, optionName: "border" },
    connector: { isCollectionItem: false, optionName: "connector" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxLegend = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        border: Object,
        columnCount: Number,
        columnItemSpacing: Number,
        customizeHint: Function,
        customizeText: Function,
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "allArgumentPoints"
            ].indexOf(v) !== -1; }
        },
        itemsAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        itemTextPosition: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        markerSize: Number,
        orientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "horizontal",
                "vertical"
            ].indexOf(v) !== -1; }
        },
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        rowCount: Number,
        rowItemSpacing: Number,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxLegend = DxLegend;
DxLegend.$_optionName = "legend";
DxLegend.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    legendBorder: { isCollectionItem: false, optionName: "border" },
    margin: { isCollectionItem: false, optionName: "margin" }
};
var DxLegendBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxLegendBorder = DxLegendBorder;
DxLegendBorder.$_optionName = "border";
var DxLoadingIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        font: Object,
        show: Boolean,
        text: String
    }
});
exports.DxLoadingIndicator = DxLoadingIndicator;
DxLoadingIndicator.$_optionName = "loadingIndicator";
DxLoadingIndicator.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxMargin = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Number,
        left: Number,
        right: Number,
        top: Number
    }
});
exports.DxMargin = DxMargin;
DxMargin.$_optionName = "margin";
var DxSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        hatching: Object
    }
});
exports.DxSelectionStyle = DxSelectionStyle;
DxSelectionStyle.$_optionName = "selectionStyle";
DxSelectionStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hatching: { isCollectionItem: false, optionName: "hatching" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxSeries = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        argumentField: String,
        argumentType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "datetime",
                "numeric",
                "string"
            ].indexOf(v) !== -1; }
        },
        border: Object,
        color: String,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        label: Object,
        maxLabelCount: Number,
        minSegmentSize: Number,
        name: String,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        smallValuesGrouping: Object,
        tag: {},
        tagField: String,
        valueField: String
    }
});
exports.DxSeries = DxSeries;
DxSeries.$_optionName = "series";
DxSeries.$_isCollectionItem = true;
DxSeries.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    label: { isCollectionItem: false, optionName: "label" },
    selectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    seriesBorder: { isCollectionItem: false, optionName: "border" },
    smallValuesGrouping: { isCollectionItem: false, optionName: "smallValuesGrouping" }
};
var DxSeriesBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        width: Number
    }
});
exports.DxSeriesBorder = DxSeriesBorder;
DxSeriesBorder.$_optionName = "border";
var DxSeriesTemplate = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeSeries: Function,
        nameField: String
    }
});
exports.DxSeriesTemplate = DxSeriesTemplate;
DxSeriesTemplate.$_optionName = "seriesTemplate";
var DxShadow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        blur: Number,
        color: String,
        offsetX: Number,
        offsetY: Number,
        opacity: Number
    }
});
exports.DxShadow = DxShadow;
DxShadow.$_optionName = "shadow";
var DxSize = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        width: Number
    }
});
exports.DxSize = DxSize;
DxSize.$_optionName = "size";
var DxSmallValuesGrouping = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        groupName: String,
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "smallValueThreshold",
                "topN"
            ].indexOf(v) !== -1; }
        },
        threshold: Number,
        topCount: Number
    }
});
exports.DxSmallValuesGrouping = DxSmallValuesGrouping;
DxSmallValuesGrouping.$_optionName = "smallValuesGrouping";
var DxSubtitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String
    }
});
exports.DxSubtitle = DxSubtitle;
DxSubtitle.$_optionName = "subtitle";
DxSubtitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        placeholderSize: Number,
        subtitle: [Object, String],
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxTitle = DxTitle;
DxTitle.$_optionName = "title";
DxTitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    margin: { isCollectionItem: false, optionName: "margin" },
    subtitle: { isCollectionItem: false, optionName: "subtitle" }
};
var DxTooltip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        argumentFormat: [Object, Function, String],
        arrowLength: Number,
        border: Object,
        color: String,
        container: {},
        customizeTooltip: Function,
        enabled: Boolean,
        font: Object,
        format: [Object, Function, String],
        opacity: Number,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        shadow: Object,
        zIndex: Number
    }
});
exports.DxTooltip = DxTooltip;
DxTooltip.$_optionName = "tooltip";
DxTooltip.$_expectedChildren = {
    argumentFormat: { isCollectionItem: false, optionName: "argumentFormat" },
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    shadow: { isCollectionItem: false, optionName: "shadow" },
    tooltipBorder: { isCollectionItem: false, optionName: "border" }
};
var DxTooltipBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxTooltipBorder = DxTooltipBorder;
DxTooltipBorder.$_optionName = "border";
exports.default = DxPieChart;
