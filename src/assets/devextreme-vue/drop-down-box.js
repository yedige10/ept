/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var drop_down_box_1 = require("devextreme/ui/drop_down_box");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxDropDownBox = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        acceptCustomValue: Boolean,
        accessKey: String,
        activeStateEnabled: Boolean,
        contentTemplate: {},
        dataSource: [Array, Object, String],
        deferRendering: Boolean,
        disabled: Boolean,
        displayExpr: [Function, String],
        dropDownButtonTemplate: {},
        dropDownOptions: Object,
        elementAttr: Object,
        fieldTemplate: {},
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        inputAttr: Object,
        isValid: Boolean,
        items: Array,
        name: String,
        onChange: Function,
        onClosed: Function,
        onCopy: Function,
        onCut: Function,
        onDisposing: Function,
        onEnterKey: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onInput: Function,
        onKeyDown: Function,
        onKeyPress: Function,
        onKeyUp: Function,
        onOpened: Function,
        onOptionChanged: Function,
        onPaste: Function,
        onValueChanged: Function,
        opened: Boolean,
        openOnFieldClick: Boolean,
        placeholder: String,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        showClearButton: Boolean,
        showDropDownButton: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "outlined",
                "underlined",
                "filled"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        text: String,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: {},
        valueChangeEvent: String,
        valueExpr: [Function, String],
        visible: Boolean,
        width: [Function, Number, String]
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = drop_down_box_1.default;
        this.$_expectedChildren = {
            dropDownOptions: { isCollectionItem: false, optionName: "dropDownOptions" },
            item: { isCollectionItem: true, optionName: "items" }
        };
    }
});
exports.DxDropDownBox = DxDropDownBox;
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        hide: Object,
        show: Object
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
DxAnimation.$_expectedChildren = {
    hide: { isCollectionItem: false, optionName: "hide" },
    show: { isCollectionItem: false, optionName: "show" }
};
var DxAt = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxAt = DxAt;
DxAt.$_optionName = "at";
var DxBoundaryOffset = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: Number,
        y: Number
    }
});
exports.DxBoundaryOffset = DxBoundaryOffset;
DxBoundaryOffset.$_optionName = "boundaryOffset";
var DxCollision = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "flip",
                "flipfit",
                "none"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "flip",
                "flipfit",
                "none"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxCollision = DxCollision;
DxCollision.$_optionName = "collision";
var DxDropDownOptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        animation: Object,
        bindingOptions: Object,
        closeOnBackButton: Boolean,
        closeOnOutsideClick: [Boolean, Function],
        container: {},
        contentTemplate: {},
        deferRendering: Boolean,
        disabled: Boolean,
        dragEnabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        fullScreen: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        maxHeight: [Function, Number, String],
        maxWidth: [Function, Number, String],
        minHeight: [Function, Number, String],
        minWidth: [Function, Number, String],
        onContentReady: Function,
        onDisposing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onHidden: Function,
        onHiding: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onResize: Function,
        onResizeEnd: Function,
        onResizeStart: Function,
        onShowing: Function,
        onShown: Function,
        onTitleRendered: Function,
        position: {
            type: [Function, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        resizeEnabled: Boolean,
        rtlEnabled: Boolean,
        shading: Boolean,
        shadingColor: String,
        showCloseButton: Boolean,
        showTitle: Boolean,
        tabIndex: Number,
        title: String,
        titleTemplate: {},
        toolbarItems: Array,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxDropDownOptions = DxDropDownOptions;
DxDropDownOptions.$_optionName = "dropDownOptions";
DxDropDownOptions.$_expectedChildren = {
    animation: { isCollectionItem: false, optionName: "animation" },
    position: { isCollectionItem: false, optionName: "position" },
    toolbarItem: { isCollectionItem: true, optionName: "toolbarItems" }
};
var DxHide = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxHide = DxHide;
DxHide.$_optionName = "hide";
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        html: String,
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
var DxMy = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxMy = DxMy;
DxMy.$_optionName = "my";
var DxOffset = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: Number,
        y: Number
    }
});
exports.DxOffset = DxOffset;
DxOffset.$_optionName = "offset";
var DxPosition = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        at: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        boundary: {},
        boundaryOffset: [Object, String],
        collision: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "fit flip",
                "fit flipfit",
                "fit none",
                "flip",
                "flip fit",
                "flip none",
                "flipfit",
                "flipfit fit",
                "flipfit none",
                "none",
                "none fit",
                "none flip",
                "none flipfit"
            ].indexOf(v) !== -1; }
        },
        my: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        of: {},
        offset: [Object, String]
    }
});
exports.DxPosition = DxPosition;
DxPosition.$_optionName = "position";
DxPosition.$_expectedChildren = {
    at: { isCollectionItem: false, optionName: "at" },
    boundaryOffset: { isCollectionItem: false, optionName: "boundaryOffset" },
    collision: { isCollectionItem: false, optionName: "collision" },
    my: { isCollectionItem: false, optionName: "my" },
    offset: { isCollectionItem: false, optionName: "offset" }
};
var DxShow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxShow = DxShow;
DxShow.$_optionName = "show";
var DxToolbarItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        html: String,
        location: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "after",
                "before",
                "center"
            ].indexOf(v) !== -1; }
        },
        options: Object,
        template: {},
        text: String,
        toolbar: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        widget: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dxAutocomplete",
                "dxButton",
                "dxCheckBox",
                "dxDateBox",
                "dxMenu",
                "dxSelectBox",
                "dxTabs",
                "dxTextBox",
                "dxButtonGroup"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxToolbarItem = DxToolbarItem;
DxToolbarItem.$_optionName = "toolbarItems";
DxToolbarItem.$_isCollectionItem = true;
exports.default = DxDropDownBox;
