/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Calendar, { IOptions } from "devextreme/ui/calendar";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "cellTemplate" | "dateSerializationFormat" | "disabled" | "disabledDates" | "elementAttr" | "firstDayOfWeek" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "isValid" | "max" | "maxZoomLevel" | "min" | "minZoomLevel" | "name" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onValueChanged" | "readOnly" | "rtlEnabled" | "showTodayButton" | "tabIndex" | "validationError" | "validationMessageMode" | "value" | "visible" | "width" | "zoomLevel">;
interface DxCalendar extends VueConstructor, AccessibleOptions {
    readonly instance?: Calendar;
}
declare const DxCalendar: DxCalendar;
export default DxCalendar;
export { DxCalendar };
