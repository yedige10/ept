/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import TabPanel, { IOptions } from "devextreme/ui/tab_panel";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "animationEnabled" | "dataSource" | "deferRendering" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemTemplate" | "itemTitleTemplate" | "loop" | "noDataText" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onOptionChanged" | "onSelectionChanged" | "onTitleClick" | "onTitleHold" | "onTitleRendered" | "repaintChangesOnly" | "rtlEnabled" | "scrollByContent" | "scrollingEnabled" | "selectedIndex" | "selectedItem" | "showNavButtons" | "swipeEnabled" | "tabIndex" | "visible" | "width">;
interface DxTabPanel extends VueConstructor, AccessibleOptions {
    readonly instance?: TabPanel;
}
declare const DxTabPanel: DxTabPanel;
declare const DxItem: any;
export default DxTabPanel;
export { DxTabPanel, DxItem };
