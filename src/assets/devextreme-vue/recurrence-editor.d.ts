/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import RecurrenceEditor, { IOptions } from "devextreme/ui/recurrence_editor";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "isValid" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onValueChanged" | "readOnly" | "rtlEnabled" | "tabIndex" | "validationError" | "validationMessageMode" | "value" | "visible" | "width">;
interface DxRecurrenceEditor extends VueConstructor, AccessibleOptions {
    readonly instance?: RecurrenceEditor;
}
declare const DxRecurrenceEditor: DxRecurrenceEditor;
export default DxRecurrenceEditor;
export { DxRecurrenceEditor };
