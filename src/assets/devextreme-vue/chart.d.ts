/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Chart, { IOptions } from "devextreme/viz/chart";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "adaptiveLayout" | "adjustOnZoom" | "animation" | "argumentAxis" | "barGroupPadding" | "barGroupWidth" | "commonAxisSettings" | "commonPaneSettings" | "commonSeriesSettings" | "containerBackgroundColor" | "crosshair" | "customizeLabel" | "customizePoint" | "dataPrepareSettings" | "dataSource" | "defaultPane" | "disabled" | "elementAttr" | "export" | "legend" | "loadingIndicator" | "margin" | "maxBubbleSize" | "minBubbleSize" | "negativesAsZeroes" | "onArgumentAxisClick" | "onDisposing" | "onDone" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onLegendClick" | "onOptionChanged" | "onPointClick" | "onPointHoverChanged" | "onPointSelectionChanged" | "onSeriesClick" | "onSeriesHoverChanged" | "onSeriesSelectionChanged" | "onTooltipHidden" | "onTooltipShown" | "onZoomEnd" | "onZoomStart" | "palette" | "paletteExtensionMode" | "panes" | "pathModified" | "pointSelectionMode" | "redrawOnResize" | "resolveLabelOverlapping" | "rotated" | "rtlEnabled" | "scrollBar" | "series" | "seriesSelectionMode" | "seriesTemplate" | "size" | "synchronizeMultiAxes" | "theme" | "title" | "tooltip" | "valueAxis" | "zoomAndPan">;
interface DxChart extends VueConstructor, AccessibleOptions {
    readonly instance?: Chart;
}
declare const DxChart: DxChart;
declare const DxAdaptiveLayout: any;
declare const DxAggregation: any;
declare const DxAggregationInterval: any;
declare const DxAnimation: any;
declare const DxArgumentAxis: any;
declare const DxArgumentFormat: any;
declare const DxAxisConstantLineStyle: any;
declare const DxAxisConstantLineStyleLabel: any;
declare const DxAxisLabel: any;
declare const DxAxisTitle: any;
declare const DxBorder: any;
declare const DxBreak: any;
declare const DxBreakStyle: any;
declare const DxChartTitle: any;
declare const DxCommonAxisSettings: any;
declare const DxCommonAxisSettingsConstantLineStyle: any;
declare const DxCommonAxisSettingsConstantLineStyleLabel: any;
declare const DxCommonAxisSettingsLabel: any;
declare const DxCommonAxisSettingsTitle: any;
declare const DxCommonPaneSettings: any;
declare const DxCommonSeriesSettings: any;
declare const DxCommonSeriesSettingsHoverStyle: any;
declare const DxCommonSeriesSettingsLabel: any;
declare const DxCommonSeriesSettingsSelectionStyle: any;
declare const DxConnector: any;
declare const DxConstantLine: any;
declare const DxConstantLineLabel: any;
declare const DxConstantLineStyle: any;
declare const DxCrosshair: any;
declare const DxCrosshairLabel: any;
declare const DxDataPrepareSettings: any;
declare const DxDragBoxStyle: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxGrid: any;
declare const DxHatching: any;
declare const DxHeight: any;
declare const DxHorizontalLine: any;
declare const DxHoverStyle: any;
declare const DxImage: any;
declare const DxLabel: any;
declare const DxLegend: any;
declare const DxLegendBorder: any;
declare const DxLength: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxMinorGrid: any;
declare const DxMinorTick: any;
declare const DxMinorTickInterval: any;
declare const DxMinVisualRangeLength: any;
declare const DxPane: any;
declare const DxPaneBorder: any;
declare const DxPoint: any;
declare const DxPointBorder: any;
declare const DxPointHoverStyle: any;
declare const DxPointSelectionStyle: any;
declare const DxReduction: any;
declare const DxScrollBar: any;
declare const DxSelectionStyle: any;
declare const DxSeries: any;
declare const DxSeriesBorder: any;
declare const DxSeriesTemplate: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxStrip: any;
declare const DxStripLabel: any;
declare const DxStripStyle: any;
declare const DxStripStyleLabel: any;
declare const DxSubtitle: any;
declare const DxTick: any;
declare const DxTickInterval: any;
declare const DxTitle: any;
declare const DxTooltip: any;
declare const DxTooltipBorder: any;
declare const DxUrl: any;
declare const DxValueAxis: any;
declare const DxValueErrorBar: any;
declare const DxVerticalLine: any;
declare const DxVisualRange: any;
declare const DxWholeRange: any;
declare const DxWidth: any;
declare const DxZoomAndPan: any;
export default DxChart;
export { DxChart, DxAdaptiveLayout, DxAggregation, DxAggregationInterval, DxAnimation, DxArgumentAxis, DxArgumentFormat, DxAxisConstantLineStyle, DxAxisConstantLineStyleLabel, DxAxisLabel, DxAxisTitle, DxBorder, DxBreak, DxBreakStyle, DxChartTitle, DxCommonAxisSettings, DxCommonAxisSettingsConstantLineStyle, DxCommonAxisSettingsConstantLineStyleLabel, DxCommonAxisSettingsLabel, DxCommonAxisSettingsTitle, DxCommonPaneSettings, DxCommonSeriesSettings, DxCommonSeriesSettingsHoverStyle, DxCommonSeriesSettingsLabel, DxCommonSeriesSettingsSelectionStyle, DxConnector, DxConstantLine, DxConstantLineLabel, DxConstantLineStyle, DxCrosshair, DxCrosshairLabel, DxDataPrepareSettings, DxDragBoxStyle, DxExport, DxFont, DxFormat, DxGrid, DxHatching, DxHeight, DxHorizontalLine, DxHoverStyle, DxImage, DxLabel, DxLegend, DxLegendBorder, DxLength, DxLoadingIndicator, DxMargin, DxMinorGrid, DxMinorTick, DxMinorTickInterval, DxMinVisualRangeLength, DxPane, DxPaneBorder, DxPoint, DxPointBorder, DxPointHoverStyle, DxPointSelectionStyle, DxReduction, DxScrollBar, DxSelectionStyle, DxSeries, DxSeriesBorder, DxSeriesTemplate, DxShadow, DxSize, DxStrip, DxStripLabel, DxStripStyle, DxStripStyleLabel, DxSubtitle, DxTick, DxTickInterval, DxTitle, DxTooltip, DxTooltipBorder, DxUrl, DxValueAxis, DxValueErrorBar, DxVerticalLine, DxVisualRange, DxWholeRange, DxWidth, DxZoomAndPan };
