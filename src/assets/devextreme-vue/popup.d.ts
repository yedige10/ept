/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Popup, { IOptions } from "devextreme/ui/popup";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "animation" | "closeOnBackButton" | "closeOnOutsideClick" | "container" | "contentTemplate" | "deferRendering" | "disabled" | "dragEnabled" | "elementAttr" | "focusStateEnabled" | "fullScreen" | "height" | "hint" | "hoverStateEnabled" | "maxHeight" | "maxWidth" | "minHeight" | "minWidth" | "onContentReady" | "onDisposing" | "onHidden" | "onHiding" | "onInitialized" | "onOptionChanged" | "onResize" | "onResizeEnd" | "onResizeStart" | "onShowing" | "onShown" | "onTitleRendered" | "position" | "resizeEnabled" | "rtlEnabled" | "shading" | "shadingColor" | "showCloseButton" | "showTitle" | "tabIndex" | "title" | "titleTemplate" | "toolbarItems" | "visible" | "width">;
interface DxPopup extends VueConstructor, AccessibleOptions {
    readonly instance?: Popup;
}
declare const DxPopup: DxPopup;
declare const DxAnimation: any;
declare const DxAt: any;
declare const DxBoundaryOffset: any;
declare const DxCollision: any;
declare const DxHide: any;
declare const DxMy: any;
declare const DxOffset: any;
declare const DxPosition: any;
declare const DxShow: any;
declare const DxToolbarItem: any;
export default DxPopup;
export { DxPopup, DxAnimation, DxAt, DxBoundaryOffset, DxCollision, DxHide, DxMy, DxOffset, DxPosition, DxShow, DxToolbarItem };
