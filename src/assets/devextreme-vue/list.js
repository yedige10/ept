/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var list_1 = require("devextreme/ui/list");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxList = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        allowItemDeleting: Boolean,
        allowItemReordering: Boolean,
        bounceEnabled: Boolean,
        collapsibleGroups: Boolean,
        dataSource: [Array, Object, String],
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        grouped: Boolean,
        groupTemplate: {},
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        indicateLoading: Boolean,
        itemDeleteMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "context",
                "slideButton",
                "slideItem",
                "static",
                "swipe",
                "toggle"
            ].indexOf(v) !== -1; }
        },
        itemHoldTimeout: Number,
        items: Array,
        itemTemplate: {},
        keyExpr: [Function, String],
        menuItems: Array,
        menuMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "context",
                "slide"
            ].indexOf(v) !== -1; }
        },
        nextButtonText: String,
        noDataText: String,
        onContentReady: Function,
        onDisposing: Function,
        onGroupRendered: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemDeleted: Function,
        onItemDeleting: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onItemReordered: Function,
        onItemSwipe: Function,
        onOptionChanged: Function,
        onPageLoading: Function,
        onPullRefresh: Function,
        onScroll: Function,
        onSelectAllValueChanged: Function,
        onSelectionChanged: Function,
        pageLoadingText: String,
        pageLoadMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "nextButton",
                "scrollBottom"
            ].indexOf(v) !== -1; }
        },
        pulledDownText: String,
        pullingDownText: String,
        pullRefreshEnabled: Boolean,
        refreshingText: String,
        repaintChangesOnly: Boolean,
        rtlEnabled: Boolean,
        scrollByContent: Boolean,
        scrollByThumb: Boolean,
        scrollingEnabled: Boolean,
        searchEditorOptions: Object,
        searchEnabled: Boolean,
        searchExpr: [Array, Function, String],
        searchMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "contains",
                "startswith",
                "equals"
            ].indexOf(v) !== -1; }
        },
        searchTimeout: Number,
        searchValue: String,
        selectAllMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allPages",
                "page"
            ].indexOf(v) !== -1; }
        },
        selectedItemKeys: Array,
        selectedItems: Array,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "all",
                "multiple",
                "none",
                "single"
            ].indexOf(v) !== -1; }
        },
        showScrollbar: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "never",
                "onHover",
                "onScroll"
            ].indexOf(v) !== -1; }
        },
        showSelectionControls: Boolean,
        tabIndex: Number,
        useNativeScrolling: Boolean,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = list_1.default;
        this.$_expectedChildren = {
            item: { isCollectionItem: true, optionName: "items" },
            menuItem: { isCollectionItem: true, optionName: "menuItems" },
            searchEditorOptions: { isCollectionItem: false, optionName: "searchEditorOptions" }
        };
    }
});
exports.DxList = DxList;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        badge: String,
        disabled: Boolean,
        html: String,
        icon: String,
        key: String,
        showChevron: Boolean,
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
var DxMenuItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        action: Function,
        text: String
    }
});
exports.DxMenuItem = DxMenuItem;
DxMenuItem.$_optionName = "menuItems";
DxMenuItem.$_isCollectionItem = true;
var DxSearchEditorOptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        bindingOptions: Object,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        inputAttr: Object,
        isValid: Boolean,
        mask: String,
        maskChar: String,
        maskInvalidMessage: String,
        maskRules: Object,
        maxLength: [Number, String],
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "email",
                "password",
                "search",
                "tel",
                "text",
                "url"
            ].indexOf(v) !== -1; }
        },
        name: String,
        onChange: Function,
        onContentReady: Function,
        onCopy: Function,
        onCut: Function,
        onDisposing: Function,
        onEnterKey: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onInput: Function,
        onKeyDown: Function,
        onKeyPress: Function,
        onKeyUp: Function,
        onOptionChanged: Function,
        onPaste: Function,
        onValueChanged: Function,
        placeholder: String,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        showClearButton: Boolean,
        showMaskMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "onFocus"
            ].indexOf(v) !== -1; }
        },
        spellcheck: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "outlined",
                "underlined",
                "filled"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        text: String,
        useMaskedValue: Boolean,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: String,
        valueChangeEvent: String,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxSearchEditorOptions = DxSearchEditorOptions;
DxSearchEditorOptions.$_optionName = "searchEditorOptions";
exports.default = DxList;
