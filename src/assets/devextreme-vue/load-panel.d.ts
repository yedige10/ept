/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import LoadPanel, { IOptions } from "devextreme/ui/load_panel";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "animation" | "closeOnOutsideClick" | "container" | "deferRendering" | "delay" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "indicatorSrc" | "maxHeight" | "maxWidth" | "message" | "minHeight" | "minWidth" | "onContentReady" | "onDisposing" | "onHidden" | "onHiding" | "onInitialized" | "onOptionChanged" | "onShowing" | "onShown" | "position" | "rtlEnabled" | "shading" | "shadingColor" | "showIndicator" | "showPane" | "visible" | "width">;
interface DxLoadPanel extends VueConstructor, AccessibleOptions {
    readonly instance?: LoadPanel;
}
declare const DxLoadPanel: DxLoadPanel;
declare const DxAnimation: any;
declare const DxAt: any;
declare const DxBoundaryOffset: any;
declare const DxCollision: any;
declare const DxHide: any;
declare const DxMy: any;
declare const DxOffset: any;
declare const DxPosition: any;
declare const DxShow: any;
export default DxLoadPanel;
export { DxLoadPanel, DxAnimation, DxAt, DxBoundaryOffset, DxCollision, DxHide, DxMy, DxOffset, DxPosition, DxShow };
