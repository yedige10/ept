/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Gallery, { IOptions } from "devextreme/ui/gallery";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "animationDuration" | "animationEnabled" | "dataSource" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "indicatorEnabled" | "initialItemWidth" | "itemHoldTimeout" | "items" | "itemTemplate" | "loop" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onOptionChanged" | "onSelectionChanged" | "rtlEnabled" | "selectedIndex" | "selectedItem" | "showIndicator" | "showNavButtons" | "slideshowDelay" | "stretchImages" | "swipeEnabled" | "tabIndex" | "visible" | "width" | "wrapAround">;
interface DxGallery extends VueConstructor, AccessibleOptions {
    readonly instance?: Gallery;
}
declare const DxGallery: DxGallery;
declare const DxItem: any;
export default DxGallery;
export { DxGallery, DxItem };
