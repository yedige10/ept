/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import ButtonGroup, { IOptions } from "devextreme/ui/button_group";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "items" | "itemTemplate" | "keyExpr" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onSelectionChanged" | "rtlEnabled" | "selectedItemKeys" | "selectedItems" | "selectionMode" | "stylingMode" | "tabIndex" | "visible" | "width">;
interface DxButtonGroup extends VueConstructor, AccessibleOptions {
    readonly instance?: ButtonGroup;
}
declare const DxButtonGroup: DxButtonGroup;
declare const DxItem: any;
export default DxButtonGroup;
export { DxButtonGroup, DxItem };
