/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import ColorBox, { IOptions } from "devextreme/ui/color_box";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "acceptCustomValue" | "accessKey" | "activeStateEnabled" | "applyButtonText" | "applyValueMode" | "cancelButtonText" | "deferRendering" | "disabled" | "dropDownButtonTemplate" | "editAlphaChannel" | "elementAttr" | "fieldTemplate" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "inputAttr" | "isValid" | "keyStep" | "name" | "onChange" | "onClosed" | "onCopy" | "onCut" | "onDisposing" | "onEnterKey" | "onFocusIn" | "onFocusOut" | "onInitialized" | "onInput" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onOpened" | "onOptionChanged" | "onPaste" | "onValueChanged" | "opened" | "placeholder" | "readOnly" | "rtlEnabled" | "stylingMode" | "tabIndex" | "text" | "validationError" | "validationMessageMode" | "value" | "visible" | "width">;
interface DxColorBox extends VueConstructor, AccessibleOptions {
    readonly instance?: ColorBox;
}
declare const DxColorBox: DxColorBox;
export default DxColorBox;
export { DxColorBox };
