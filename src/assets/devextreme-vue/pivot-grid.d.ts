/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import PivotGrid, { IOptions } from "devextreme/ui/pivot_grid";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "allowExpandAll" | "allowFiltering" | "allowSorting" | "allowSortingBySummary" | "dataFieldArea" | "dataSource" | "disabled" | "elementAttr" | "export" | "fieldChooser" | "fieldPanel" | "headerFilter" | "height" | "hideEmptySummaryCells" | "hint" | "loadPanel" | "onCellClick" | "onCellPrepared" | "onContentReady" | "onContextMenuPreparing" | "onDisposing" | "onExported" | "onExporting" | "onFileSaving" | "onInitialized" | "onOptionChanged" | "rowHeaderLayout" | "rtlEnabled" | "scrolling" | "showBorders" | "showColumnGrandTotals" | "showColumnTotals" | "showRowGrandTotals" | "showRowTotals" | "showTotalsPrior" | "stateStoring" | "tabIndex" | "texts" | "visible" | "width" | "wordWrapEnabled">;
interface DxPivotGrid extends VueConstructor, AccessibleOptions {
    readonly instance?: PivotGrid;
}
declare const DxPivotGrid: DxPivotGrid;
declare const DxExport: any;
declare const DxFieldChooser: any;
declare const DxFieldChooserTexts: any;
declare const DxFieldPanel: any;
declare const DxFieldPanelTexts: any;
declare const DxHeaderFilter: any;
declare const DxHeaderFilterTexts: any;
declare const DxLoadPanel: any;
declare const DxPivotGridTexts: any;
declare const DxScrolling: any;
declare const DxStateStoring: any;
declare const DxTexts: any;
export default DxPivotGrid;
export { DxPivotGrid, DxExport, DxFieldChooser, DxFieldChooserTexts, DxFieldPanel, DxFieldPanelTexts, DxHeaderFilter, DxHeaderFilterTexts, DxLoadPanel, DxPivotGridTexts, DxScrolling, DxStateStoring, DxTexts };
