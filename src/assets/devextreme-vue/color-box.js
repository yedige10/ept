/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var color_box_1 = require("devextreme/ui/color_box");
var component_1 = require("./core/component");
var DxColorBox = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        acceptCustomValue: Boolean,
        accessKey: String,
        activeStateEnabled: Boolean,
        applyButtonText: String,
        applyValueMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "instantly",
                "useButtons"
            ].indexOf(v) !== -1; }
        },
        cancelButtonText: String,
        deferRendering: Boolean,
        disabled: Boolean,
        dropDownButtonTemplate: {},
        editAlphaChannel: Boolean,
        elementAttr: Object,
        fieldTemplate: {},
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        inputAttr: Object,
        isValid: Boolean,
        keyStep: Number,
        name: String,
        onChange: Function,
        onClosed: Function,
        onCopy: Function,
        onCut: Function,
        onDisposing: Function,
        onEnterKey: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onInput: Function,
        onKeyDown: Function,
        onKeyPress: Function,
        onKeyUp: Function,
        onOpened: Function,
        onOptionChanged: Function,
        onPaste: Function,
        onValueChanged: Function,
        opened: Boolean,
        placeholder: String,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "outlined",
                "underlined",
                "filled"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        text: String,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: String,
        visible: Boolean,
        width: [Function, Number, String]
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = color_box_1.default;
    }
});
exports.DxColorBox = DxColorBox;
exports.default = DxColorBox;
