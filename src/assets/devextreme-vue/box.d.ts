/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Box, { IOptions } from "devextreme/ui/box";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "align" | "crossAlign" | "dataSource" | "direction" | "disabled" | "elementAttr" | "height" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemTemplate" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onOptionChanged" | "rtlEnabled" | "visible" | "width">;
interface DxBox extends VueConstructor, AccessibleOptions {
    readonly instance?: Box;
}
declare const DxBox: DxBox;
declare const DxItem: any;
export default DxBox;
export { DxBox, DxItem };
