/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import SlideOut, { IOptions } from "devextreme/ui/slide_out";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "activeStateEnabled" | "contentTemplate" | "dataSource" | "disabled" | "elementAttr" | "height" | "hint" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemTemplate" | "menuGrouped" | "menuGroupTemplate" | "menuItemTemplate" | "menuPosition" | "menuVisible" | "noDataText" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onMenuGroupRendered" | "onMenuItemRendered" | "onOptionChanged" | "onSelectionChanged" | "rtlEnabled" | "selectedIndex" | "selectedItem" | "swipeEnabled" | "visible" | "width">;
interface DxSlideOut extends VueConstructor, AccessibleOptions {
    readonly instance?: SlideOut;
}
declare const DxSlideOut: DxSlideOut;
declare const DxItem: any;
export default DxSlideOut;
export { DxSlideOut, DxItem };
