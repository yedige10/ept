/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Tooltip, { IOptions } from "devextreme/ui/tooltip";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "animation" | "closeOnBackButton" | "closeOnOutsideClick" | "container" | "contentTemplate" | "deferRendering" | "disabled" | "elementAttr" | "height" | "hideEvent" | "hint" | "hoverStateEnabled" | "maxHeight" | "maxWidth" | "minHeight" | "minWidth" | "onContentReady" | "onDisposing" | "onHidden" | "onHiding" | "onInitialized" | "onOptionChanged" | "onShowing" | "onShown" | "position" | "rtlEnabled" | "shading" | "shadingColor" | "showEvent" | "target" | "visible" | "width">;
interface DxTooltip extends VueConstructor, AccessibleOptions {
    readonly instance?: Tooltip;
}
declare const DxTooltip: DxTooltip;
declare const DxAnimation: any;
declare const DxAt: any;
declare const DxBoundaryOffset: any;
declare const DxCollision: any;
declare const DxHide: any;
declare const DxHideEvent: any;
declare const DxMy: any;
declare const DxOffset: any;
declare const DxPosition: any;
declare const DxShow: any;
declare const DxShowEvent: any;
export default DxTooltip;
export { DxTooltip, DxAnimation, DxAt, DxBoundaryOffset, DxCollision, DxHide, DxHideEvent, DxMy, DxOffset, DxPosition, DxShow, DxShowEvent };
