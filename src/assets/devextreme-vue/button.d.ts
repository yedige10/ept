/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Button, { IOptions } from "devextreme/ui/button";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "icon" | "onClick" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "rtlEnabled" | "stylingMode" | "tabIndex" | "template" | "text" | "type" | "useSubmitBehavior" | "validationGroup" | "visible" | "width">;
interface DxButton extends VueConstructor, AccessibleOptions {
    readonly instance?: Button;
}
declare const DxButton: DxButton;
export default DxButton;
export { DxButton };
