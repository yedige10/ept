/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var tree_list_1 = require("devextreme/ui/tree_list");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxTreeList = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        allowColumnReordering: Boolean,
        allowColumnResizing: Boolean,
        autoExpandAll: Boolean,
        cacheEnabled: Boolean,
        cellHintEnabled: Boolean,
        columnAutoWidth: Boolean,
        columnChooser: Object,
        columnFixing: Object,
        columnHidingEnabled: Boolean,
        columnMinWidth: Number,
        columnResizingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "nextColumn",
                "widget"
            ].indexOf(v) !== -1; }
        },
        columns: Array,
        columnWidth: Number,
        customizeColumns: Function,
        dataSource: [Array, Object, String],
        dataStructure: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "plain",
                "tree"
            ].indexOf(v) !== -1; }
        },
        dateSerializationFormat: String,
        disabled: Boolean,
        editing: Object,
        elementAttr: Object,
        errorRowEnabled: Boolean,
        expandedRowKeys: Array,
        expandNodesOnFiltering: Boolean,
        filterBuilder: Object,
        filterBuilderPopup: Object,
        filterPanel: Object,
        filterRow: Object,
        filterSyncEnabled: Boolean,
        filterValue: [Array, Function, String],
        focusedColumnIndex: Number,
        focusedRowEnabled: Boolean,
        focusedRowIndex: Number,
        focusedRowKey: {},
        focusStateEnabled: Boolean,
        hasItemsExpr: [Function, String],
        headerFilter: Object,
        height: [Function, Number, String],
        highlightChanges: Boolean,
        hint: String,
        hoverStateEnabled: Boolean,
        itemsExpr: [Function, String],
        keyExpr: [Function, String],
        loadPanel: Object,
        noDataText: String,
        onAdaptiveDetailRowPreparing: Function,
        onCellClick: [Function, String],
        onCellHoverChanged: Function,
        onCellPrepared: Function,
        onContentReady: Function,
        onContextMenuPreparing: Function,
        onDataErrorOccurred: Function,
        onDisposing: Function,
        onEditingStart: Function,
        onEditorPrepared: Function,
        onEditorPreparing: Function,
        onFocusedCellChanged: Function,
        onFocusedCellChanging: Function,
        onFocusedRowChanged: Function,
        onFocusedRowChanging: Function,
        onInitialized: Function,
        onInitNewRow: Function,
        onKeyDown: Function,
        onNodesInitialized: Function,
        onOptionChanged: Function,
        onRowClick: [Function, String],
        onRowCollapsed: Function,
        onRowCollapsing: Function,
        onRowExpanded: Function,
        onRowExpanding: Function,
        onRowInserted: Function,
        onRowInserting: Function,
        onRowPrepared: Function,
        onRowRemoved: Function,
        onRowRemoving: Function,
        onRowUpdated: Function,
        onRowUpdating: Function,
        onRowValidating: Function,
        onSelectionChanged: Function,
        onToolbarPreparing: Function,
        pager: Object,
        paging: Object,
        parentIdExpr: [Function, String],
        remoteOperations: Object,
        renderAsync: Boolean,
        repaintChangesOnly: Boolean,
        rootValue: Object,
        rowAlternationEnabled: Boolean,
        rtlEnabled: Boolean,
        scrolling: Object,
        searchPanel: Object,
        selectedRowKeys: Array,
        selection: Object,
        showBorders: Boolean,
        showColumnHeaders: Boolean,
        showColumnLines: Boolean,
        showRowLines: Boolean,
        sorting: Object,
        stateStoring: Object,
        tabIndex: Number,
        twoWayBindingEnabled: Boolean,
        visible: Boolean,
        width: [Function, Number, String],
        wordWrapEnabled: Boolean
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = tree_list_1.default;
        this.$_expectedChildren = {
            column: { isCollectionItem: true, optionName: "columns" },
            columnChooser: { isCollectionItem: false, optionName: "columnChooser" },
            columnFixing: { isCollectionItem: false, optionName: "columnFixing" },
            editing: { isCollectionItem: false, optionName: "editing" },
            filterBuilder: { isCollectionItem: false, optionName: "filterBuilder" },
            filterBuilderPopup: { isCollectionItem: false, optionName: "filterBuilderPopup" },
            filterPanel: { isCollectionItem: false, optionName: "filterPanel" },
            filterRow: { isCollectionItem: false, optionName: "filterRow" },
            headerFilter: { isCollectionItem: false, optionName: "headerFilter" },
            loadPanel: { isCollectionItem: false, optionName: "loadPanel" },
            pager: { isCollectionItem: false, optionName: "pager" },
            paging: { isCollectionItem: false, optionName: "paging" },
            remoteOperations: { isCollectionItem: false, optionName: "remoteOperations" },
            scrolling: { isCollectionItem: false, optionName: "scrolling" },
            searchPanel: { isCollectionItem: false, optionName: "searchPanel" },
            selection: { isCollectionItem: false, optionName: "selection" },
            sorting: { isCollectionItem: false, optionName: "sorting" },
            stateStoring: { isCollectionItem: false, optionName: "stateStoring" },
            treeListHeaderFilter: { isCollectionItem: false, optionName: "headerFilter" }
        };
    }
});
exports.DxTreeList = DxTreeList;
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        hide: Object,
        show: Object
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
DxAnimation.$_expectedChildren = {
    hide: { isCollectionItem: false, optionName: "hide" },
    show: { isCollectionItem: false, optionName: "show" }
};
var DxAt = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxAt = DxAt;
DxAt.$_optionName = "at";
var DxBoundaryOffset = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: Number,
        y: Number
    }
});
exports.DxBoundaryOffset = DxBoundaryOffset;
DxBoundaryOffset.$_optionName = "boundaryOffset";
var DxButton = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        cssClass: String,
        hint: String,
        icon: String,
        name: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "add",
                "cancel",
                "delete",
                "edit",
                "save",
                "undelete"
            ].indexOf(v) !== -1; }
        },
        onClick: [Function, String],
        template: {},
        text: String,
        visible: [Boolean, Function]
    }
});
exports.DxButton = DxButton;
DxButton.$_optionName = "buttons";
DxButton.$_isCollectionItem = true;
var DxButtonItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        buttonOptions: Object,
        colSpan: Number,
        cssClass: String,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        name: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxButtonItem = DxButtonItem;
DxButtonItem.$_optionName = "items";
DxButtonItem.$_isCollectionItem = true;
DxButtonItem.$_predefinedProps = {
    itemType: "button"
};
DxButtonItem.$_expectedChildren = {
    buttonOptions: { isCollectionItem: false, optionName: "buttonOptions" }
};
var DxButtonOptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        bindingOptions: Object,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        icon: String,
        onClick: [Function, String],
        onContentReady: Function,
        onDisposing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        rtlEnabled: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "text",
                "outlined",
                "contained"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        template: {},
        text: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "back",
                "danger",
                "default",
                "normal",
                "success"
            ].indexOf(v) !== -1; }
        },
        useSubmitBehavior: Boolean,
        validationGroup: String,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxButtonOptions = DxButtonOptions;
DxButtonOptions.$_optionName = "buttonOptions";
var DxColCountByScreen = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        lg: Number,
        md: Number,
        sm: Number,
        xs: Number
    }
});
exports.DxColCountByScreen = DxColCountByScreen;
DxColCountByScreen.$_optionName = "colCountByScreen";
var DxCollision = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "flip",
                "flipfit",
                "none"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "flip",
                "flipfit",
                "none"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxCollision = DxCollision;
DxCollision.$_optionName = "collision";
var DxColumn = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        allowEditing: Boolean,
        allowFiltering: Boolean,
        allowFixing: Boolean,
        allowHeaderFiltering: Boolean,
        allowHiding: Boolean,
        allowReordering: Boolean,
        allowResizing: Boolean,
        allowSearch: Boolean,
        allowSorting: Boolean,
        buttons: Array,
        calculateCellValue: Function,
        calculateDisplayValue: [Function, String],
        calculateFilterExpression: Function,
        calculateSortValue: [Function, String],
        caption: String,
        cellTemplate: {},
        columns: Array,
        cssClass: String,
        customizeText: Function,
        dataField: String,
        dataType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "string",
                "number",
                "date",
                "boolean",
                "object",
                "datetime"
            ].indexOf(v) !== -1; }
        },
        editCellTemplate: {},
        editorOptions: Object,
        encodeHtml: Boolean,
        falseText: String,
        filterOperations: Array,
        filterType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "exclude",
                "include"
            ].indexOf(v) !== -1; }
        },
        filterValue: {},
        filterValues: Array,
        fixed: Boolean,
        fixedPosition: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        format: [Object, Function, String],
        formItem: Object,
        headerCellTemplate: {},
        headerFilter: Object,
        hidingPriority: Number,
        isBand: Boolean,
        lookup: Object,
        minWidth: Number,
        name: String,
        ownerBand: Number,
        renderAsync: Boolean,
        selectedFilterOperation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "<",
                "<=",
                "<>",
                "=",
                ">",
                ">=",
                "between",
                "contains",
                "endswith",
                "notcontains",
                "startswith"
            ].indexOf(v) !== -1; }
        },
        setCellValue: Function,
        showEditorAlways: Boolean,
        showInColumnChooser: Boolean,
        sortIndex: Number,
        sortingMethod: Function,
        sortOrder: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "asc",
                "desc"
            ].indexOf(v) !== -1; }
        },
        trueText: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "adaptive",
                "buttons"
            ].indexOf(v) !== -1; }
        },
        validationRules: Array,
        visible: Boolean,
        visibleIndex: Number,
        width: [Number, String]
    }
});
exports.DxColumn = DxColumn;
DxColumn.$_optionName = "columns";
DxColumn.$_isCollectionItem = true;
DxColumn.$_expectedChildren = {
    button: { isCollectionItem: true, optionName: "buttons" },
    columnHeaderFilter: { isCollectionItem: false, optionName: "headerFilter" },
    columnLookup: { isCollectionItem: false, optionName: "lookup" },
    CompareRule: { isCollectionItem: true, optionName: "validationRules" },
    CustomRule: { isCollectionItem: true, optionName: "validationRules" },
    EmailRule: { isCollectionItem: true, optionName: "validationRules" },
    format: { isCollectionItem: false, optionName: "format" },
    formItem: { isCollectionItem: false, optionName: "formItem" },
    headerFilter: { isCollectionItem: false, optionName: "headerFilter" },
    lookup: { isCollectionItem: false, optionName: "lookup" },
    NumericRule: { isCollectionItem: true, optionName: "validationRules" },
    PatternRule: { isCollectionItem: true, optionName: "validationRules" },
    RangeRule: { isCollectionItem: true, optionName: "validationRules" },
    RequiredRule: { isCollectionItem: true, optionName: "validationRules" },
    StringLengthRule: { isCollectionItem: true, optionName: "validationRules" },
    validationRule: { isCollectionItem: true, optionName: "validationRules" }
};
var DxColumnChooser = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowSearch: Boolean,
        emptyPanelText: String,
        enabled: Boolean,
        height: Number,
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dragAndDrop",
                "select"
            ].indexOf(v) !== -1; }
        },
        searchTimeout: Number,
        title: String,
        width: Number
    }
});
exports.DxColumnChooser = DxColumnChooser;
DxColumnChooser.$_optionName = "columnChooser";
var DxColumnFixing = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        enabled: Boolean,
        texts: Object
    }
});
exports.DxColumnFixing = DxColumnFixing;
DxColumnFixing.$_optionName = "columnFixing";
DxColumnFixing.$_expectedChildren = {
    columnFixingTexts: { isCollectionItem: false, optionName: "texts" },
    texts: { isCollectionItem: false, optionName: "texts" }
};
var DxColumnFixingTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        fix: String,
        leftPosition: String,
        rightPosition: String,
        unfix: String
    }
});
exports.DxColumnFixingTexts = DxColumnFixingTexts;
DxColumnFixingTexts.$_optionName = "texts";
var DxColumnHeaderFilter = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowSearch: Boolean,
        dataSource: [Array, Object, Function],
        groupInterval: {
            type: [Number, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "minute",
                "month",
                "quarter",
                "second",
                "year"
            ].indexOf(v) !== -1; }
        },
        height: Number,
        searchMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "contains",
                "startswith",
                "equals"
            ].indexOf(v) !== -1; }
        },
        width: Number
    }
});
exports.DxColumnHeaderFilter = DxColumnHeaderFilter;
DxColumnHeaderFilter.$_optionName = "headerFilter";
var DxColumnLookup = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowClearing: Boolean,
        dataSource: [Array, Object, Function],
        displayExpr: [Function, String],
        valueExpr: String
    }
});
exports.DxColumnLookup = DxColumnLookup;
DxColumnLookup.$_optionName = "lookup";
var DxCompareRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        comparisonTarget: Function,
        comparisonType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "!=",
                "!==",
                "<",
                "<=",
                "==",
                "===",
                ">",
                ">="
            ].indexOf(v) !== -1; }
        },
        ignoreEmptyValue: Boolean,
        message: String,
        reevaluate: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxCompareRule = DxCompareRule;
DxCompareRule.$_optionName = "validationRules";
DxCompareRule.$_isCollectionItem = true;
DxCompareRule.$_predefinedProps = {
    type: "compare"
};
var DxCustomOperation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        calculateFilterExpression: Function,
        caption: String,
        customizeText: Function,
        dataTypes: Array,
        editorTemplate: {},
        hasValue: Boolean,
        icon: String,
        name: String
    }
});
exports.DxCustomOperation = DxCustomOperation;
DxCustomOperation.$_optionName = "customOperations";
DxCustomOperation.$_isCollectionItem = true;
var DxCustomRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        reevaluate: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        },
        validationCallback: Function
    }
});
exports.DxCustomRule = DxCustomRule;
DxCustomRule.$_optionName = "validationRules";
DxCustomRule.$_isCollectionItem = true;
DxCustomRule.$_predefinedProps = {
    type: "custom"
};
var DxEditing = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowAdding: [Boolean, Function],
        allowDeleting: [Boolean, Function],
        allowUpdating: [Boolean, Function],
        form: Object,
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "batch",
                "cell",
                "row",
                "form",
                "popup"
            ].indexOf(v) !== -1; }
        },
        popup: Object,
        refreshMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "full",
                "reshape",
                "repaint"
            ].indexOf(v) !== -1; }
        },
        texts: Object,
        useIcons: Boolean
    }
});
exports.DxEditing = DxEditing;
DxEditing.$_optionName = "editing";
DxEditing.$_expectedChildren = {
    editingTexts: { isCollectionItem: false, optionName: "texts" },
    form: { isCollectionItem: false, optionName: "form" },
    popup: { isCollectionItem: false, optionName: "popup" },
    texts: { isCollectionItem: false, optionName: "texts" }
};
var DxEditingTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        addRow: String,
        addRowToNode: String,
        cancelAllChanges: String,
        cancelRowChanges: String,
        confirmDeleteMessage: String,
        confirmDeleteTitle: String,
        deleteRow: String,
        editRow: String,
        saveAllChanges: String,
        saveRowChanges: String,
        undeleteRow: String,
        validationCancelChanges: String
    }
});
exports.DxEditingTexts = DxEditingTexts;
DxEditingTexts.$_optionName = "texts";
var DxEmailRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxEmailRule = DxEmailRule;
DxEmailRule.$_optionName = "validationRules";
DxEmailRule.$_isCollectionItem = true;
DxEmailRule.$_predefinedProps = {
    type: "email"
};
var DxEmptyItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        colSpan: Number,
        cssClass: String,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        name: String,
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxEmptyItem = DxEmptyItem;
DxEmptyItem.$_optionName = "items";
DxEmptyItem.$_isCollectionItem = true;
DxEmptyItem.$_predefinedProps = {
    itemType: "empty"
};
var DxField = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        calculateFilterExpression: Function,
        caption: String,
        customizeText: Function,
        dataField: String,
        dataType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "string",
                "number",
                "date",
                "boolean",
                "object",
                "datetime"
            ].indexOf(v) !== -1; }
        },
        defaultFilterOperation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "=",
                "<>",
                "<",
                "<=",
                ">",
                ">=",
                "contains",
                "endswith",
                "isblank",
                "isnotblank",
                "notcontains",
                "startswith",
                "between"
            ].indexOf(v) !== -1; }
        },
        editorOptions: Object,
        editorTemplate: {},
        falseText: String,
        filterOperations: Array,
        format: [Object, Function, String],
        lookup: Object,
        trueText: String
    }
});
exports.DxField = DxField;
DxField.$_optionName = "fields";
DxField.$_isCollectionItem = true;
DxField.$_expectedChildren = {
    fieldLookup: { isCollectionItem: false, optionName: "lookup" },
    format: { isCollectionItem: false, optionName: "format" },
    lookup: { isCollectionItem: false, optionName: "lookup" }
};
var DxFieldLookup = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowClearing: Boolean,
        dataSource: [Array, Object],
        displayExpr: [Function, String],
        valueExpr: [Function, String]
    }
});
exports.DxFieldLookup = DxFieldLookup;
DxFieldLookup.$_optionName = "lookup";
var DxFilterBuilder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        allowHierarchicalFields: Boolean,
        bindingOptions: Object,
        customOperations: Array,
        defaultGroupOperation: String,
        disabled: Boolean,
        elementAttr: Object,
        fields: Array,
        filterOperationDescriptions: Object,
        focusStateEnabled: Boolean,
        groupOperationDescriptions: Object,
        groupOperations: Array,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        maxGroupLevel: Number,
        onContentReady: Function,
        onDisposing: Function,
        onEditorPrepared: Function,
        onEditorPreparing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onValueChanged: Function,
        rtlEnabled: Boolean,
        tabIndex: Number,
        value: [Array, Function, String],
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxFilterBuilder = DxFilterBuilder;
DxFilterBuilder.$_optionName = "filterBuilder";
DxFilterBuilder.$_expectedChildren = {
    customOperation: { isCollectionItem: true, optionName: "customOperations" },
    field: { isCollectionItem: true, optionName: "fields" },
    filterOperationDescriptions: { isCollectionItem: false, optionName: "filterOperationDescriptions" },
    groupOperationDescriptions: { isCollectionItem: false, optionName: "groupOperationDescriptions" }
};
var DxFilterBuilderPopup = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        animation: Object,
        bindingOptions: Object,
        closeOnBackButton: Boolean,
        closeOnOutsideClick: [Boolean, Function],
        container: {},
        contentTemplate: {},
        deferRendering: Boolean,
        disabled: Boolean,
        dragEnabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        fullScreen: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        maxHeight: [Function, Number, String],
        maxWidth: [Function, Number, String],
        minHeight: [Function, Number, String],
        minWidth: [Function, Number, String],
        onContentReady: Function,
        onDisposing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onHidden: Function,
        onHiding: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onResize: Function,
        onResizeEnd: Function,
        onResizeStart: Function,
        onShowing: Function,
        onShown: Function,
        onTitleRendered: Function,
        position: {
            type: [Function, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        resizeEnabled: Boolean,
        rtlEnabled: Boolean,
        shading: Boolean,
        shadingColor: String,
        showCloseButton: Boolean,
        showTitle: Boolean,
        tabIndex: Number,
        title: String,
        titleTemplate: {},
        toolbarItems: Array,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxFilterBuilderPopup = DxFilterBuilderPopup;
DxFilterBuilderPopup.$_optionName = "filterBuilderPopup";
var DxFilterOperationDescriptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        between: String,
        contains: String,
        endsWith: String,
        equal: String,
        greaterThan: String,
        greaterThanOrEqual: String,
        isBlank: String,
        isNotBlank: String,
        lessThan: String,
        lessThanOrEqual: String,
        notContains: String,
        notEqual: String,
        startsWith: String
    }
});
exports.DxFilterOperationDescriptions = DxFilterOperationDescriptions;
DxFilterOperationDescriptions.$_optionName = "filterOperationDescriptions";
var DxFilterPanel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeText: Function,
        filterEnabled: Boolean,
        texts: Object,
        visible: Boolean
    }
});
exports.DxFilterPanel = DxFilterPanel;
DxFilterPanel.$_optionName = "filterPanel";
DxFilterPanel.$_expectedChildren = {
    filterPanelTexts: { isCollectionItem: false, optionName: "texts" },
    texts: { isCollectionItem: false, optionName: "texts" }
};
var DxFilterPanelTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        clearFilter: String,
        createFilter: String,
        filterEnabledHint: String
    }
});
exports.DxFilterPanelTexts = DxFilterPanelTexts;
DxFilterPanelTexts.$_optionName = "texts";
var DxFilterRow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        applyFilter: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "onClick"
            ].indexOf(v) !== -1; }
        },
        applyFilterText: String,
        betweenEndText: String,
        betweenStartText: String,
        operationDescriptions: Object,
        resetOperationText: String,
        showAllText: String,
        showOperationChooser: Boolean,
        visible: Boolean
    }
});
exports.DxFilterRow = DxFilterRow;
DxFilterRow.$_optionName = "filterRow";
DxFilterRow.$_expectedChildren = {
    operationDescriptions: { isCollectionItem: false, optionName: "operationDescriptions" }
};
var DxForm = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        alignItemLabels: Boolean,
        alignItemLabelsInAllGroups: Boolean,
        bindingOptions: Object,
        colCount: {
            type: [Number, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "auto"
            ].indexOf(v) !== -1; }
        },
        colCountByScreen: Object,
        customizeItem: Function,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        formData: Object,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        items: Array,
        labelLocation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        minColWidth: Number,
        onContentReady: Function,
        onDisposing: Function,
        onEditorEnterKey: Function,
        onFieldDataChanged: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        optionalMark: String,
        readOnly: Boolean,
        requiredMark: String,
        requiredMessage: String,
        rtlEnabled: Boolean,
        screenByWidth: Function,
        scrollingEnabled: Boolean,
        showColonAfterLabel: Boolean,
        showOptionalMark: Boolean,
        showRequiredMark: Boolean,
        showValidationSummary: Boolean,
        tabIndex: Number,
        validationGroup: String,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxForm = DxForm;
DxForm.$_optionName = "form";
DxForm.$_expectedChildren = {
    ButtonItem: { isCollectionItem: true, optionName: "items" },
    colCountByScreen: { isCollectionItem: false, optionName: "colCountByScreen" },
    EmptyItem: { isCollectionItem: true, optionName: "items" },
    GroupItem: { isCollectionItem: true, optionName: "items" },
    item: { isCollectionItem: true, optionName: "items" },
    SimpleItem: { isCollectionItem: true, optionName: "items" },
    TabbedItem: { isCollectionItem: true, optionName: "items" }
};
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxFormItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        colSpan: Number,
        cssClass: String,
        dataField: String,
        editorOptions: Object,
        editorType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dxAutocomplete",
                "dxCalendar",
                "dxCheckBox",
                "dxColorBox",
                "dxDateBox",
                "dxDropDownBox",
                "dxLookup",
                "dxNumberBox",
                "dxRadioGroup",
                "dxRangeSlider",
                "dxSelectBox",
                "dxSlider",
                "dxSwitch",
                "dxTagBox",
                "dxTextArea",
                "dxTextBox"
            ].indexOf(v) !== -1; }
        },
        helpText: String,
        isRequired: Boolean,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        name: String,
        template: {},
        validationRules: Array,
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxFormItem = DxFormItem;
DxFormItem.$_optionName = "formItem";
DxFormItem.$_expectedChildren = {
    CompareRule: { isCollectionItem: true, optionName: "validationRules" },
    CustomRule: { isCollectionItem: true, optionName: "validationRules" },
    EmailRule: { isCollectionItem: true, optionName: "validationRules" },
    label: { isCollectionItem: false, optionName: "label" },
    NumericRule: { isCollectionItem: true, optionName: "validationRules" },
    PatternRule: { isCollectionItem: true, optionName: "validationRules" },
    RangeRule: { isCollectionItem: true, optionName: "validationRules" },
    RequiredRule: { isCollectionItem: true, optionName: "validationRules" },
    StringLengthRule: { isCollectionItem: true, optionName: "validationRules" },
    validationRule: { isCollectionItem: true, optionName: "validationRules" }
};
var DxGroupItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignItemLabels: Boolean,
        caption: String,
        colCount: Number,
        colCountByScreen: Object,
        colSpan: Number,
        cssClass: String,
        items: Array,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        name: String,
        template: {},
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxGroupItem = DxGroupItem;
DxGroupItem.$_optionName = "items";
DxGroupItem.$_isCollectionItem = true;
DxGroupItem.$_predefinedProps = {
    itemType: "group"
};
DxGroupItem.$_expectedChildren = {
    colCountByScreen: { isCollectionItem: false, optionName: "colCountByScreen" }
};
var DxGroupOperationDescriptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        and: String,
        notAnd: String,
        notOr: String,
        or: String
    }
});
exports.DxGroupOperationDescriptions = DxGroupOperationDescriptions;
DxGroupOperationDescriptions.$_optionName = "groupOperationDescriptions";
var DxHeaderFilter = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowSearch: Boolean,
        dataSource: [Array, Object, Function],
        groupInterval: {
            type: [Number, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "minute",
                "month",
                "quarter",
                "second",
                "year"
            ].indexOf(v) !== -1; }
        },
        height: Number,
        searchMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "contains",
                "startswith",
                "equals"
            ].indexOf(v) !== -1; }
        },
        searchTimeout: Number,
        texts: Object,
        visible: Boolean,
        width: Number
    }
});
exports.DxHeaderFilter = DxHeaderFilter;
DxHeaderFilter.$_optionName = "headerFilter";
var DxHide = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxHide = DxHide;
DxHide.$_optionName = "hide";
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignItemLabels: Boolean,
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        badge: String,
        buttonOptions: Object,
        caption: String,
        colCount: Number,
        colCountByScreen: Object,
        colSpan: Number,
        cssClass: String,
        dataField: String,
        disabled: Boolean,
        editorOptions: Object,
        editorType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dxAutocomplete",
                "dxCalendar",
                "dxCheckBox",
                "dxColorBox",
                "dxDateBox",
                "dxDropDownBox",
                "dxLookup",
                "dxNumberBox",
                "dxRadioGroup",
                "dxRangeSlider",
                "dxSelectBox",
                "dxSlider",
                "dxSwitch",
                "dxTagBox",
                "dxTextArea",
                "dxTextBox"
            ].indexOf(v) !== -1; }
        },
        helpText: String,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        html: String,
        icon: String,
        isRequired: Boolean,
        items: Array,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        name: String,
        tabPanelOptions: Object,
        tabs: Array,
        tabTemplate: {},
        template: {},
        text: String,
        title: String,
        validationRules: Array,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        location: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        showColon: Boolean,
        text: String,
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
var DxLoadPanel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        enabled: Boolean,
        height: Number,
        indicatorSrc: String,
        shading: Boolean,
        shadingColor: String,
        showIndicator: Boolean,
        showPane: Boolean,
        text: String,
        width: Number
    }
});
exports.DxLoadPanel = DxLoadPanel;
DxLoadPanel.$_optionName = "loadPanel";
var DxLookup = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowClearing: Boolean,
        dataSource: [Array, Object, Function],
        displayExpr: [Function, String],
        valueExpr: String
    }
});
exports.DxLookup = DxLookup;
DxLookup.$_optionName = "lookup";
var DxMy = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxMy = DxMy;
DxMy.$_optionName = "my";
var DxNumericRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxNumericRule = DxNumericRule;
DxNumericRule.$_optionName = "validationRules";
DxNumericRule.$_isCollectionItem = true;
DxNumericRule.$_predefinedProps = {
    type: "numeric"
};
var DxOffset = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: Number,
        y: Number
    }
});
exports.DxOffset = DxOffset;
DxOffset.$_optionName = "offset";
var DxOperationDescriptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        between: String,
        contains: String,
        endsWith: String,
        equal: String,
        greaterThan: String,
        greaterThanOrEqual: String,
        lessThan: String,
        lessThanOrEqual: String,
        notContains: String,
        notEqual: String,
        startsWith: String
    }
});
exports.DxOperationDescriptions = DxOperationDescriptions;
DxOperationDescriptions.$_optionName = "operationDescriptions";
var DxPager = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowedPageSizes: Array,
        infoText: String,
        showInfo: Boolean,
        showNavigationButtons: Boolean,
        showPageSizeSelector: Boolean,
        visible: Boolean
    }
});
exports.DxPager = DxPager;
DxPager.$_optionName = "pager";
var DxPaging = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        enabled: Boolean,
        pageIndex: Number,
        pageSize: Number
    }
});
exports.DxPaging = DxPaging;
DxPaging.$_optionName = "paging";
var DxPatternRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        pattern: {},
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxPatternRule = DxPatternRule;
DxPatternRule.$_optionName = "validationRules";
DxPatternRule.$_isCollectionItem = true;
DxPatternRule.$_predefinedProps = {
    type: "pattern"
};
var DxPopup = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        animation: Object,
        bindingOptions: Object,
        closeOnBackButton: Boolean,
        closeOnOutsideClick: [Boolean, Function],
        container: {},
        contentTemplate: {},
        deferRendering: Boolean,
        disabled: Boolean,
        dragEnabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        fullScreen: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        maxHeight: [Function, Number, String],
        maxWidth: [Function, Number, String],
        minHeight: [Function, Number, String],
        minWidth: [Function, Number, String],
        onContentReady: Function,
        onDisposing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onHidden: Function,
        onHiding: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onResize: Function,
        onResizeEnd: Function,
        onResizeStart: Function,
        onShowing: Function,
        onShown: Function,
        onTitleRendered: Function,
        position: {
            type: [Function, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        resizeEnabled: Boolean,
        rtlEnabled: Boolean,
        shading: Boolean,
        shadingColor: String,
        showCloseButton: Boolean,
        showTitle: Boolean,
        tabIndex: Number,
        title: String,
        titleTemplate: {},
        toolbarItems: Array,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxPopup = DxPopup;
DxPopup.$_optionName = "popup";
DxPopup.$_expectedChildren = {
    animation: { isCollectionItem: false, optionName: "animation" },
    position: { isCollectionItem: false, optionName: "position" },
    toolbarItem: { isCollectionItem: true, optionName: "toolbarItems" }
};
var DxPosition = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        at: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        boundary: {},
        boundaryOffset: [Object, String],
        collision: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "fit flip",
                "fit flipfit",
                "fit none",
                "flip",
                "flip fit",
                "flip none",
                "flipfit",
                "flipfit fit",
                "flipfit none",
                "none",
                "none fit",
                "none flip",
                "none flipfit"
            ].indexOf(v) !== -1; }
        },
        my: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        of: {},
        offset: [Object, String]
    }
});
exports.DxPosition = DxPosition;
DxPosition.$_optionName = "position";
DxPosition.$_expectedChildren = {
    at: { isCollectionItem: false, optionName: "at" },
    boundaryOffset: { isCollectionItem: false, optionName: "boundaryOffset" },
    collision: { isCollectionItem: false, optionName: "collision" },
    my: { isCollectionItem: false, optionName: "my" },
    offset: { isCollectionItem: false, optionName: "offset" }
};
var DxRangeRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        max: {},
        message: String,
        min: {},
        reevaluate: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxRangeRule = DxRangeRule;
DxRangeRule.$_optionName = "validationRules";
DxRangeRule.$_isCollectionItem = true;
DxRangeRule.$_predefinedProps = {
    type: "range"
};
var DxRemoteOperations = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        filtering: Boolean,
        grouping: Boolean,
        sorting: Boolean
    }
});
exports.DxRemoteOperations = DxRemoteOperations;
DxRemoteOperations.$_optionName = "remoteOperations";
var DxRequiredRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        message: String,
        trim: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxRequiredRule = DxRequiredRule;
DxRequiredRule.$_optionName = "validationRules";
DxRequiredRule.$_isCollectionItem = true;
DxRequiredRule.$_predefinedProps = {
    type: "required"
};
var DxScrolling = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        columnRenderingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "standard",
                "virtual"
            ].indexOf(v) !== -1; }
        },
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "standard",
                "virtual"
            ].indexOf(v) !== -1; }
        },
        preloadEnabled: Boolean,
        rowRenderingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "standard",
                "virtual"
            ].indexOf(v) !== -1; }
        },
        scrollByContent: Boolean,
        scrollByThumb: Boolean,
        showScrollbar: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "never",
                "onHover",
                "onScroll"
            ].indexOf(v) !== -1; }
        },
        useNative: Boolean
    }
});
exports.DxScrolling = DxScrolling;
DxScrolling.$_optionName = "scrolling";
var DxSearchPanel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        highlightCaseSensitive: Boolean,
        highlightSearchText: Boolean,
        placeholder: String,
        searchVisibleColumnsOnly: Boolean,
        text: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxSearchPanel = DxSearchPanel;
DxSearchPanel.$_optionName = "searchPanel";
var DxSelection = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowSelectAll: Boolean,
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "none",
                "single"
            ].indexOf(v) !== -1; }
        },
        recursive: Boolean
    }
});
exports.DxSelection = DxSelection;
DxSelection.$_optionName = "selection";
var DxShow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxShow = DxShow;
DxShow.$_optionName = "show";
var DxSimpleItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        colSpan: Number,
        cssClass: String,
        dataField: String,
        editorOptions: Object,
        editorType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dxAutocomplete",
                "dxCalendar",
                "dxCheckBox",
                "dxColorBox",
                "dxDateBox",
                "dxDropDownBox",
                "dxLookup",
                "dxNumberBox",
                "dxRadioGroup",
                "dxRangeSlider",
                "dxSelectBox",
                "dxSlider",
                "dxSwitch",
                "dxTagBox",
                "dxTextArea",
                "dxTextBox"
            ].indexOf(v) !== -1; }
        },
        helpText: String,
        isRequired: Boolean,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        name: String,
        template: {},
        validationRules: Array,
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxSimpleItem = DxSimpleItem;
DxSimpleItem.$_optionName = "items";
DxSimpleItem.$_isCollectionItem = true;
DxSimpleItem.$_predefinedProps = {
    itemType: "simple"
};
var DxSorting = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ascendingText: String,
        clearText: String,
        descendingText: String,
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "none",
                "single"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxSorting = DxSorting;
DxSorting.$_optionName = "sorting";
var DxStateStoring = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customLoad: Function,
        customSave: Function,
        enabled: Boolean,
        savingTimeout: Number,
        storageKey: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "custom",
                "localStorage",
                "sessionStorage"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxStateStoring = DxStateStoring;
DxStateStoring.$_optionName = "stateStoring";
var DxStringLengthRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        max: Number,
        message: String,
        min: Number,
        trim: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxStringLengthRule = DxStringLengthRule;
DxStringLengthRule.$_optionName = "validationRules";
DxStringLengthRule.$_isCollectionItem = true;
DxStringLengthRule.$_predefinedProps = {
    type: "stringLength"
};
var DxTab = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignItemLabels: Boolean,
        badge: String,
        colCount: Number,
        colCountByScreen: Object,
        disabled: Boolean,
        icon: String,
        items: Array,
        tabTemplate: {},
        template: {},
        title: String
    }
});
exports.DxTab = DxTab;
DxTab.$_optionName = "tabs";
DxTab.$_isCollectionItem = true;
DxTab.$_expectedChildren = {
    colCountByScreen: { isCollectionItem: false, optionName: "colCountByScreen" }
};
var DxTabbedItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        colSpan: Number,
        cssClass: String,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        name: String,
        tabPanelOptions: Object,
        tabs: Array,
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxTabbedItem = DxTabbedItem;
DxTabbedItem.$_optionName = "items";
DxTabbedItem.$_isCollectionItem = true;
DxTabbedItem.$_predefinedProps = {
    itemType: "tabbed"
};
DxTabbedItem.$_expectedChildren = {
    tab: { isCollectionItem: true, optionName: "tabs" },
    tabPanelOptions: { isCollectionItem: false, optionName: "tabPanelOptions" }
};
var DxTabPanelOptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        animationEnabled: Boolean,
        bindingOptions: Object,
        dataSource: [Array, Object, String],
        deferRendering: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusedElement: {},
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        itemHoldTimeout: Number,
        items: Array,
        itemTemplate: {},
        itemTitleTemplate: {},
        keyExpr: [Function, String],
        loop: Boolean,
        loopItemFocus: Boolean,
        noDataText: String,
        onContentReady: Function,
        onDisposing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemDeleted: Function,
        onItemDeleting: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onItemReordered: Function,
        onOptionChanged: Function,
        onSelectionChanged: Function,
        onTitleClick: [Function, String],
        onTitleHold: Function,
        onTitleRendered: Function,
        repaintChangesOnly: Boolean,
        rtlEnabled: Boolean,
        scrollByContent: Boolean,
        scrollingEnabled: Boolean,
        selectedIndex: Number,
        selectedItem: Object,
        selectedItemKeys: Array,
        selectedItems: Array,
        selectionByClick: Boolean,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "single",
                "all",
                "none"
            ].indexOf(v) !== -1; }
        },
        selectionRequired: Boolean,
        selectOnFocus: Boolean,
        showNavButtons: Boolean,
        swipeEnabled: Boolean,
        tabIndex: Number,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxTabPanelOptions = DxTabPanelOptions;
DxTabPanelOptions.$_optionName = "tabPanelOptions";
DxTabPanelOptions.$_expectedChildren = {
    item: { isCollectionItem: true, optionName: "items" },
    tabPanelOptionsItem: { isCollectionItem: true, optionName: "items" }
};
var DxTabPanelOptionsItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        badge: String,
        disabled: Boolean,
        html: String,
        icon: String,
        tabTemplate: {},
        template: {},
        text: String,
        title: String,
        visible: Boolean
    }
});
exports.DxTabPanelOptionsItem = DxTabPanelOptionsItem;
DxTabPanelOptionsItem.$_optionName = "items";
DxTabPanelOptionsItem.$_isCollectionItem = true;
var DxTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        addRow: String,
        addRowToNode: String,
        cancel: String,
        cancelAllChanges: String,
        cancelRowChanges: String,
        clearFilter: String,
        confirmDeleteMessage: String,
        confirmDeleteTitle: String,
        createFilter: String,
        deleteRow: String,
        editRow: String,
        emptyValue: String,
        filterEnabledHint: String,
        fix: String,
        leftPosition: String,
        ok: String,
        rightPosition: String,
        saveAllChanges: String,
        saveRowChanges: String,
        undeleteRow: String,
        unfix: String,
        validationCancelChanges: String
    }
});
exports.DxTexts = DxTexts;
DxTexts.$_optionName = "texts";
var DxToolbarItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        html: String,
        location: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "after",
                "before",
                "center"
            ].indexOf(v) !== -1; }
        },
        options: Object,
        template: {},
        text: String,
        toolbar: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        widget: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dxAutocomplete",
                "dxButton",
                "dxCheckBox",
                "dxDateBox",
                "dxMenu",
                "dxSelectBox",
                "dxTabs",
                "dxTextBox",
                "dxButtonGroup"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxToolbarItem = DxToolbarItem;
DxToolbarItem.$_optionName = "toolbarItems";
DxToolbarItem.$_isCollectionItem = true;
var DxTreeListHeaderFilter = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowSearch: Boolean,
        height: Number,
        searchTimeout: Number,
        texts: Object,
        visible: Boolean,
        width: Number
    }
});
exports.DxTreeListHeaderFilter = DxTreeListHeaderFilter;
DxTreeListHeaderFilter.$_optionName = "headerFilter";
DxTreeListHeaderFilter.$_expectedChildren = {
    texts: { isCollectionItem: false, optionName: "texts" },
    treeListHeaderFilterTexts: { isCollectionItem: false, optionName: "texts" }
};
var DxTreeListHeaderFilterTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        cancel: String,
        emptyValue: String,
        ok: String
    }
});
exports.DxTreeListHeaderFilterTexts = DxTreeListHeaderFilterTexts;
DxTreeListHeaderFilterTexts.$_optionName = "texts";
var DxValidationRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        comparisonTarget: Function,
        comparisonType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "!=",
                "!==",
                "<",
                "<=",
                "==",
                "===",
                ">",
                ">="
            ].indexOf(v) !== -1; }
        },
        ignoreEmptyValue: Boolean,
        max: {},
        message: String,
        min: {},
        pattern: {},
        reevaluate: Boolean,
        trim: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        },
        validationCallback: Function
    }
});
exports.DxValidationRule = DxValidationRule;
DxValidationRule.$_optionName = "validationRules";
DxValidationRule.$_isCollectionItem = true;
DxValidationRule.$_predefinedProps = {
    type: "required"
};
exports.default = DxTreeList;
