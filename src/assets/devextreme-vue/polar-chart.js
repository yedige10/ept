/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var polar_chart_1 = require("devextreme/viz/polar_chart");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxPolarChart = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        adaptiveLayout: Object,
        animation: [Boolean, Object],
        argumentAxis: Object,
        barGroupPadding: Number,
        barGroupWidth: Number,
        commonAxisSettings: Object,
        commonSeriesSettings: Object,
        containerBackgroundColor: String,
        customizeLabel: Function,
        customizePoint: Function,
        dataPrepareSettings: Object,
        dataSource: [Array, Object, String],
        disabled: Boolean,
        elementAttr: Object,
        export: Object,
        legend: Object,
        loadingIndicator: Object,
        margin: Object,
        negativesAsZeroes: Boolean,
        onArgumentAxisClick: [Function, String],
        onDisposing: Function,
        onDone: Function,
        onDrawn: Function,
        onExported: Function,
        onExporting: Function,
        onFileSaving: Function,
        onIncidentOccurred: Function,
        onInitialized: Function,
        onLegendClick: [Function, String],
        onOptionChanged: Function,
        onPointClick: [Function, String],
        onPointHoverChanged: Function,
        onPointSelectionChanged: Function,
        onSeriesClick: [Function, String],
        onSeriesHoverChanged: Function,
        onSeriesSelectionChanged: Function,
        onTooltipHidden: Function,
        onTooltipShown: Function,
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        paletteExtensionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "alternate",
                "blend",
                "extrapolate"
            ].indexOf(v) !== -1; }
        },
        pathModified: Boolean,
        pointSelectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "single"
            ].indexOf(v) !== -1; }
        },
        redrawOnResize: Boolean,
        resolveLabelOverlapping: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "hide",
                "none"
            ].indexOf(v) !== -1; }
        },
        rtlEnabled: Boolean,
        series: [Array, Object],
        seriesSelectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "single"
            ].indexOf(v) !== -1; }
        },
        seriesTemplate: Object,
        size: Object,
        theme: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "android5.light",
                "generic.dark",
                "generic.light",
                "generic.contrast",
                "ios7.default",
                "win10.black",
                "win10.white",
                "win8.black",
                "win8.white",
                "generic.carmine",
                "generic.darkmoon",
                "generic.darkviolet",
                "generic.greenmist",
                "generic.softblue",
                "material.blue.light",
                "material.lime.light",
                "material.orange.light",
                "material.purple.light",
                "material.teal.light"
            ].indexOf(v) !== -1; }
        },
        title: [Object, String],
        tooltip: Object,
        useSpiderWeb: Boolean,
        valueAxis: Object
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = polar_chart_1.default;
        this.$_expectedChildren = {
            adaptiveLayout: { isCollectionItem: false, optionName: "adaptiveLayout" },
            animation: { isCollectionItem: false, optionName: "animation" },
            argumentAxis: { isCollectionItem: false, optionName: "argumentAxis" },
            commonAxisSettings: { isCollectionItem: false, optionName: "commonAxisSettings" },
            commonSeriesSettings: { isCollectionItem: false, optionName: "commonSeriesSettings" },
            dataPrepareSettings: { isCollectionItem: false, optionName: "dataPrepareSettings" },
            export: { isCollectionItem: false, optionName: "export" },
            legend: { isCollectionItem: false, optionName: "legend" },
            loadingIndicator: { isCollectionItem: false, optionName: "loadingIndicator" },
            margin: { isCollectionItem: false, optionName: "margin" },
            series: { isCollectionItem: true, optionName: "series" },
            seriesTemplate: { isCollectionItem: false, optionName: "seriesTemplate" },
            size: { isCollectionItem: false, optionName: "size" },
            title: { isCollectionItem: false, optionName: "title" },
            tooltip: { isCollectionItem: false, optionName: "tooltip" },
            valueAxis: { isCollectionItem: false, optionName: "valueAxis" }
        };
    }
});
exports.DxPolarChart = DxPolarChart;
var DxAdaptiveLayout = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        keepLabels: Boolean,
        width: Number
    }
});
exports.DxAdaptiveLayout = DxAdaptiveLayout;
DxAdaptiveLayout.$_optionName = "adaptiveLayout";
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        duration: Number,
        easing: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "easeOutCubic",
                "linear"
            ].indexOf(v) !== -1; }
        },
        enabled: Boolean,
        maxPointCountSupported: Number
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
var DxArgumentAxis = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowDecimals: Boolean,
        argumentType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "datetime",
                "numeric",
                "string"
            ].indexOf(v) !== -1; }
        },
        axisDivisionFactor: Number,
        categories: Array,
        color: String,
        constantLines: Array,
        constantLineStyle: Object,
        discreteAxisDivisionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "betweenLabels",
                "crossLabels"
            ].indexOf(v) !== -1; }
        },
        endOnTick: Boolean,
        firstPointOnStartAngle: Boolean,
        grid: Object,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "none"
            ].indexOf(v) !== -1; }
        },
        inverted: Boolean,
        label: Object,
        logarithmBase: Number,
        minorGrid: Object,
        minorTick: Object,
        minorTickCount: Number,
        minorTickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        originValue: Number,
        period: Number,
        startAngle: Number,
        strips: Array,
        stripStyle: Object,
        tick: Object,
        tickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "continuous",
                "discrete",
                "logarithmic"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        width: Number
    }
});
exports.DxArgumentAxis = DxArgumentAxis;
DxArgumentAxis.$_optionName = "argumentAxis";
DxArgumentAxis.$_expectedChildren = {
    axisLabel: { isCollectionItem: false, optionName: "label" },
    constantLine: { isCollectionItem: true, optionName: "constantLines" },
    label: { isCollectionItem: false, optionName: "label" },
    minorTickInterval: { isCollectionItem: false, optionName: "minorTickInterval" },
    strip: { isCollectionItem: true, optionName: "strips" },
    tickInterval: { isCollectionItem: false, optionName: "tickInterval" }
};
var DxArgumentFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxArgumentFormat = DxArgumentFormat;
DxArgumentFormat.$_optionName = "argumentFormat";
var DxAxisLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeHint: Function,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        indentFromAxis: Number,
        overlappingBehavior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "hide"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxAxisLabel = DxAxisLabel;
DxAxisLabel.$_optionName = "label";
var DxBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxBorder = DxBorder;
DxBorder.$_optionName = "border";
var DxCommonAxisSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowDecimals: Boolean,
        color: String,
        constantLineStyle: Object,
        discreteAxisDivisionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "betweenLabels",
                "crossLabels"
            ].indexOf(v) !== -1; }
        },
        endOnTick: Boolean,
        grid: Object,
        inverted: Boolean,
        label: Object,
        minorGrid: Object,
        minorTick: Object,
        opacity: Number,
        stripStyle: Object,
        tick: Object,
        visible: Boolean,
        width: Number
    }
});
exports.DxCommonAxisSettings = DxCommonAxisSettings;
DxCommonAxisSettings.$_optionName = "commonAxisSettings";
DxCommonAxisSettings.$_expectedChildren = {
    commonAxisSettingsLabel: { isCollectionItem: false, optionName: "label" },
    constantLineStyle: { isCollectionItem: false, optionName: "constantLineStyle" },
    grid: { isCollectionItem: false, optionName: "grid" },
    label: { isCollectionItem: false, optionName: "label" },
    minorGrid: { isCollectionItem: false, optionName: "minorGrid" },
    minorTick: { isCollectionItem: false, optionName: "minorTick" },
    stripStyle: { isCollectionItem: false, optionName: "stripStyle" },
    tick: { isCollectionItem: false, optionName: "tick" }
};
var DxCommonAxisSettingsLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        indentFromAxis: Number,
        overlappingBehavior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "hide"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxCommonAxisSettingsLabel = DxCommonAxisSettingsLabel;
DxCommonAxisSettingsLabel.$_optionName = "label";
DxCommonAxisSettingsLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxCommonSeriesSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        area: Object,
        argumentField: String,
        axis: String,
        bar: Object,
        barPadding: Number,
        barWidth: Number,
        border: Object,
        closed: Boolean,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "nearestPoint",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        ignoreEmptyPoints: Boolean,
        label: Object,
        line: Object,
        maxLabelCount: Number,
        minBarSize: Number,
        opacity: Number,
        point: Object,
        scatter: Object,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        showInLegend: Boolean,
        stack: String,
        stackedbar: Object,
        tagField: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "area",
                "bar",
                "line",
                "scatter",
                "stackedbar"
            ].indexOf(v) !== -1; }
        },
        valueErrorBar: Object,
        valueField: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxCommonSeriesSettings = DxCommonSeriesSettings;
DxCommonSeriesSettings.$_optionName = "commonSeriesSettings";
DxCommonSeriesSettings.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    commonSeriesSettingsHoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    commonSeriesSettingsLabel: { isCollectionItem: false, optionName: "label" },
    commonSeriesSettingsSelectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    hoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    label: { isCollectionItem: false, optionName: "label" },
    point: { isCollectionItem: false, optionName: "point" },
    selectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    seriesBorder: { isCollectionItem: false, optionName: "border" },
    valueErrorBar: { isCollectionItem: false, optionName: "valueErrorBar" }
};
var DxCommonSeriesSettingsHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        width: Number
    }
});
exports.DxCommonSeriesSettingsHoverStyle = DxCommonSeriesSettingsHoverStyle;
DxCommonSeriesSettingsHoverStyle.$_optionName = "hoverStyle";
DxCommonSeriesSettingsHoverStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hatching: { isCollectionItem: false, optionName: "hatching" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxCommonSeriesSettingsLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        argumentFormat: [Object, Function, String],
        backgroundColor: String,
        border: Object,
        connector: Object,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        rotationAngle: Number,
        showForZeroValues: Boolean,
        visible: Boolean
    }
});
exports.DxCommonSeriesSettingsLabel = DxCommonSeriesSettingsLabel;
DxCommonSeriesSettingsLabel.$_optionName = "label";
DxCommonSeriesSettingsLabel.$_expectedChildren = {
    argumentFormat: { isCollectionItem: false, optionName: "argumentFormat" },
    border: { isCollectionItem: false, optionName: "border" },
    connector: { isCollectionItem: false, optionName: "connector" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxCommonSeriesSettingsSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        width: Number
    }
});
exports.DxCommonSeriesSettingsSelectionStyle = DxCommonSeriesSettingsSelectionStyle;
DxCommonSeriesSettingsSelectionStyle.$_optionName = "selectionStyle";
DxCommonSeriesSettingsSelectionStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hatching: { isCollectionItem: false, optionName: "hatching" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxConnector = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxConnector = DxConnector;
DxConnector.$_optionName = "connector";
var DxConstantLine = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        extendAxis: Boolean,
        label: Object,
        value: {},
        width: Number
    }
});
exports.DxConstantLine = DxConstantLine;
DxConstantLine.$_optionName = "constantLines";
DxConstantLine.$_isCollectionItem = true;
var DxConstantLineLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String,
        visible: Boolean
    }
});
exports.DxConstantLineLabel = DxConstantLineLabel;
DxConstantLineLabel.$_optionName = "label";
var DxConstantLineStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        width: Number
    }
});
exports.DxConstantLineStyle = DxConstantLineStyle;
DxConstantLineStyle.$_optionName = "constantLineStyle";
DxConstantLineStyle.$_expectedChildren = {
    constantLineStyleLabel: { isCollectionItem: false, optionName: "label" },
    label: { isCollectionItem: false, optionName: "label" }
};
var DxConstantLineStyleLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        visible: Boolean
    }
});
exports.DxConstantLineStyleLabel = DxConstantLineStyleLabel;
DxConstantLineStyleLabel.$_optionName = "label";
DxConstantLineStyleLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxDataPrepareSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        checkTypeForAllData: Boolean,
        convertToAxisDataType: Boolean,
        sortingMethod: [Boolean, Function]
    }
});
exports.DxDataPrepareSettings = DxDataPrepareSettings;
DxDataPrepareSettings.$_optionName = "dataPrepareSettings";
var DxExport = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        enabled: Boolean,
        fileName: String,
        formats: Array,
        margin: Number,
        printingEnabled: Boolean,
        proxyUrl: String
    }
});
exports.DxExport = DxExport;
DxExport.$_optionName = "export";
var DxFont = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        family: String,
        opacity: Number,
        size: [Number, String],
        weight: Number
    }
});
exports.DxFont = DxFont;
DxFont.$_optionName = "font";
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxGrid = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxGrid = DxGrid;
DxGrid.$_optionName = "grid";
var DxHatching = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "none",
                "right"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        step: Number,
        width: Number
    }
});
exports.DxHatching = DxHatching;
DxHatching.$_optionName = "hatching";
var DxHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        size: Number,
        width: Number
    }
});
exports.DxHoverStyle = DxHoverStyle;
DxHoverStyle.$_optionName = "hoverStyle";
var DxImage = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        url: String,
        width: Number
    }
});
exports.DxImage = DxImage;
DxImage.$_optionName = "image";
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        argumentFormat: [Object, Function, String],
        backgroundColor: String,
        border: Object,
        connector: Object,
        customizeHint: Function,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        indentFromAxis: Number,
        overlappingBehavior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "hide"
            ].indexOf(v) !== -1; }
        },
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        rotationAngle: Number,
        showForZeroValues: Boolean,
        text: String,
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
var DxLegend = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        border: Object,
        columnCount: Number,
        columnItemSpacing: Number,
        customizeHint: Function,
        customizeText: Function,
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "excludePoints",
                "includePoints",
                "none"
            ].indexOf(v) !== -1; }
        },
        itemsAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        itemTextPosition: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        markerSize: Number,
        orientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "horizontal",
                "vertical"
            ].indexOf(v) !== -1; }
        },
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        rowCount: Number,
        rowItemSpacing: Number,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxLegend = DxLegend;
DxLegend.$_optionName = "legend";
DxLegend.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    legendBorder: { isCollectionItem: false, optionName: "border" },
    margin: { isCollectionItem: false, optionName: "margin" }
};
var DxLegendBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxLegendBorder = DxLegendBorder;
DxLegendBorder.$_optionName = "border";
var DxLoadingIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        font: Object,
        show: Boolean,
        text: String
    }
});
exports.DxLoadingIndicator = DxLoadingIndicator;
DxLoadingIndicator.$_optionName = "loadingIndicator";
DxLoadingIndicator.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxMargin = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Number,
        left: Number,
        right: Number,
        top: Number
    }
});
exports.DxMargin = DxMargin;
DxMargin.$_optionName = "margin";
var DxMinorGrid = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxMinorGrid = DxMinorGrid;
DxMinorGrid.$_optionName = "minorGrid";
var DxMinorTick = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        length: Number,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxMinorTick = DxMinorTick;
DxMinorTick.$_optionName = "minorTick";
var DxMinorTickInterval = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxMinorTickInterval = DxMinorTickInterval;
DxMinorTickInterval.$_optionName = "minorTickInterval";
var DxPoint = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        image: [Object, String],
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        size: Number,
        symbol: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "circle",
                "cross",
                "polygon",
                "square",
                "triangle"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxPoint = DxPoint;
DxPoint.$_optionName = "point";
DxPoint.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    image: { isCollectionItem: false, optionName: "image" },
    pointBorder: { isCollectionItem: false, optionName: "border" },
    pointHoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    pointSelectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    selectionStyle: { isCollectionItem: false, optionName: "selectionStyle" }
};
var DxPointBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxPointBorder = DxPointBorder;
DxPointBorder.$_optionName = "border";
var DxPointHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        size: Number
    }
});
exports.DxPointHoverStyle = DxPointHoverStyle;
DxPointHoverStyle.$_optionName = "hoverStyle";
DxPointHoverStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    pointBorder: { isCollectionItem: false, optionName: "border" }
};
var DxPointSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        size: Number
    }
});
exports.DxPointSelectionStyle = DxPointSelectionStyle;
DxPointSelectionStyle.$_optionName = "selectionStyle";
DxPointSelectionStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    pointBorder: { isCollectionItem: false, optionName: "border" }
};
var DxSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        size: Number,
        width: Number
    }
});
exports.DxSelectionStyle = DxSelectionStyle;
DxSelectionStyle.$_optionName = "selectionStyle";
var DxSeries = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        argumentField: String,
        axis: String,
        barPadding: Number,
        barWidth: Number,
        border: Object,
        closed: Boolean,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "nearestPoint",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        ignoreEmptyPoints: Boolean,
        label: Object,
        maxLabelCount: Number,
        minBarSize: Number,
        name: String,
        opacity: Number,
        point: Object,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        showInLegend: Boolean,
        stack: String,
        tag: {},
        tagField: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "area",
                "bar",
                "line",
                "scatter",
                "stackedbar"
            ].indexOf(v) !== -1; }
        },
        valueErrorBar: Object,
        valueField: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxSeries = DxSeries;
DxSeries.$_optionName = "series";
DxSeries.$_isCollectionItem = true;
var DxSeriesBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        width: Number
    }
});
exports.DxSeriesBorder = DxSeriesBorder;
DxSeriesBorder.$_optionName = "border";
var DxSeriesTemplate = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeSeries: Function,
        nameField: String
    }
});
exports.DxSeriesTemplate = DxSeriesTemplate;
DxSeriesTemplate.$_optionName = "seriesTemplate";
var DxShadow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        blur: Number,
        color: String,
        offsetX: Number,
        offsetY: Number,
        opacity: Number
    }
});
exports.DxShadow = DxShadow;
DxShadow.$_optionName = "shadow";
var DxSize = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        width: Number
    }
});
exports.DxSize = DxSize;
DxSize.$_optionName = "size";
var DxStrip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        endValue: {},
        label: Object,
        startValue: {}
    }
});
exports.DxStrip = DxStrip;
DxStrip.$_optionName = "strips";
DxStrip.$_isCollectionItem = true;
var DxStripLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String
    }
});
exports.DxStripLabel = DxStripLabel;
DxStripLabel.$_optionName = "label";
var DxStripStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        label: Object
    }
});
exports.DxStripStyle = DxStripStyle;
DxStripStyle.$_optionName = "stripStyle";
DxStripStyle.$_expectedChildren = {
    label: { isCollectionItem: false, optionName: "label" },
    stripStyleLabel: { isCollectionItem: false, optionName: "label" }
};
var DxStripStyleLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object
    }
});
exports.DxStripStyleLabel = DxStripStyleLabel;
DxStripStyleLabel.$_optionName = "label";
DxStripStyleLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxSubtitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String
    }
});
exports.DxSubtitle = DxSubtitle;
DxSubtitle.$_optionName = "subtitle";
DxSubtitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxTick = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        length: Number,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxTick = DxTick;
DxTick.$_optionName = "tick";
var DxTickInterval = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxTickInterval = DxTickInterval;
DxTickInterval.$_optionName = "tickInterval";
var DxTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        placeholderSize: Number,
        subtitle: [Object, String],
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxTitle = DxTitle;
DxTitle.$_optionName = "title";
DxTitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    margin: { isCollectionItem: false, optionName: "margin" },
    subtitle: { isCollectionItem: false, optionName: "subtitle" }
};
var DxTooltip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        argumentFormat: [Object, Function, String],
        arrowLength: Number,
        border: Object,
        color: String,
        container: {},
        customizeTooltip: Function,
        enabled: Boolean,
        font: Object,
        format: [Object, Function, String],
        opacity: Number,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        shadow: Object,
        shared: Boolean,
        zIndex: Number
    }
});
exports.DxTooltip = DxTooltip;
DxTooltip.$_optionName = "tooltip";
DxTooltip.$_expectedChildren = {
    argumentFormat: { isCollectionItem: false, optionName: "argumentFormat" },
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    shadow: { isCollectionItem: false, optionName: "shadow" },
    tooltipBorder: { isCollectionItem: false, optionName: "border" }
};
var DxTooltipBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxTooltipBorder = DxTooltipBorder;
DxTooltipBorder.$_optionName = "border";
var DxValueAxis = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowDecimals: Boolean,
        axisDivisionFactor: Number,
        categories: Array,
        color: String,
        constantLines: Array,
        constantLineStyle: Object,
        discreteAxisDivisionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "betweenLabels",
                "crossLabels"
            ].indexOf(v) !== -1; }
        },
        endOnTick: Boolean,
        grid: Object,
        inverted: Boolean,
        label: Object,
        logarithmBase: Number,
        maxValueMargin: Number,
        minorGrid: Object,
        minorTick: Object,
        minorTickCount: Number,
        minorTickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        minValueMargin: Number,
        opacity: Number,
        showZero: Boolean,
        strips: Array,
        stripStyle: Object,
        tick: Object,
        tickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "continuous",
                "discrete",
                "logarithmic"
            ].indexOf(v) !== -1; }
        },
        valueMarginsEnabled: Boolean,
        valueType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "datetime",
                "numeric",
                "string"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        width: Number
    }
});
exports.DxValueAxis = DxValueAxis;
DxValueAxis.$_optionName = "valueAxis";
DxValueAxis.$_expectedChildren = {
    axisLabel: { isCollectionItem: false, optionName: "label" },
    constantLine: { isCollectionItem: true, optionName: "constantLines" },
    label: { isCollectionItem: false, optionName: "label" },
    minorTickInterval: { isCollectionItem: false, optionName: "minorTickInterval" },
    strip: { isCollectionItem: true, optionName: "strips" },
    tick: { isCollectionItem: false, optionName: "tick" },
    tickInterval: { isCollectionItem: false, optionName: "tickInterval" }
};
var DxValueErrorBar = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        displayMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "high",
                "low",
                "none"
            ].indexOf(v) !== -1; }
        },
        edgeLength: Number,
        highValueField: String,
        lineWidth: Number,
        lowValueField: String,
        opacity: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fixed",
                "percent",
                "stdDeviation",
                "stdError",
                "variance"
            ].indexOf(v) !== -1; }
        },
        value: Number
    }
});
exports.DxValueErrorBar = DxValueErrorBar;
DxValueErrorBar.$_optionName = "valueErrorBar";
exports.default = DxPolarChart;
