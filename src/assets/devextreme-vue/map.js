/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var map_1 = require("devextreme/ui/map");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxMap = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        autoAdjust: Boolean,
        center: [Array, Object, String],
        controls: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        key: [Object, String],
        markerIconSrc: String,
        markers: Array,
        onClick: [Function, String],
        onDisposing: Function,
        onInitialized: Function,
        onMarkerAdded: Function,
        onMarkerRemoved: Function,
        onOptionChanged: Function,
        onReady: Function,
        onRouteAdded: Function,
        onRouteRemoved: Function,
        provider: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bing",
                "google",
                "googleStatic"
            ].indexOf(v) !== -1; }
        },
        routes: Array,
        rtlEnabled: Boolean,
        tabIndex: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "hybrid",
                "roadmap",
                "satellite"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        width: [Function, Number, String],
        zoom: Number
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = map_1.default;
        this.$_expectedChildren = {
            center: { isCollectionItem: false, optionName: "center" },
            key: { isCollectionItem: false, optionName: "key" },
            marker: { isCollectionItem: true, optionName: "markers" },
            route: { isCollectionItem: true, optionName: "routes" }
        };
    }
});
exports.DxMap = DxMap;
var DxCenter = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        lat: Number,
        lng: Number
    }
});
exports.DxCenter = DxCenter;
DxCenter.$_optionName = "center";
var DxKey = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bing: String,
        google: String,
        googleStatic: String
    }
});
exports.DxKey = DxKey;
DxKey.$_optionName = "key";
var DxLocation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        lat: Number,
        lng: Number
    }
});
exports.DxLocation = DxLocation;
DxLocation.$_optionName = "location";
var DxMarker = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        iconSrc: String,
        location: [Array, Object, String],
        onClick: Function,
        tooltip: [Object, String]
    }
});
exports.DxMarker = DxMarker;
DxMarker.$_optionName = "markers";
DxMarker.$_isCollectionItem = true;
DxMarker.$_expectedChildren = {
    location: { isCollectionItem: false, optionName: "location" },
    tooltip: { isCollectionItem: false, optionName: "tooltip" }
};
var DxRoute = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        locations: Array,
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "driving",
                "walking"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        weight: Number
    }
});
exports.DxRoute = DxRoute;
DxRoute.$_optionName = "routes";
DxRoute.$_isCollectionItem = true;
DxRoute.$_expectedChildren = {
    location: { isCollectionItem: true, optionName: "locations" }
};
var DxTooltip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        isShown: Boolean,
        text: String
    }
});
exports.DxTooltip = DxTooltip;
DxTooltip.$_optionName = "tooltip";
exports.default = DxMap;
