/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var range_selector_1 = require("devextreme/viz/range_selector");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxRangeSelector = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        background: Object,
        behavior: Object,
        chart: Object,
        containerBackgroundColor: String,
        dataSource: [Array, Object, String],
        dataSourceField: String,
        disabled: Boolean,
        elementAttr: Object,
        export: Object,
        indent: Object,
        loadingIndicator: Object,
        margin: Object,
        onDisposing: Function,
        onDrawn: Function,
        onExported: Function,
        onExporting: Function,
        onFileSaving: Function,
        onIncidentOccurred: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onValueChanged: Function,
        pathModified: Boolean,
        redrawOnResize: Boolean,
        rtlEnabled: Boolean,
        scale: Object,
        selectedRangeColor: String,
        selectedRangeUpdateMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "keep",
                "reset",
                "shift"
            ].indexOf(v) !== -1; }
        },
        shutter: Object,
        size: Object,
        sliderHandle: Object,
        sliderMarker: Object,
        theme: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "android5.light",
                "generic.dark",
                "generic.light",
                "generic.contrast",
                "ios7.default",
                "win10.black",
                "win10.white",
                "win8.black",
                "win8.white",
                "generic.carmine",
                "generic.darkmoon",
                "generic.darkviolet",
                "generic.greenmist",
                "generic.softblue",
                "material.blue.light",
                "material.lime.light",
                "material.orange.light",
                "material.purple.light",
                "material.teal.light"
            ].indexOf(v) !== -1; }
        },
        title: [Object, String],
        value: [Array, Object]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = range_selector_1.default;
        this.$_expectedChildren = {
            background: { isCollectionItem: false, optionName: "background" },
            behavior: { isCollectionItem: false, optionName: "behavior" },
            chart: { isCollectionItem: false, optionName: "chart" },
            export: { isCollectionItem: false, optionName: "export" },
            indent: { isCollectionItem: false, optionName: "indent" },
            loadingIndicator: { isCollectionItem: false, optionName: "loadingIndicator" },
            margin: { isCollectionItem: false, optionName: "margin" },
            scale: { isCollectionItem: false, optionName: "scale" },
            shutter: { isCollectionItem: false, optionName: "shutter" },
            size: { isCollectionItem: false, optionName: "size" },
            sliderHandle: { isCollectionItem: false, optionName: "sliderHandle" },
            sliderMarker: { isCollectionItem: false, optionName: "sliderMarker" },
            title: { isCollectionItem: false, optionName: "title" },
            value: { isCollectionItem: false, optionName: "value" }
        };
    }
});
exports.DxRangeSelector = DxRangeSelector;
var DxAggregation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        calculate: Function,
        enabled: Boolean,
        method: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "avg",
                "count",
                "max",
                "min",
                "ohlc",
                "range",
                "sum",
                "custom"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxAggregation = DxAggregation;
DxAggregation.$_optionName = "aggregation";
var DxAggregationInterval = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxAggregationInterval = DxAggregationInterval;
DxAggregationInterval.$_optionName = "aggregationInterval";
var DxArgumentFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxArgumentFormat = DxArgumentFormat;
DxArgumentFormat.$_optionName = "argumentFormat";
var DxBackground = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        image: Object,
        visible: Boolean
    }
});
exports.DxBackground = DxBackground;
DxBackground.$_optionName = "background";
DxBackground.$_expectedChildren = {
    backgroundImage: { isCollectionItem: false, optionName: "image" },
    image: { isCollectionItem: false, optionName: "image" }
};
var DxBackgroundImage = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        location: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "centerBottom",
                "centerTop",
                "full",
                "leftBottom",
                "leftCenter",
                "leftTop",
                "rightBottom",
                "rightCenter",
                "rightTop"
            ].indexOf(v) !== -1; }
        },
        url: String
    }
});
exports.DxBackgroundImage = DxBackgroundImage;
DxBackgroundImage.$_optionName = "image";
var DxBehavior = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowSlidersSwap: Boolean,
        animationEnabled: Boolean,
        callValueChanged: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "onMoving",
                "onMovingComplete"
            ].indexOf(v) !== -1; }
        },
        manualRangeSelectionEnabled: Boolean,
        moveSelectedRangeByClick: Boolean,
        snapToTicks: Boolean
    }
});
exports.DxBehavior = DxBehavior;
DxBehavior.$_optionName = "behavior";
var DxBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        width: Number
    }
});
exports.DxBorder = DxBorder;
DxBorder.$_optionName = "border";
var DxBreak = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        endValue: {},
        startValue: {}
    }
});
exports.DxBreak = DxBreak;
DxBreak.$_optionName = "breaks";
DxBreak.$_isCollectionItem = true;
var DxBreakStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        line: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "straight",
                "waved"
            ].indexOf(v) !== -1; }
        },
        width: Number
    }
});
exports.DxBreakStyle = DxBreakStyle;
DxBreakStyle.$_optionName = "breakStyle";
var DxChart = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        barGroupPadding: Number,
        barGroupWidth: Number,
        barWidth: Number,
        bottomIndent: Number,
        commonSeriesSettings: Object,
        dataPrepareSettings: Object,
        equalBarWidth: Boolean,
        negativesAsZeroes: Boolean,
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        paletteExtensionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "alternate",
                "blend",
                "extrapolate"
            ].indexOf(v) !== -1; }
        },
        series: [Array, Object],
        seriesTemplate: Object,
        topIndent: Number,
        useAggregation: Boolean,
        valueAxis: Object
    }
});
exports.DxChart = DxChart;
DxChart.$_optionName = "chart";
DxChart.$_expectedChildren = {
    commonSeriesSettings: { isCollectionItem: false, optionName: "commonSeriesSettings" },
    dataPrepareSettings: { isCollectionItem: false, optionName: "dataPrepareSettings" },
    series: { isCollectionItem: true, optionName: "series" },
    seriesTemplate: { isCollectionItem: false, optionName: "seriesTemplate" },
    valueAxis: { isCollectionItem: false, optionName: "valueAxis" }
};
var DxCommonSeriesSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        aggregation: Object,
        area: Object,
        argumentField: String,
        axis: String,
        bar: Object,
        barPadding: Number,
        barWidth: Number,
        border: Object,
        bubble: Object,
        candlestick: Object,
        closeValueField: String,
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        fullstackedarea: Object,
        fullstackedbar: Object,
        fullstackedline: Object,
        fullstackedspline: Object,
        fullstackedsplinearea: Object,
        highValueField: String,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "nearestPoint",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        ignoreEmptyPoints: Boolean,
        innerColor: String,
        label: Object,
        line: Object,
        lowValueField: String,
        maxLabelCount: Number,
        minBarSize: Number,
        opacity: Number,
        openValueField: String,
        pane: String,
        point: Object,
        rangearea: Object,
        rangebar: Object,
        rangeValue1Field: String,
        rangeValue2Field: String,
        reduction: Object,
        scatter: Object,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        showInLegend: Boolean,
        sizeField: String,
        spline: Object,
        splinearea: Object,
        stack: String,
        stackedarea: Object,
        stackedbar: Object,
        stackedline: Object,
        stackedspline: Object,
        stackedsplinearea: Object,
        steparea: Object,
        stepline: Object,
        stock: Object,
        tagField: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "area",
                "bar",
                "bubble",
                "candlestick",
                "fullstackedarea",
                "fullstackedbar",
                "fullstackedline",
                "fullstackedspline",
                "fullstackedsplinearea",
                "line",
                "rangearea",
                "rangebar",
                "scatter",
                "spline",
                "splinearea",
                "stackedarea",
                "stackedbar",
                "stackedline",
                "stackedspline",
                "stackedsplinearea",
                "steparea",
                "stepline",
                "stock"
            ].indexOf(v) !== -1; }
        },
        valueErrorBar: Object,
        valueField: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxCommonSeriesSettings = DxCommonSeriesSettings;
DxCommonSeriesSettings.$_optionName = "commonSeriesSettings";
DxCommonSeriesSettings.$_expectedChildren = {
    aggregation: { isCollectionItem: false, optionName: "aggregation" },
    border: { isCollectionItem: false, optionName: "border" },
    commonSeriesSettingsHoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    commonSeriesSettingsLabel: { isCollectionItem: false, optionName: "label" },
    commonSeriesSettingsSelectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    hoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    label: { isCollectionItem: false, optionName: "label" },
    point: { isCollectionItem: false, optionName: "point" },
    reduction: { isCollectionItem: false, optionName: "reduction" },
    selectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    seriesBorder: { isCollectionItem: false, optionName: "border" },
    valueErrorBar: { isCollectionItem: false, optionName: "valueErrorBar" }
};
var DxCommonSeriesSettingsHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        width: Number
    }
});
exports.DxCommonSeriesSettingsHoverStyle = DxCommonSeriesSettingsHoverStyle;
DxCommonSeriesSettingsHoverStyle.$_optionName = "hoverStyle";
DxCommonSeriesSettingsHoverStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hatching: { isCollectionItem: false, optionName: "hatching" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxCommonSeriesSettingsLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        argumentFormat: [Object, Function, String],
        backgroundColor: String,
        border: Object,
        connector: Object,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        horizontalOffset: Number,
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        rotationAngle: Number,
        showForZeroValues: Boolean,
        verticalOffset: Number,
        visible: Boolean
    }
});
exports.DxCommonSeriesSettingsLabel = DxCommonSeriesSettingsLabel;
DxCommonSeriesSettingsLabel.$_optionName = "label";
DxCommonSeriesSettingsLabel.$_expectedChildren = {
    argumentFormat: { isCollectionItem: false, optionName: "argumentFormat" },
    border: { isCollectionItem: false, optionName: "border" },
    connector: { isCollectionItem: false, optionName: "connector" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxCommonSeriesSettingsSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        width: Number
    }
});
exports.DxCommonSeriesSettingsSelectionStyle = DxCommonSeriesSettingsSelectionStyle;
DxCommonSeriesSettingsSelectionStyle.$_optionName = "selectionStyle";
DxCommonSeriesSettingsSelectionStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hatching: { isCollectionItem: false, optionName: "hatching" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxConnector = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxConnector = DxConnector;
DxConnector.$_optionName = "connector";
var DxDataPrepareSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        checkTypeForAllData: Boolean,
        convertToAxisDataType: Boolean,
        sortingMethod: [Boolean, Function]
    }
});
exports.DxDataPrepareSettings = DxDataPrepareSettings;
DxDataPrepareSettings.$_optionName = "dataPrepareSettings";
var DxExport = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        enabled: Boolean,
        fileName: String,
        formats: Array,
        margin: Number,
        printingEnabled: Boolean,
        proxyUrl: String
    }
});
exports.DxExport = DxExport;
DxExport.$_optionName = "export";
var DxFont = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        family: String,
        opacity: Number,
        size: [Number, String],
        weight: Number
    }
});
exports.DxFont = DxFont;
DxFont.$_optionName = "font";
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxHatching = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "none",
                "right"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        step: Number,
        width: Number
    }
});
exports.DxHatching = DxHatching;
DxHatching.$_optionName = "hatching";
var DxHeight = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        rangeMaxPoint: Number,
        rangeMinPoint: Number
    }
});
exports.DxHeight = DxHeight;
DxHeight.$_optionName = "height";
var DxHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        size: Number,
        width: Number
    }
});
exports.DxHoverStyle = DxHoverStyle;
DxHoverStyle.$_optionName = "hoverStyle";
var DxImage = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: [Number, Object],
        location: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "centerBottom",
                "centerTop",
                "full",
                "leftBottom",
                "leftCenter",
                "leftTop",
                "rightBottom",
                "rightCenter",
                "rightTop"
            ].indexOf(v) !== -1; }
        },
        url: String,
        width: [Number, Object]
    }
});
exports.DxImage = DxImage;
DxImage.$_optionName = "image";
var DxIndent = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        left: Number,
        right: Number
    }
});
exports.DxIndent = DxIndent;
DxIndent.$_optionName = "indent";
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        argumentFormat: [Object, Function, String],
        backgroundColor: String,
        border: Object,
        connector: Object,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        horizontalOffset: Number,
        overlappingBehavior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "hide",
                "none"
            ].indexOf(v) !== -1; }
        },
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        rotationAngle: Number,
        showForZeroValues: Boolean,
        topIndent: Number,
        verticalOffset: Number,
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
var DxLength = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxLength = DxLength;
DxLength.$_optionName = "length";
var DxLoadingIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        font: Object,
        show: Boolean,
        text: String
    }
});
exports.DxLoadingIndicator = DxLoadingIndicator;
DxLoadingIndicator.$_optionName = "loadingIndicator";
DxLoadingIndicator.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxMargin = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Number,
        left: Number,
        right: Number,
        top: Number
    }
});
exports.DxMargin = DxMargin;
DxMargin.$_optionName = "margin";
var DxMarker = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        label: Object,
        separatorHeight: Number,
        textLeftIndent: Number,
        textTopIndent: Number,
        topIndent: Number,
        visible: Boolean
    }
});
exports.DxMarker = DxMarker;
DxMarker.$_optionName = "marker";
DxMarker.$_expectedChildren = {
    label: { isCollectionItem: false, optionName: "label" },
    markerLabel: { isCollectionItem: false, optionName: "label" }
};
var DxMarkerLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeText: Function,
        format: [Object, Function, String]
    }
});
exports.DxMarkerLabel = DxMarkerLabel;
DxMarkerLabel.$_optionName = "label";
DxMarkerLabel.$_expectedChildren = {
    format: { isCollectionItem: false, optionName: "format" }
};
var DxMaxRange = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxMaxRange = DxMaxRange;
DxMaxRange.$_optionName = "maxRange";
var DxMinorTick = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxMinorTick = DxMinorTick;
DxMinorTick.$_optionName = "minorTick";
var DxMinorTickInterval = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxMinorTickInterval = DxMinorTickInterval;
DxMinorTickInterval.$_optionName = "minorTickInterval";
var DxMinRange = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxMinRange = DxMinRange;
DxMinRange.$_optionName = "minRange";
var DxPoint = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        image: [Object, String],
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        size: Number,
        symbol: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "circle",
                "cross",
                "polygon",
                "square",
                "triangleDown",
                "triangleUp"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxPoint = DxPoint;
DxPoint.$_optionName = "point";
DxPoint.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    image: { isCollectionItem: false, optionName: "image" },
    pointBorder: { isCollectionItem: false, optionName: "border" },
    pointHoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    pointImage: { isCollectionItem: false, optionName: "image" },
    pointSelectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    selectionStyle: { isCollectionItem: false, optionName: "selectionStyle" }
};
var DxPointBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxPointBorder = DxPointBorder;
DxPointBorder.$_optionName = "border";
var DxPointHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        size: Number
    }
});
exports.DxPointHoverStyle = DxPointHoverStyle;
DxPointHoverStyle.$_optionName = "hoverStyle";
DxPointHoverStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    pointBorder: { isCollectionItem: false, optionName: "border" }
};
var DxPointImage = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: [Number, Object],
        url: [Object, String],
        width: [Number, Object]
    }
});
exports.DxPointImage = DxPointImage;
DxPointImage.$_optionName = "image";
DxPointImage.$_expectedChildren = {
    height: { isCollectionItem: false, optionName: "height" },
    url: { isCollectionItem: false, optionName: "url" },
    width: { isCollectionItem: false, optionName: "width" }
};
var DxPointSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        size: Number
    }
});
exports.DxPointSelectionStyle = DxPointSelectionStyle;
DxPointSelectionStyle.$_optionName = "selectionStyle";
DxPointSelectionStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    pointBorder: { isCollectionItem: false, optionName: "border" }
};
var DxReduction = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        level: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "close",
                "high",
                "low",
                "open"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxReduction = DxReduction;
DxReduction.$_optionName = "reduction";
var DxScale = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        aggregationGroupWidth: Number,
        aggregationInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        allowDecimals: Boolean,
        breaks: Array,
        breakStyle: Object,
        categories: Array,
        endOnTick: Boolean,
        endValue: {},
        holidays: Array,
        label: Object,
        logarithmBase: Number,
        marker: Object,
        maxRange: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        minorTick: Object,
        minorTickCount: Number,
        minorTickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        minRange: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        placeholderHeight: Number,
        showCustomBoundaryTicks: Boolean,
        singleWorkdays: Array,
        startValue: {},
        tick: Object,
        tickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "continuous",
                "discrete",
                "logarithmic",
                "semidiscrete"
            ].indexOf(v) !== -1; }
        },
        valueType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "datetime",
                "numeric",
                "string"
            ].indexOf(v) !== -1; }
        },
        workdaysOnly: Boolean,
        workWeek: Array
    }
});
exports.DxScale = DxScale;
DxScale.$_optionName = "scale";
DxScale.$_expectedChildren = {
    aggregationInterval: { isCollectionItem: false, optionName: "aggregationInterval" },
    break: { isCollectionItem: true, optionName: "breaks" },
    breakStyle: { isCollectionItem: false, optionName: "breakStyle" },
    label: { isCollectionItem: false, optionName: "label" },
    marker: { isCollectionItem: false, optionName: "marker" },
    maxRange: { isCollectionItem: false, optionName: "maxRange" },
    minorTick: { isCollectionItem: false, optionName: "minorTick" },
    minorTickInterval: { isCollectionItem: false, optionName: "minorTickInterval" },
    minRange: { isCollectionItem: false, optionName: "minRange" },
    scaleLabel: { isCollectionItem: false, optionName: "label" },
    tick: { isCollectionItem: false, optionName: "tick" },
    tickInterval: { isCollectionItem: false, optionName: "tickInterval" }
};
var DxScaleLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        overlappingBehavior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "hide",
                "none"
            ].indexOf(v) !== -1; }
        },
        topIndent: Number,
        visible: Boolean
    }
});
exports.DxScaleLabel = DxScaleLabel;
DxScaleLabel.$_optionName = "label";
DxScaleLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" }
};
var DxSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        size: Number,
        width: Number
    }
});
exports.DxSelectionStyle = DxSelectionStyle;
DxSelectionStyle.$_optionName = "selectionStyle";
var DxSeries = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        aggregation: Object,
        argumentField: String,
        axis: String,
        barPadding: Number,
        barWidth: Number,
        border: Object,
        closeValueField: String,
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        highValueField: String,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "nearestPoint",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        ignoreEmptyPoints: Boolean,
        innerColor: String,
        label: Object,
        lowValueField: String,
        maxLabelCount: Number,
        minBarSize: Number,
        name: String,
        opacity: Number,
        openValueField: String,
        pane: String,
        point: Object,
        rangeValue1Field: String,
        rangeValue2Field: String,
        reduction: Object,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        showInLegend: Boolean,
        sizeField: String,
        stack: String,
        tag: {},
        tagField: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "area",
                "bar",
                "bubble",
                "candlestick",
                "fullstackedarea",
                "fullstackedbar",
                "fullstackedline",
                "fullstackedspline",
                "fullstackedsplinearea",
                "line",
                "rangearea",
                "rangebar",
                "scatter",
                "spline",
                "splinearea",
                "stackedarea",
                "stackedbar",
                "stackedline",
                "stackedspline",
                "stackedsplinearea",
                "steparea",
                "stepline",
                "stock"
            ].indexOf(v) !== -1; }
        },
        valueErrorBar: Object,
        valueField: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxSeries = DxSeries;
DxSeries.$_optionName = "series";
DxSeries.$_isCollectionItem = true;
var DxSeriesBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        width: Number
    }
});
exports.DxSeriesBorder = DxSeriesBorder;
DxSeriesBorder.$_optionName = "border";
var DxSeriesTemplate = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeSeries: Function,
        nameField: String
    }
});
exports.DxSeriesTemplate = DxSeriesTemplate;
DxSeriesTemplate.$_optionName = "seriesTemplate";
var DxShutter = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        opacity: Number
    }
});
exports.DxShutter = DxShutter;
DxShutter.$_optionName = "shutter";
var DxSize = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        width: Number
    }
});
exports.DxSize = DxSize;
DxSize.$_optionName = "size";
var DxSliderHandle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        opacity: Number,
        width: Number
    }
});
exports.DxSliderHandle = DxSliderHandle;
DxSliderHandle.$_optionName = "sliderHandle";
var DxSliderMarker = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        invalidRangeColor: String,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        placeholderHeight: Number,
        visible: Boolean
    }
});
exports.DxSliderMarker = DxSliderMarker;
DxSliderMarker.$_optionName = "sliderMarker";
DxSliderMarker.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" }
};
var DxSubtitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String
    }
});
exports.DxSubtitle = DxSubtitle;
DxSubtitle.$_optionName = "subtitle";
DxSubtitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxTick = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        opacity: Number,
        width: Number
    }
});
exports.DxTick = DxTick;
DxTick.$_optionName = "tick";
var DxTickInterval = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxTickInterval = DxTickInterval;
DxTickInterval.$_optionName = "tickInterval";
var DxTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        placeholderSize: Number,
        subtitle: [Object, String],
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxTitle = DxTitle;
DxTitle.$_optionName = "title";
DxTitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    margin: { isCollectionItem: false, optionName: "margin" },
    subtitle: { isCollectionItem: false, optionName: "subtitle" }
};
var DxUrl = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        rangeMaxPoint: String,
        rangeMinPoint: String
    }
});
exports.DxUrl = DxUrl;
DxUrl.$_optionName = "url";
var DxValue = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        endValue: {},
        length: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        startValue: {}
    }
});
exports.DxValue = DxValue;
DxValue.$_optionName = "value";
DxValue.$_expectedChildren = {
    length: { isCollectionItem: false, optionName: "length" }
};
var DxValueAxis = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        inverted: Boolean,
        logarithmBase: Number,
        max: Number,
        min: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "continuous",
                "logarithmic"
            ].indexOf(v) !== -1; }
        },
        valueType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "datetime",
                "numeric",
                "string"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxValueAxis = DxValueAxis;
DxValueAxis.$_optionName = "valueAxis";
var DxValueErrorBar = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        displayMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "high",
                "low",
                "none"
            ].indexOf(v) !== -1; }
        },
        edgeLength: Number,
        highValueField: String,
        lineWidth: Number,
        lowValueField: String,
        opacity: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fixed",
                "percent",
                "stdDeviation",
                "stdError",
                "variance"
            ].indexOf(v) !== -1; }
        },
        value: Number
    }
});
exports.DxValueErrorBar = DxValueErrorBar;
DxValueErrorBar.$_optionName = "valueErrorBar";
var DxWidth = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        rangeMaxPoint: Number,
        rangeMinPoint: Number
    }
});
exports.DxWidth = DxWidth;
DxWidth.$_optionName = "width";
exports.default = DxRangeSelector;
