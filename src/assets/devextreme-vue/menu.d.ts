/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Menu, { IOptions } from "devextreme/ui/menu";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "adaptivityEnabled" | "animation" | "cssClass" | "dataSource" | "disabled" | "disabledExpr" | "displayExpr" | "elementAttr" | "focusStateEnabled" | "height" | "hideSubmenuOnMouseLeave" | "hint" | "hoverStateEnabled" | "items" | "itemsExpr" | "itemTemplate" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemRendered" | "onOptionChanged" | "onSelectionChanged" | "onSubmenuHidden" | "onSubmenuHiding" | "onSubmenuShowing" | "onSubmenuShown" | "orientation" | "rtlEnabled" | "selectByClick" | "selectedExpr" | "selectedItem" | "selectionMode" | "showFirstSubmenuMode" | "showSubmenuMode" | "submenuDirection" | "tabIndex" | "visible" | "width">;
interface DxMenu extends VueConstructor, AccessibleOptions {
    readonly instance?: Menu;
}
declare const DxMenu: DxMenu;
declare const DxAnimation: any;
declare const DxDelay: any;
declare const DxHide: any;
declare const DxItem: any;
declare const DxShow: any;
declare const DxShowFirstSubmenuMode: any;
declare const DxShowSubmenuMode: any;
export default DxMenu;
export { DxMenu, DxAnimation, DxDelay, DxHide, DxItem, DxShow, DxShowFirstSubmenuMode, DxShowSubmenuMode };
