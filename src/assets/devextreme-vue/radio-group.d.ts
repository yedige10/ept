/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import RadioGroup, { IOptions } from "devextreme/ui/radio_group";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "dataSource" | "disabled" | "displayExpr" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "isValid" | "items" | "itemTemplate" | "layout" | "name" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onValueChanged" | "readOnly" | "rtlEnabled" | "tabIndex" | "validationError" | "validationMessageMode" | "value" | "valueExpr" | "visible" | "width">;
interface DxRadioGroup extends VueConstructor, AccessibleOptions {
    readonly instance?: RadioGroup;
}
declare const DxRadioGroup: DxRadioGroup;
declare const DxItem: any;
export default DxRadioGroup;
export { DxRadioGroup, DxItem };
