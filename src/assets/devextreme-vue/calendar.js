/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var calendar_1 = require("devextreme/ui/calendar");
var component_1 = require("./core/component");
var DxCalendar = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        cellTemplate: {},
        dateSerializationFormat: String,
        disabled: Boolean,
        disabledDates: [Array, Function],
        elementAttr: Object,
        firstDayOfWeek: {
            type: Number,
            validator: function (v) { return typeof (v) !== "number" || [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ].indexOf(v) !== -1; }
        },
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        isValid: Boolean,
        max: {},
        maxZoomLevel: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "century",
                "decade",
                "month",
                "year"
            ].indexOf(v) !== -1; }
        },
        min: {},
        minZoomLevel: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "century",
                "decade",
                "month",
                "year"
            ].indexOf(v) !== -1; }
        },
        name: String,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onValueChanged: Function,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        showTodayButton: Boolean,
        tabIndex: Number,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: {},
        visible: Boolean,
        width: [Function, Number, String],
        zoomLevel: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "century",
                "decade",
                "month",
                "year"
            ].indexOf(v) !== -1; }
        }
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = calendar_1.default;
    }
});
exports.DxCalendar = DxCalendar;
exports.default = DxCalendar;
