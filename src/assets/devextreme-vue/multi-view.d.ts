/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import MultiView, { IOptions } from "devextreme/ui/multi_view";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "animationEnabled" | "dataSource" | "deferRendering" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemTemplate" | "loop" | "noDataText" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onOptionChanged" | "onSelectionChanged" | "rtlEnabled" | "selectedIndex" | "selectedItem" | "swipeEnabled" | "tabIndex" | "visible" | "width">;
interface DxMultiView extends VueConstructor, AccessibleOptions {
    readonly instance?: MultiView;
}
declare const DxMultiView: DxMultiView;
declare const DxItem: any;
export default DxMultiView;
export { DxMultiView, DxItem };
