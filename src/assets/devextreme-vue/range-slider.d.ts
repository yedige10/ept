/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import RangeSlider, { IOptions } from "devextreme/ui/range_slider";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "disabled" | "elementAttr" | "end" | "endName" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "isValid" | "keyStep" | "label" | "max" | "min" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onValueChanged" | "readOnly" | "rtlEnabled" | "showRange" | "start" | "startName" | "step" | "tabIndex" | "tooltip" | "validationError" | "validationMessageMode" | "value" | "visible" | "width">;
interface DxRangeSlider extends VueConstructor, AccessibleOptions {
    readonly instance?: RangeSlider;
}
declare const DxRangeSlider: DxRangeSlider;
declare const DxFormat: any;
declare const DxLabel: any;
declare const DxTooltip: any;
export default DxRangeSlider;
export { DxRangeSlider, DxFormat, DxLabel, DxTooltip };
