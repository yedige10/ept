/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import ValidationSummary, { IOptions } from "devextreme/ui/validation_summary";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "elementAttr" | "hoverStateEnabled" | "items" | "itemTemplate" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onOptionChanged" | "validationGroup">;
interface DxValidationSummary extends VueConstructor, AccessibleOptions {
    readonly instance?: ValidationSummary;
}
declare const DxValidationSummary: DxValidationSummary;
declare const DxItem: any;
export default DxValidationSummary;
export { DxValidationSummary, DxItem };
