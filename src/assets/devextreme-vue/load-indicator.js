/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var load_indicator_1 = require("devextreme/ui/load_indicator");
var component_1 = require("./core/component");
var DxLoadIndicator = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        elementAttr: Object,
        height: [Function, Number, String],
        hint: String,
        indicatorSrc: String,
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        rtlEnabled: Boolean,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = load_indicator_1.default;
    }
});
exports.DxLoadIndicator = DxLoadIndicator;
exports.default = DxLoadIndicator;
