/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var action_sheet_1 = require("devextreme/ui/action_sheet");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxActionSheet = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        cancelText: String,
        dataSource: [Array, Object, String],
        disabled: Boolean,
        elementAttr: Object,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        itemHoldTimeout: Number,
        items: Array,
        itemTemplate: {},
        onCancelClick: [Function, String],
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onOptionChanged: Function,
        rtlEnabled: Boolean,
        showCancelButton: Boolean,
        showTitle: Boolean,
        target: {},
        title: String,
        usePopover: Boolean,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = action_sheet_1.default;
        this.$_expectedChildren = {
            item: { isCollectionItem: true, optionName: "items" }
        };
    }
});
exports.DxActionSheet = DxActionSheet;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        html: String,
        icon: String,
        onClick: [Function, String],
        template: {},
        text: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "back",
                "danger",
                "default",
                "normal",
                "success"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
exports.default = DxActionSheet;
