/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var chart_1 = require("devextreme/viz/chart");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxChart = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        adaptiveLayout: Object,
        adjustOnZoom: Boolean,
        animation: [Boolean, Object],
        argumentAxis: Object,
        barGroupPadding: Number,
        barGroupWidth: Number,
        commonAxisSettings: Object,
        commonPaneSettings: Object,
        commonSeriesSettings: Object,
        containerBackgroundColor: String,
        crosshair: Object,
        customizeLabel: Function,
        customizePoint: Function,
        dataPrepareSettings: Object,
        dataSource: [Array, Object, String],
        defaultPane: String,
        disabled: Boolean,
        elementAttr: Object,
        export: Object,
        legend: Object,
        loadingIndicator: Object,
        margin: Object,
        maxBubbleSize: Number,
        minBubbleSize: Number,
        negativesAsZeroes: Boolean,
        onArgumentAxisClick: [Function, String],
        onDisposing: Function,
        onDone: Function,
        onDrawn: Function,
        onExported: Function,
        onExporting: Function,
        onFileSaving: Function,
        onIncidentOccurred: Function,
        onInitialized: Function,
        onLegendClick: [Function, String],
        onOptionChanged: Function,
        onPointClick: [Function, String],
        onPointHoverChanged: Function,
        onPointSelectionChanged: Function,
        onSeriesClick: [Function, String],
        onSeriesHoverChanged: Function,
        onSeriesSelectionChanged: Function,
        onTooltipHidden: Function,
        onTooltipShown: Function,
        onZoomEnd: Function,
        onZoomStart: Function,
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        paletteExtensionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "alternate",
                "blend",
                "extrapolate"
            ].indexOf(v) !== -1; }
        },
        panes: [Array, Object],
        pathModified: Boolean,
        pointSelectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "single"
            ].indexOf(v) !== -1; }
        },
        redrawOnResize: Boolean,
        resolveLabelOverlapping: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "hide",
                "none",
                "stack"
            ].indexOf(v) !== -1; }
        },
        rotated: Boolean,
        rtlEnabled: Boolean,
        scrollBar: Object,
        series: [Array, Object],
        seriesSelectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "single"
            ].indexOf(v) !== -1; }
        },
        seriesTemplate: Object,
        size: Object,
        synchronizeMultiAxes: Boolean,
        theme: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "android5.light",
                "generic.dark",
                "generic.light",
                "generic.contrast",
                "ios7.default",
                "win10.black",
                "win10.white",
                "win8.black",
                "win8.white",
                "generic.carmine",
                "generic.darkmoon",
                "generic.darkviolet",
                "generic.greenmist",
                "generic.softblue",
                "material.blue.light",
                "material.lime.light",
                "material.orange.light",
                "material.purple.light",
                "material.teal.light"
            ].indexOf(v) !== -1; }
        },
        title: [Object, String],
        tooltip: Object,
        valueAxis: [Array, Object],
        zoomAndPan: Object
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = chart_1.default;
        this.$_expectedChildren = {
            adaptiveLayout: { isCollectionItem: false, optionName: "adaptiveLayout" },
            animation: { isCollectionItem: false, optionName: "animation" },
            argumentAxis: { isCollectionItem: false, optionName: "argumentAxis" },
            chartTitle: { isCollectionItem: false, optionName: "title" },
            commonAxisSettings: { isCollectionItem: false, optionName: "commonAxisSettings" },
            commonPaneSettings: { isCollectionItem: false, optionName: "commonPaneSettings" },
            commonSeriesSettings: { isCollectionItem: false, optionName: "commonSeriesSettings" },
            crosshair: { isCollectionItem: false, optionName: "crosshair" },
            dataPrepareSettings: { isCollectionItem: false, optionName: "dataPrepareSettings" },
            export: { isCollectionItem: false, optionName: "export" },
            legend: { isCollectionItem: false, optionName: "legend" },
            loadingIndicator: { isCollectionItem: false, optionName: "loadingIndicator" },
            margin: { isCollectionItem: false, optionName: "margin" },
            pane: { isCollectionItem: true, optionName: "panes" },
            scrollBar: { isCollectionItem: false, optionName: "scrollBar" },
            series: { isCollectionItem: true, optionName: "series" },
            seriesTemplate: { isCollectionItem: false, optionName: "seriesTemplate" },
            size: { isCollectionItem: false, optionName: "size" },
            title: { isCollectionItem: false, optionName: "title" },
            tooltip: { isCollectionItem: false, optionName: "tooltip" },
            valueAxis: { isCollectionItem: true, optionName: "valueAxis" },
            zoomAndPan: { isCollectionItem: false, optionName: "zoomAndPan" }
        };
    }
});
exports.DxChart = DxChart;
var DxAdaptiveLayout = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        keepLabels: Boolean,
        width: Number
    }
});
exports.DxAdaptiveLayout = DxAdaptiveLayout;
DxAdaptiveLayout.$_optionName = "adaptiveLayout";
var DxAggregation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        calculate: Function,
        enabled: Boolean,
        method: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "avg",
                "count",
                "max",
                "min",
                "ohlc",
                "range",
                "sum",
                "custom"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxAggregation = DxAggregation;
DxAggregation.$_optionName = "aggregation";
var DxAggregationInterval = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxAggregationInterval = DxAggregationInterval;
DxAggregationInterval.$_optionName = "aggregationInterval";
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        duration: Number,
        easing: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "easeOutCubic",
                "linear"
            ].indexOf(v) !== -1; }
        },
        enabled: Boolean,
        maxPointCountSupported: Number
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
var DxArgumentAxis = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        aggregationGroupWidth: Number,
        aggregationInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        allowDecimals: Boolean,
        argumentType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "datetime",
                "numeric",
                "string"
            ].indexOf(v) !== -1; }
        },
        axisDivisionFactor: Number,
        breaks: Array,
        breakStyle: Object,
        categories: Array,
        color: String,
        constantLines: Array,
        constantLineStyle: Object,
        discreteAxisDivisionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "betweenLabels",
                "crossLabels"
            ].indexOf(v) !== -1; }
        },
        endOnTick: Boolean,
        grid: Object,
        holidays: Array,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "none"
            ].indexOf(v) !== -1; }
        },
        inverted: Boolean,
        label: Object,
        logarithmBase: Number,
        max: {},
        maxValueMargin: Number,
        min: {},
        minorGrid: Object,
        minorTick: Object,
        minorTickCount: Number,
        minorTickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        minValueMargin: Number,
        minVisualRangeLength: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        placeholderSize: Number,
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        singleWorkdays: Array,
        strips: Array,
        stripStyle: Object,
        tick: Object,
        tickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        title: [Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "continuous",
                "discrete",
                "logarithmic"
            ].indexOf(v) !== -1; }
        },
        valueMarginsEnabled: Boolean,
        visible: Boolean,
        visualRange: [Array, Object],
        visualRangeUpdateMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "keep",
                "reset",
                "shift"
            ].indexOf(v) !== -1; }
        },
        wholeRange: [Array, Object],
        width: Number,
        workdaysOnly: Boolean,
        workWeek: Array
    }
});
exports.DxArgumentAxis = DxArgumentAxis;
DxArgumentAxis.$_optionName = "argumentAxis";
DxArgumentAxis.$_expectedChildren = {
    aggregationInterval: { isCollectionItem: false, optionName: "aggregationInterval" },
    axisConstantLineStyle: { isCollectionItem: false, optionName: "constantLineStyle" },
    axisLabel: { isCollectionItem: false, optionName: "label" },
    axisTitle: { isCollectionItem: false, optionName: "title" },
    break: { isCollectionItem: true, optionName: "breaks" },
    constantLine: { isCollectionItem: true, optionName: "constantLines" },
    constantLineStyle: { isCollectionItem: false, optionName: "constantLineStyle" },
    label: { isCollectionItem: false, optionName: "label" },
    minorTickInterval: { isCollectionItem: false, optionName: "minorTickInterval" },
    minVisualRangeLength: { isCollectionItem: false, optionName: "minVisualRangeLength" },
    strip: { isCollectionItem: true, optionName: "strips" },
    tickInterval: { isCollectionItem: false, optionName: "tickInterval" },
    title: { isCollectionItem: false, optionName: "title" },
    visualRange: { isCollectionItem: false, optionName: "visualRange" },
    wholeRange: { isCollectionItem: false, optionName: "wholeRange" }
};
var DxArgumentFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxArgumentFormat = DxArgumentFormat;
DxArgumentFormat.$_optionName = "argumentFormat";
var DxAxisConstantLineStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        width: Number
    }
});
exports.DxAxisConstantLineStyle = DxAxisConstantLineStyle;
DxAxisConstantLineStyle.$_optionName = "constantLineStyle";
var DxAxisConstantLineStyleLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxAxisConstantLineStyleLabel = DxAxisConstantLineStyleLabel;
DxAxisConstantLineStyleLabel.$_optionName = "label";
var DxAxisLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        customizeHint: Function,
        customizeText: Function,
        displayMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "rotate",
                "stagger",
                "standard"
            ].indexOf(v) !== -1; }
        },
        font: Object,
        format: [Object, Function, String],
        indentFromAxis: Number,
        overlappingBehavior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "rotate",
                "stagger",
                "none",
                "hide"
            ].indexOf(v) !== -1; }
        },
        rotationAngle: Number,
        staggeringSpacing: Number,
        visible: Boolean
    }
});
exports.DxAxisLabel = DxAxisLabel;
DxAxisLabel.$_optionName = "label";
var DxAxisTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        margin: Number,
        text: String
    }
});
exports.DxAxisTitle = DxAxisTitle;
DxAxisTitle.$_optionName = "title";
var DxBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Boolean,
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        left: Boolean,
        opacity: Number,
        right: Boolean,
        top: Boolean,
        visible: Boolean,
        width: Number
    }
});
exports.DxBorder = DxBorder;
DxBorder.$_optionName = "border";
var DxBreak = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        endValue: {},
        startValue: {}
    }
});
exports.DxBreak = DxBreak;
DxBreak.$_optionName = "breaks";
DxBreak.$_isCollectionItem = true;
var DxBreakStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        line: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "straight",
                "waved"
            ].indexOf(v) !== -1; }
        },
        width: Number
    }
});
exports.DxBreakStyle = DxBreakStyle;
DxBreakStyle.$_optionName = "breakStyle";
var DxChartTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        placeholderSize: Number,
        subtitle: [Object, String],
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxChartTitle = DxChartTitle;
DxChartTitle.$_optionName = "title";
DxChartTitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    margin: { isCollectionItem: false, optionName: "margin" },
    subtitle: { isCollectionItem: false, optionName: "subtitle" }
};
var DxCommonAxisSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowDecimals: Boolean,
        breakStyle: Object,
        color: String,
        constantLineStyle: Object,
        discreteAxisDivisionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "betweenLabels",
                "crossLabels"
            ].indexOf(v) !== -1; }
        },
        endOnTick: Boolean,
        grid: Object,
        inverted: Boolean,
        label: Object,
        maxValueMargin: Number,
        minorGrid: Object,
        minorTick: Object,
        minValueMargin: Number,
        opacity: Number,
        placeholderSize: Number,
        stripStyle: Object,
        tick: Object,
        title: Object,
        valueMarginsEnabled: Boolean,
        visible: Boolean,
        width: Number
    }
});
exports.DxCommonAxisSettings = DxCommonAxisSettings;
DxCommonAxisSettings.$_optionName = "commonAxisSettings";
DxCommonAxisSettings.$_expectedChildren = {
    breakStyle: { isCollectionItem: false, optionName: "breakStyle" },
    commonAxisSettingsConstantLineStyle: { isCollectionItem: false, optionName: "constantLineStyle" },
    commonAxisSettingsLabel: { isCollectionItem: false, optionName: "label" },
    commonAxisSettingsTitle: { isCollectionItem: false, optionName: "title" },
    constantLineStyle: { isCollectionItem: false, optionName: "constantLineStyle" },
    grid: { isCollectionItem: false, optionName: "grid" },
    label: { isCollectionItem: false, optionName: "label" },
    minorGrid: { isCollectionItem: false, optionName: "minorGrid" },
    minorTick: { isCollectionItem: false, optionName: "minorTick" },
    stripStyle: { isCollectionItem: false, optionName: "stripStyle" },
    tick: { isCollectionItem: false, optionName: "tick" },
    title: { isCollectionItem: false, optionName: "title" }
};
var DxCommonAxisSettingsConstantLineStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        width: Number
    }
});
exports.DxCommonAxisSettingsConstantLineStyle = DxCommonAxisSettingsConstantLineStyle;
DxCommonAxisSettingsConstantLineStyle.$_optionName = "constantLineStyle";
DxCommonAxisSettingsConstantLineStyle.$_expectedChildren = {
    commonAxisSettingsConstantLineStyleLabel: { isCollectionItem: false, optionName: "label" },
    label: { isCollectionItem: false, optionName: "label" }
};
var DxCommonAxisSettingsConstantLineStyleLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxCommonAxisSettingsConstantLineStyleLabel = DxCommonAxisSettingsConstantLineStyleLabel;
DxCommonAxisSettingsConstantLineStyleLabel.$_optionName = "label";
DxCommonAxisSettingsConstantLineStyleLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxCommonAxisSettingsLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        displayMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "rotate",
                "stagger",
                "standard"
            ].indexOf(v) !== -1; }
        },
        font: Object,
        indentFromAxis: Number,
        overlappingBehavior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "rotate",
                "stagger",
                "none",
                "hide"
            ].indexOf(v) !== -1; }
        },
        rotationAngle: Number,
        staggeringSpacing: Number,
        visible: Boolean
    }
});
exports.DxCommonAxisSettingsLabel = DxCommonAxisSettingsLabel;
DxCommonAxisSettingsLabel.$_optionName = "label";
DxCommonAxisSettingsLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxCommonAxisSettingsTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        margin: Number
    }
});
exports.DxCommonAxisSettingsTitle = DxCommonAxisSettingsTitle;
DxCommonAxisSettingsTitle.$_optionName = "title";
DxCommonAxisSettingsTitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxCommonPaneSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        border: Object
    }
});
exports.DxCommonPaneSettings = DxCommonPaneSettings;
DxCommonPaneSettings.$_optionName = "commonPaneSettings";
DxCommonPaneSettings.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    paneBorder: { isCollectionItem: false, optionName: "border" }
};
var DxCommonSeriesSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        aggregation: Object,
        area: Object,
        argumentField: String,
        axis: String,
        bar: Object,
        barPadding: Number,
        barWidth: Number,
        border: Object,
        bubble: Object,
        candlestick: Object,
        closeValueField: String,
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        fullstackedarea: Object,
        fullstackedbar: Object,
        fullstackedline: Object,
        fullstackedspline: Object,
        fullstackedsplinearea: Object,
        highValueField: String,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "nearestPoint",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        ignoreEmptyPoints: Boolean,
        innerColor: String,
        label: Object,
        line: Object,
        lowValueField: String,
        maxLabelCount: Number,
        minBarSize: Number,
        opacity: Number,
        openValueField: String,
        pane: String,
        point: Object,
        rangearea: Object,
        rangebar: Object,
        rangeValue1Field: String,
        rangeValue2Field: String,
        reduction: Object,
        scatter: Object,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        showInLegend: Boolean,
        sizeField: String,
        spline: Object,
        splinearea: Object,
        stack: String,
        stackedarea: Object,
        stackedbar: Object,
        stackedline: Object,
        stackedspline: Object,
        stackedsplinearea: Object,
        steparea: Object,
        stepline: Object,
        stock: Object,
        tagField: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "area",
                "bar",
                "bubble",
                "candlestick",
                "fullstackedarea",
                "fullstackedbar",
                "fullstackedline",
                "fullstackedspline",
                "fullstackedsplinearea",
                "line",
                "rangearea",
                "rangebar",
                "scatter",
                "spline",
                "splinearea",
                "stackedarea",
                "stackedbar",
                "stackedline",
                "stackedspline",
                "stackedsplinearea",
                "steparea",
                "stepline",
                "stock"
            ].indexOf(v) !== -1; }
        },
        valueErrorBar: Object,
        valueField: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxCommonSeriesSettings = DxCommonSeriesSettings;
DxCommonSeriesSettings.$_optionName = "commonSeriesSettings";
DxCommonSeriesSettings.$_expectedChildren = {
    aggregation: { isCollectionItem: false, optionName: "aggregation" },
    border: { isCollectionItem: false, optionName: "border" },
    commonSeriesSettingsHoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    commonSeriesSettingsLabel: { isCollectionItem: false, optionName: "label" },
    commonSeriesSettingsSelectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    hoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    label: { isCollectionItem: false, optionName: "label" },
    point: { isCollectionItem: false, optionName: "point" },
    reduction: { isCollectionItem: false, optionName: "reduction" },
    selectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    seriesBorder: { isCollectionItem: false, optionName: "border" },
    valueErrorBar: { isCollectionItem: false, optionName: "valueErrorBar" }
};
var DxCommonSeriesSettingsHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        width: Number
    }
});
exports.DxCommonSeriesSettingsHoverStyle = DxCommonSeriesSettingsHoverStyle;
DxCommonSeriesSettingsHoverStyle.$_optionName = "hoverStyle";
DxCommonSeriesSettingsHoverStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hatching: { isCollectionItem: false, optionName: "hatching" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxCommonSeriesSettingsLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        argumentFormat: [Object, Function, String],
        backgroundColor: String,
        border: Object,
        connector: Object,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        horizontalOffset: Number,
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        rotationAngle: Number,
        showForZeroValues: Boolean,
        verticalOffset: Number,
        visible: Boolean
    }
});
exports.DxCommonSeriesSettingsLabel = DxCommonSeriesSettingsLabel;
DxCommonSeriesSettingsLabel.$_optionName = "label";
DxCommonSeriesSettingsLabel.$_expectedChildren = {
    argumentFormat: { isCollectionItem: false, optionName: "argumentFormat" },
    border: { isCollectionItem: false, optionName: "border" },
    connector: { isCollectionItem: false, optionName: "connector" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxCommonSeriesSettingsSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        width: Number
    }
});
exports.DxCommonSeriesSettingsSelectionStyle = DxCommonSeriesSettingsSelectionStyle;
DxCommonSeriesSettingsSelectionStyle.$_optionName = "selectionStyle";
DxCommonSeriesSettingsSelectionStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hatching: { isCollectionItem: false, optionName: "hatching" },
    seriesBorder: { isCollectionItem: false, optionName: "border" }
};
var DxConnector = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxConnector = DxConnector;
DxConnector.$_optionName = "connector";
var DxConstantLine = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        extendAxis: Boolean,
        label: Object,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        value: {},
        width: Number
    }
});
exports.DxConstantLine = DxConstantLine;
DxConstantLine.$_optionName = "constantLines";
DxConstantLine.$_isCollectionItem = true;
var DxConstantLineLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxConstantLineLabel = DxConstantLineLabel;
DxConstantLineLabel.$_optionName = "label";
var DxConstantLineStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        width: Number
    }
});
exports.DxConstantLineStyle = DxConstantLineStyle;
DxConstantLineStyle.$_optionName = "constantLineStyle";
var DxCrosshair = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        enabled: Boolean,
        horizontalLine: [Boolean, Object],
        label: Object,
        opacity: Number,
        verticalLine: [Boolean, Object],
        width: Number
    }
});
exports.DxCrosshair = DxCrosshair;
DxCrosshair.$_optionName = "crosshair";
DxCrosshair.$_expectedChildren = {
    crosshairLabel: { isCollectionItem: false, optionName: "label" },
    horizontalLine: { isCollectionItem: false, optionName: "horizontalLine" },
    label: { isCollectionItem: false, optionName: "label" },
    verticalLine: { isCollectionItem: false, optionName: "verticalLine" }
};
var DxCrosshairLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        visible: Boolean
    }
});
exports.DxCrosshairLabel = DxCrosshairLabel;
DxCrosshairLabel.$_optionName = "label";
var DxDataPrepareSettings = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        checkTypeForAllData: Boolean,
        convertToAxisDataType: Boolean,
        sortingMethod: [Boolean, Function]
    }
});
exports.DxDataPrepareSettings = DxDataPrepareSettings;
DxDataPrepareSettings.$_optionName = "dataPrepareSettings";
var DxDragBoxStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        opacity: Number
    }
});
exports.DxDragBoxStyle = DxDragBoxStyle;
DxDragBoxStyle.$_optionName = "dragBoxStyle";
var DxExport = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        enabled: Boolean,
        fileName: String,
        formats: Array,
        margin: Number,
        printingEnabled: Boolean,
        proxyUrl: String
    }
});
exports.DxExport = DxExport;
DxExport.$_optionName = "export";
var DxFont = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        family: String,
        opacity: Number,
        size: [Number, String],
        weight: Number
    }
});
exports.DxFont = DxFont;
DxFont.$_optionName = "font";
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxGrid = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxGrid = DxGrid;
DxGrid.$_optionName = "grid";
var DxHatching = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "none",
                "right"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        step: Number,
        width: Number
    }
});
exports.DxHatching = DxHatching;
DxHatching.$_optionName = "hatching";
var DxHeight = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        rangeMaxPoint: Number,
        rangeMinPoint: Number
    }
});
exports.DxHeight = DxHeight;
DxHeight.$_optionName = "height";
var DxHorizontalLine = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxHorizontalLine = DxHorizontalLine;
DxHorizontalLine.$_optionName = "horizontalLine";
DxHorizontalLine.$_expectedChildren = {
    crosshairLabel: { isCollectionItem: false, optionName: "label" },
    label: { isCollectionItem: false, optionName: "label" }
};
var DxHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        size: Number,
        width: Number
    }
});
exports.DxHoverStyle = DxHoverStyle;
DxHoverStyle.$_optionName = "hoverStyle";
var DxImage = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: [Number, Object],
        url: [Object, String],
        width: [Number, Object]
    }
});
exports.DxImage = DxImage;
DxImage.$_optionName = "image";
DxImage.$_expectedChildren = {
    height: { isCollectionItem: false, optionName: "height" },
    url: { isCollectionItem: false, optionName: "url" },
    width: { isCollectionItem: false, optionName: "width" }
};
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        argumentFormat: [Object, Function, String],
        backgroundColor: String,
        border: Object,
        connector: Object,
        customizeHint: Function,
        customizeText: Function,
        displayMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "rotate",
                "stagger",
                "standard"
            ].indexOf(v) !== -1; }
        },
        font: Object,
        format: [Object, Function, String],
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        horizontalOffset: Number,
        indentFromAxis: Number,
        overlappingBehavior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "rotate",
                "stagger",
                "none",
                "hide"
            ].indexOf(v) !== -1; }
        },
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        rotationAngle: Number,
        showForZeroValues: Boolean,
        staggeringSpacing: Number,
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        },
        verticalOffset: Number,
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
var DxLegend = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        border: Object,
        columnCount: Number,
        columnItemSpacing: Number,
        customizeHint: Function,
        customizeText: Function,
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "excludePoints",
                "includePoints",
                "none"
            ].indexOf(v) !== -1; }
        },
        itemsAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        itemTextPosition: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        markerSize: Number,
        orientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "horizontal",
                "vertical"
            ].indexOf(v) !== -1; }
        },
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        rowCount: Number,
        rowItemSpacing: Number,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxLegend = DxLegend;
DxLegend.$_optionName = "legend";
DxLegend.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    legendBorder: { isCollectionItem: false, optionName: "border" },
    margin: { isCollectionItem: false, optionName: "margin" }
};
var DxLegendBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxLegendBorder = DxLegendBorder;
DxLegendBorder.$_optionName = "border";
var DxLength = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxLength = DxLength;
DxLength.$_optionName = "length";
var DxLoadingIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        font: Object,
        show: Boolean,
        text: String
    }
});
exports.DxLoadingIndicator = DxLoadingIndicator;
DxLoadingIndicator.$_optionName = "loadingIndicator";
DxLoadingIndicator.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxMargin = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Number,
        left: Number,
        right: Number,
        top: Number
    }
});
exports.DxMargin = DxMargin;
DxMargin.$_optionName = "margin";
var DxMinorGrid = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxMinorGrid = DxMinorGrid;
DxMinorGrid.$_optionName = "minorGrid";
var DxMinorTick = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        length: Number,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxMinorTick = DxMinorTick;
DxMinorTick.$_optionName = "minorTick";
var DxMinorTickInterval = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxMinorTickInterval = DxMinorTickInterval;
DxMinorTickInterval.$_optionName = "minorTickInterval";
var DxMinVisualRangeLength = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxMinVisualRangeLength = DxMinVisualRangeLength;
DxMinVisualRangeLength.$_optionName = "minVisualRangeLength";
var DxPane = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        border: Object,
        name: String
    }
});
exports.DxPane = DxPane;
DxPane.$_optionName = "panes";
DxPane.$_isCollectionItem = true;
var DxPaneBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Boolean,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        left: Boolean,
        opacity: Number,
        right: Boolean,
        top: Boolean,
        visible: Boolean,
        width: Number
    }
});
exports.DxPaneBorder = DxPaneBorder;
DxPaneBorder.$_optionName = "border";
var DxPoint = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        image: [Object, String],
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        size: Number,
        symbol: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "circle",
                "cross",
                "polygon",
                "square",
                "triangleDown",
                "triangleUp"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxPoint = DxPoint;
DxPoint.$_optionName = "point";
DxPoint.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    image: { isCollectionItem: false, optionName: "image" },
    pointBorder: { isCollectionItem: false, optionName: "border" },
    pointHoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    pointSelectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    selectionStyle: { isCollectionItem: false, optionName: "selectionStyle" }
};
var DxPointBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxPointBorder = DxPointBorder;
DxPointBorder.$_optionName = "border";
var DxPointHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        size: Number
    }
});
exports.DxPointHoverStyle = DxPointHoverStyle;
DxPointHoverStyle.$_optionName = "hoverStyle";
DxPointHoverStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    pointBorder: { isCollectionItem: false, optionName: "border" }
};
var DxPointSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        size: Number
    }
});
exports.DxPointSelectionStyle = DxPointSelectionStyle;
DxPointSelectionStyle.$_optionName = "selectionStyle";
DxPointSelectionStyle.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    pointBorder: { isCollectionItem: false, optionName: "border" }
};
var DxReduction = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        level: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "close",
                "high",
                "low",
                "open"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxReduction = DxReduction;
DxReduction.$_optionName = "reduction";
var DxScrollBar = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        offset: Number,
        opacity: Number,
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        width: Number
    }
});
exports.DxScrollBar = DxScrollBar;
DxScrollBar.$_optionName = "scrollBar";
var DxSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        hatching: Object,
        size: Number,
        width: Number
    }
});
exports.DxSelectionStyle = DxSelectionStyle;
DxSelectionStyle.$_optionName = "selectionStyle";
var DxSeries = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        aggregation: Object,
        argumentField: String,
        axis: String,
        barPadding: Number,
        barWidth: Number,
        border: Object,
        closeValueField: String,
        color: String,
        cornerRadius: Number,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        highValueField: String,
        hoverMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "nearestPoint",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        hoverStyle: Object,
        ignoreEmptyPoints: Boolean,
        innerColor: String,
        label: Object,
        lowValueField: String,
        maxLabelCount: Number,
        minBarSize: Number,
        name: String,
        opacity: Number,
        openValueField: String,
        pane: String,
        point: Object,
        rangeValue1Field: String,
        rangeValue2Field: String,
        reduction: Object,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "allArgumentPoints",
                "allSeriesPoints",
                "excludePoints",
                "includePoints",
                "none",
                "onlyPoint"
            ].indexOf(v) !== -1; }
        },
        selectionStyle: Object,
        showInLegend: Boolean,
        sizeField: String,
        stack: String,
        tag: {},
        tagField: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "area",
                "bar",
                "bubble",
                "candlestick",
                "fullstackedarea",
                "fullstackedbar",
                "fullstackedline",
                "fullstackedspline",
                "fullstackedsplinearea",
                "line",
                "rangearea",
                "rangebar",
                "scatter",
                "spline",
                "splinearea",
                "stackedarea",
                "stackedbar",
                "stackedline",
                "stackedspline",
                "stackedsplinearea",
                "steparea",
                "stepline",
                "stock"
            ].indexOf(v) !== -1; }
        },
        valueErrorBar: Object,
        valueField: String,
        visible: Boolean,
        width: Number
    }
});
exports.DxSeries = DxSeries;
DxSeries.$_optionName = "series";
DxSeries.$_isCollectionItem = true;
var DxSeriesBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        width: Number
    }
});
exports.DxSeriesBorder = DxSeriesBorder;
DxSeriesBorder.$_optionName = "border";
var DxSeriesTemplate = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeSeries: Function,
        nameField: String
    }
});
exports.DxSeriesTemplate = DxSeriesTemplate;
DxSeriesTemplate.$_optionName = "seriesTemplate";
var DxShadow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        blur: Number,
        color: String,
        offsetX: Number,
        offsetY: Number,
        opacity: Number
    }
});
exports.DxShadow = DxShadow;
DxShadow.$_optionName = "shadow";
var DxSize = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        width: Number
    }
});
exports.DxSize = DxSize;
DxSize.$_optionName = "size";
var DxStrip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        endValue: {},
        label: Object,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        startValue: {}
    }
});
exports.DxStrip = DxStrip;
DxStrip.$_optionName = "strips";
DxStrip.$_isCollectionItem = true;
var DxStripLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxStripLabel = DxStripLabel;
DxStripLabel.$_optionName = "label";
var DxStripStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        label: Object,
        paddingLeftRight: Number,
        paddingTopBottom: Number
    }
});
exports.DxStripStyle = DxStripStyle;
DxStripStyle.$_optionName = "stripStyle";
DxStripStyle.$_expectedChildren = {
    label: { isCollectionItem: false, optionName: "label" },
    stripStyleLabel: { isCollectionItem: false, optionName: "label" }
};
var DxStripStyleLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxStripStyleLabel = DxStripStyleLabel;
DxStripStyleLabel.$_optionName = "label";
DxStripStyleLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxSubtitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String
    }
});
exports.DxSubtitle = DxSubtitle;
DxSubtitle.$_optionName = "subtitle";
DxSubtitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxTick = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        length: Number,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxTick = DxTick;
DxTick.$_optionName = "tick";
var DxTickInterval = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        days: Number,
        hours: Number,
        milliseconds: Number,
        minutes: Number,
        months: Number,
        quarters: Number,
        seconds: Number,
        weeks: Number,
        years: Number
    }
});
exports.DxTickInterval = DxTickInterval;
DxTickInterval.$_optionName = "tickInterval";
var DxTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: Number,
        placeholderSize: Number,
        subtitle: [Object, String],
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxTitle = DxTitle;
DxTitle.$_optionName = "title";
var DxTooltip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        argumentFormat: [Object, Function, String],
        arrowLength: Number,
        border: Object,
        color: String,
        container: {},
        customizeTooltip: Function,
        enabled: Boolean,
        font: Object,
        format: [Object, Function, String],
        location: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "edge"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        shadow: Object,
        shared: Boolean,
        zIndex: Number
    }
});
exports.DxTooltip = DxTooltip;
DxTooltip.$_optionName = "tooltip";
DxTooltip.$_expectedChildren = {
    argumentFormat: { isCollectionItem: false, optionName: "argumentFormat" },
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    shadow: { isCollectionItem: false, optionName: "shadow" },
    tooltipBorder: { isCollectionItem: false, optionName: "border" }
};
var DxTooltipBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxTooltipBorder = DxTooltipBorder;
DxTooltipBorder.$_optionName = "border";
var DxUrl = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        rangeMaxPoint: String,
        rangeMinPoint: String
    }
});
exports.DxUrl = DxUrl;
DxUrl.$_optionName = "url";
var DxValueAxis = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowDecimals: Boolean,
        autoBreaksEnabled: Boolean,
        axisDivisionFactor: Number,
        breaks: Array,
        breakStyle: Object,
        categories: Array,
        color: String,
        constantLines: Array,
        constantLineStyle: Object,
        discreteAxisDivisionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "betweenLabels",
                "crossLabels"
            ].indexOf(v) !== -1; }
        },
        endOnTick: Boolean,
        grid: Object,
        inverted: Boolean,
        label: Object,
        logarithmBase: Number,
        max: {},
        maxAutoBreakCount: Number,
        maxValueMargin: Number,
        min: {},
        minorGrid: Object,
        minorTick: Object,
        minorTickCount: Number,
        minorTickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        minValueMargin: Number,
        minVisualRangeLength: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        multipleAxesSpacing: Number,
        name: String,
        opacity: Number,
        pane: String,
        placeholderSize: Number,
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        showZero: Boolean,
        strips: Array,
        stripStyle: Object,
        synchronizedValue: Number,
        tick: Object,
        tickInterval: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        title: [Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "continuous",
                "discrete",
                "logarithmic"
            ].indexOf(v) !== -1; }
        },
        valueMarginsEnabled: Boolean,
        valueType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "datetime",
                "numeric",
                "string"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        visualRange: [Array, Object],
        visualRangeUpdateMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "keep",
                "reset",
                "shift"
            ].indexOf(v) !== -1; }
        },
        wholeRange: [Array, Object],
        width: Number
    }
});
exports.DxValueAxis = DxValueAxis;
DxValueAxis.$_optionName = "valueAxis";
DxValueAxis.$_isCollectionItem = true;
DxValueAxis.$_expectedChildren = {
    axisConstantLineStyle: { isCollectionItem: false, optionName: "constantLineStyle" },
    axisLabel: { isCollectionItem: false, optionName: "label" },
    axisTitle: { isCollectionItem: false, optionName: "title" },
    break: { isCollectionItem: true, optionName: "breaks" },
    constantLine: { isCollectionItem: true, optionName: "constantLines" },
    constantLineStyle: { isCollectionItem: false, optionName: "constantLineStyle" },
    label: { isCollectionItem: false, optionName: "label" },
    minorTickInterval: { isCollectionItem: false, optionName: "minorTickInterval" },
    minVisualRangeLength: { isCollectionItem: false, optionName: "minVisualRangeLength" },
    strip: { isCollectionItem: true, optionName: "strips" },
    tickInterval: { isCollectionItem: false, optionName: "tickInterval" },
    title: { isCollectionItem: false, optionName: "title" },
    visualRange: { isCollectionItem: false, optionName: "visualRange" },
    wholeRange: { isCollectionItem: false, optionName: "wholeRange" }
};
var DxValueErrorBar = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        displayMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "high",
                "low",
                "none"
            ].indexOf(v) !== -1; }
        },
        edgeLength: Number,
        highValueField: String,
        lineWidth: Number,
        lowValueField: String,
        opacity: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fixed",
                "percent",
                "stdDeviation",
                "stdError",
                "variance"
            ].indexOf(v) !== -1; }
        },
        value: Number
    }
});
exports.DxValueErrorBar = DxValueErrorBar;
DxValueErrorBar.$_optionName = "valueErrorBar";
var DxVerticalLine = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxVerticalLine = DxVerticalLine;
DxVerticalLine.$_optionName = "verticalLine";
DxVerticalLine.$_expectedChildren = {
    crosshairLabel: { isCollectionItem: false, optionName: "label" },
    label: { isCollectionItem: false, optionName: "label" }
};
var DxVisualRange = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        endValue: {},
        length: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        startValue: {}
    }
});
exports.DxVisualRange = DxVisualRange;
DxVisualRange.$_optionName = "visualRange";
var DxWholeRange = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        endValue: {},
        length: {
            type: [Number, Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "day",
                "hour",
                "millisecond",
                "minute",
                "month",
                "quarter",
                "second",
                "week",
                "year"
            ].indexOf(v) !== -1; }
        },
        startValue: {}
    }
});
exports.DxWholeRange = DxWholeRange;
DxWholeRange.$_optionName = "wholeRange";
var DxWidth = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        rangeMaxPoint: Number,
        rangeMinPoint: Number
    }
});
exports.DxWidth = DxWidth;
DxWidth.$_optionName = "width";
var DxZoomAndPan = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowMouseWheel: Boolean,
        allowTouchGestures: Boolean,
        argumentAxis: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "both",
                "none",
                "pan",
                "zoom"
            ].indexOf(v) !== -1; }
        },
        dragBoxStyle: Object,
        dragToZoom: Boolean,
        panKey: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "alt",
                "ctrl",
                "meta",
                "shift"
            ].indexOf(v) !== -1; }
        },
        valueAxis: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "both",
                "none",
                "pan",
                "zoom"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxZoomAndPan = DxZoomAndPan;
DxZoomAndPan.$_optionName = "zoomAndPan";
DxZoomAndPan.$_expectedChildren = {
    dragBoxStyle: { isCollectionItem: false, optionName: "dragBoxStyle" }
};
exports.default = DxChart;
