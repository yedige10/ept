/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import BarGauge, { IOptions } from "devextreme/viz/bar_gauge";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "animation" | "backgroundColor" | "barSpacing" | "baseValue" | "disabled" | "elementAttr" | "endValue" | "export" | "geometry" | "label" | "loadingIndicator" | "margin" | "onDisposing" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onOptionChanged" | "onTooltipHidden" | "onTooltipShown" | "palette" | "paletteExtensionMode" | "pathModified" | "redrawOnResize" | "relativeInnerRadius" | "rtlEnabled" | "size" | "startValue" | "theme" | "title" | "tooltip" | "values">;
interface DxBarGauge extends VueConstructor, AccessibleOptions {
    readonly instance?: BarGauge;
}
declare const DxBarGauge: DxBarGauge;
declare const DxAnimation: any;
declare const DxBorder: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxGeometry: any;
declare const DxLabel: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxSubtitle: any;
declare const DxTitle: any;
declare const DxTooltip: any;
export default DxBarGauge;
export { DxBarGauge, DxAnimation, DxBorder, DxExport, DxFont, DxFormat, DxGeometry, DxLabel, DxLoadingIndicator, DxMargin, DxShadow, DxSize, DxSubtitle, DxTitle, DxTooltip };
