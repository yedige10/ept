/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var autocomplete_1 = require("devextreme/ui/autocomplete");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxAutocomplete = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        dataSource: [Array, Object, String],
        deferRendering: Boolean,
        disabled: Boolean,
        displayValue: String,
        dropDownButtonTemplate: {},
        elementAttr: Object,
        focusStateEnabled: Boolean,
        grouped: Boolean,
        groupTemplate: {},
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        inputAttr: Object,
        isValid: Boolean,
        items: Array,
        itemTemplate: {},
        maxItemCount: Number,
        maxLength: [Number, String],
        minSearchLength: Number,
        name: String,
        onChange: Function,
        onClosed: Function,
        onContentReady: Function,
        onCopy: Function,
        onCut: Function,
        onDisposing: Function,
        onEnterKey: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onInput: Function,
        onItemClick: Function,
        onKeyDown: Function,
        onKeyPress: Function,
        onKeyUp: Function,
        onOpened: Function,
        onOptionChanged: Function,
        onPaste: Function,
        onSelectionChanged: Function,
        onValueChanged: Function,
        opened: Boolean,
        openOnFieldClick: Boolean,
        placeholder: String,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        searchExpr: [Array, Function, String],
        searchMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "contains",
                "startswith"
            ].indexOf(v) !== -1; }
        },
        searchTimeout: Number,
        selectedItem: {},
        showClearButton: Boolean,
        showDropDownButton: Boolean,
        spellcheck: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "outlined",
                "underlined",
                "filled"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        text: String,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: String,
        valueChangeEvent: String,
        valueExpr: [Function, String],
        visible: Boolean,
        width: [Function, Number, String]
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = autocomplete_1.default;
        this.$_expectedChildren = {
            item: { isCollectionItem: true, optionName: "items" }
        };
    }
});
exports.DxAutocomplete = DxAutocomplete;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        html: String,
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
exports.default = DxAutocomplete;
