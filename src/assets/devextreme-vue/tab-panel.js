/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var tab_panel_1 = require("devextreme/ui/tab_panel");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxTabPanel = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        animationEnabled: Boolean,
        dataSource: [Array, Object, String],
        deferRendering: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        itemHoldTimeout: Number,
        items: Array,
        itemTemplate: {},
        itemTitleTemplate: {},
        loop: Boolean,
        noDataText: String,
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onOptionChanged: Function,
        onSelectionChanged: Function,
        onTitleClick: [Function, String],
        onTitleHold: Function,
        onTitleRendered: Function,
        repaintChangesOnly: Boolean,
        rtlEnabled: Boolean,
        scrollByContent: Boolean,
        scrollingEnabled: Boolean,
        selectedIndex: Number,
        selectedItem: Object,
        showNavButtons: Boolean,
        swipeEnabled: Boolean,
        tabIndex: Number,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = tab_panel_1.default;
        this.$_expectedChildren = {
            item: { isCollectionItem: true, optionName: "items" }
        };
    }
});
exports.DxTabPanel = DxTabPanel;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        badge: String,
        disabled: Boolean,
        html: String,
        icon: String,
        tabTemplate: {},
        template: {},
        text: String,
        title: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
exports.default = DxTabPanel;
