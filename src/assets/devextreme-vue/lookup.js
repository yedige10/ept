/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var lookup_1 = require("devextreme/ui/lookup");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxLookup = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        animation: Object,
        applyButtonText: String,
        applyValueMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "instantly",
                "useButtons"
            ].indexOf(v) !== -1; }
        },
        cancelButtonText: String,
        cleanSearchOnOpening: Boolean,
        clearButtonText: String,
        closeOnOutsideClick: [Boolean, Function],
        dataSource: [Array, Object, String],
        deferRendering: Boolean,
        disabled: Boolean,
        displayExpr: [Function, String],
        displayValue: String,
        elementAttr: Object,
        fieldTemplate: {},
        focusStateEnabled: Boolean,
        fullScreen: Boolean,
        grouped: Boolean,
        groupTemplate: {},
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        inputAttr: Object,
        isValid: Boolean,
        items: Array,
        itemTemplate: {},
        minSearchLength: Number,
        name: String,
        nextButtonText: String,
        noDataText: String,
        onClosed: Function,
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onItemClick: Function,
        onOpened: Function,
        onOptionChanged: Function,
        onPageLoading: Function,
        onPullRefresh: Function,
        onScroll: Function,
        onSelectionChanged: Function,
        onTitleRendered: Function,
        onValueChanged: Function,
        opened: Boolean,
        pageLoadingText: String,
        pageLoadMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "nextButton",
                "scrollBottom"
            ].indexOf(v) !== -1; }
        },
        placeholder: String,
        popupHeight: [Function, Number, String],
        popupWidth: [Function, Number, String],
        position: Object,
        pulledDownText: String,
        pullingDownText: String,
        pullRefreshEnabled: Boolean,
        refreshingText: String,
        rtlEnabled: Boolean,
        searchEnabled: Boolean,
        searchExpr: [Array, Function, String],
        searchMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "contains",
                "startswith"
            ].indexOf(v) !== -1; }
        },
        searchPlaceholder: String,
        searchTimeout: Number,
        selectedItem: {},
        shading: Boolean,
        showCancelButton: Boolean,
        showClearButton: Boolean,
        showDataBeforeSearch: Boolean,
        showPopupTitle: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "outlined",
                "underlined",
                "filled"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        text: String,
        title: String,
        titleTemplate: {},
        useNativeScrolling: Boolean,
        usePopover: Boolean,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: {},
        valueChangeEvent: String,
        valueExpr: [Function, String],
        visible: Boolean,
        width: [Function, Number, String]
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = lookup_1.default;
        this.$_expectedChildren = {
            animation: { isCollectionItem: false, optionName: "animation" },
            item: { isCollectionItem: true, optionName: "items" },
            position: { isCollectionItem: false, optionName: "position" }
        };
    }
});
exports.DxLookup = DxLookup;
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        hide: Object,
        show: Object
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
DxAnimation.$_expectedChildren = {
    hide: { isCollectionItem: false, optionName: "hide" },
    show: { isCollectionItem: false, optionName: "show" }
};
var DxAt = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxAt = DxAt;
DxAt.$_optionName = "at";
var DxBoundaryOffset = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: Number,
        y: Number
    }
});
exports.DxBoundaryOffset = DxBoundaryOffset;
DxBoundaryOffset.$_optionName = "boundaryOffset";
var DxCollision = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "flip",
                "flipfit",
                "none"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "flip",
                "flipfit",
                "none"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxCollision = DxCollision;
DxCollision.$_optionName = "collision";
var DxHide = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxHide = DxHide;
DxHide.$_optionName = "hide";
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        html: String,
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
var DxMy = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxMy = DxMy;
DxMy.$_optionName = "my";
var DxOffset = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: Number,
        y: Number
    }
});
exports.DxOffset = DxOffset;
DxOffset.$_optionName = "offset";
var DxPosition = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        at: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        boundary: {},
        boundaryOffset: [Object, String],
        collision: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "fit flip",
                "fit flipfit",
                "fit none",
                "flip",
                "flip fit",
                "flip none",
                "flipfit",
                "flipfit fit",
                "flipfit none",
                "none",
                "none fit",
                "none flip",
                "none flipfit"
            ].indexOf(v) !== -1; }
        },
        my: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        of: {},
        offset: [Object, String]
    }
});
exports.DxPosition = DxPosition;
DxPosition.$_optionName = "position";
DxPosition.$_expectedChildren = {
    at: { isCollectionItem: false, optionName: "at" },
    boundaryOffset: { isCollectionItem: false, optionName: "boundaryOffset" },
    collision: { isCollectionItem: false, optionName: "collision" },
    my: { isCollectionItem: false, optionName: "my" },
    offset: { isCollectionItem: false, optionName: "offset" }
};
var DxShow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxShow = DxShow;
DxShow.$_optionName = "show";
exports.default = DxLookup;
